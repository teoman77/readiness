_While it is encouraged for parts of this document to be filled out, not all of the items below will be relevant. Leave all non-applicable items intact and add the reasons for why in place of the response._
_This Guide is just that, a Guide. If something is not asked, but should be, it is strongly encouraged to add it as necessary._

## Summary

- [ ] **Provide a high level summary of this new product feature. Explain how this change will benefit GitLab customers. Enumerate the customer use-cases.**

GitLab Metrics Exporter (GME) is meant to subsume the functionality of [gitlab-exporter](https://gitlab.com/gitlab-org/gitlab-exporter/)
and in-app Ruby Prometheus exporters distributed with and running in gitlab-rails.

It has the following goals:

- Provide a single, unified mechanism to export application-specific metrics to Prometheus.
- Ability to run as a standalone system or as a sidecar process to gitlab-rails.
- Ability to collect and serve a variety of metrics using a flexible approach to probing.
- Ability to collect and serve metrics in a memory- and CPU-efficient way.
- Ability to exploit concurrency when running multiple probes.

GME is not a user-facing system or feature.

The plan is to develop and roll out GME in two phases:

- [Phase 1](https://gitlab.com/groups/gitlab-org/-/epics/8042): Replace Ruby exporters part of gitlab-rails. This runs GME as a process sidecar to Puma and Sidekiq. **Done.**
- [Phase 2](https://gitlab.com/groups/gitlab-org/-/epics/8043): Replace gitlab-exporter. This would see GME run in its own container. **Not done.**

- [ ] **What metrics, including business metrics, should be monitored to ensure will this feature launch will be a success?**

GME exports metrics about itself via the `self` probe: https://gitlab.com/gitlab-org/gitlab-metrics-exporter#probes

That said, we will only measure sucess of **phase 1** at this point (see previous section). I would consider the rollout successful if:

1. There are no meaningful semantic differences w.r.t. exported Prometheus metrics compared to the Ruby exporters.
   There _may_ be minor acceptable differences in e.g. the precision of numeric values rendered.
1. It must perform at least as good as the Ruby exporters (ideally better). Performance is measured in both request
   latencies, CPU usage and memory use for the same workloads compared to the Ruby exporters. Synthetic benchmarks
   so far show improved performance with similar memory use (there are ideas for how to bring memory use down further.)

The actual rollout plan is still in draft and will be found in [this issue](https://gitlab.com/gitlab-org/gitlab/-/issues/355460).

## Architecture

- [ ] **Add architecture diagrams to this issue of feature components and how they interact with existing GitLab components. Make sure to include the following: Internal dependencies, ports, encryption, protocols, security policies, etc.**

Note that GME is a drop-in replacement for the Ruby exporters. From the perspective of both Prometheus and the Rails app,
they behave the same (outside of being configured in a different way.) Rails workers write metrics into a memory-mapped
file via the `prometheus-client-mmap` gem. These binary files are stored on disk. Exporters receive a scrape request and load,
parse, merge and serialize these files to Prometheus.

The main difference is in how they operate: GME will execute probes in parallel and also break down the `mmap` job
into smaller, parallel subtasks. Ruby exporters use a singled-threaded web server (WEBrick) and use Rack middleware
to export both Rails metrics as well as their own. This requires serializing this work onto a single thread of execution.

### Current: Ruby exporters

```mermaid
graph LR
  subgraph "Puma/Sidekiq node"
    disk[File system]

    note1[/*only for Puma/]

    subgraph "Web/Sidekiq exporter"
      server(Rack HTTP/S server)
      self(metrics middleware)--3a: Read exporter metrics-->server
      mmap(mmap middleware)--3b: Read Rails metrics-->disk
    end

    subgraph "GitLab Rails"
      worker(Worker process)-- 2: Write metrics -->disk
      master(Primary process*)--1: Start/Supervise-->server
    end
  end

  subgraph "Prometheus node"
    prometheus-scraper-- "4: GET /metrics" -->server
  end
```

### New: GME

```mermaid
graph LR
  subgraph "Puma/Sidekiq node"
    disk[File system]

    note1[/*only for Puma/]

    subgraph GME
      server(Go HTTP/S server)
      self("self probe (async)")--3a: Read exporter metrics-->server
      mmap("mmap probe (async)")--3b: Read Rails metrics-->disk
    end

    subgraph "GitLab Rails"
      worker(Worker process)-- 2: Write metrics -->disk
      master("Primary process*")--1: Start/Supervise-->server
    end
  end

  subgraph "Prometheus node"
    prometheus-scraper-- "4: GET /metrics" -->server
  end
```

Ports used for scraping vary with Puma or Sidekiq. SaaS currently scrapes via plain HTTP. HTTP with TLS is supported, however.

[Internal dependencies](https://gitlab.com/gitlab-org/gitlab-metrics-exporter/-/blob/main/go.mod)

- [ ] **Describe each component of the new feature and enumerate what it does to support customer use cases.**

GME is modularized via probes. A probe is a function that samples data from some data source. The data source can be anything.
We currently support two probes: `self` and `mmap`.

- `self`. This probe samples Go runtime, procfs, and HTTP server metrics. This is useful for GitLab employees and
  self-managed admins to monitor GME itself.
- `mmap`. This probe reads db files written by prometheus-client-mmap. It is the
format used to emit application metrics from gitlab-rails. This is the primary way of publishing Rails application metrics
and benefits both self-managed admins and GitLab employees. Examples are Rails SLI metrics, Puma and Sidekiq metrics, Ruby and process
metrics, and more. See https://docs.gitlab.com/ee/administration/monitoring/prometheus/gitlab_metrics.html.

https://gitlab.com/gitlab-org/gitlab-metrics-exporter#probes

- [ ] **For each component and dependency, what is the blast radius of failures? Is there anything in the feature design that will reduce this risk?**

Fault tolerance was one of the primary objectives when rewriting Ruby exporters. Originally they would execute on a thread in a Puma
or Sidekiq process, thus impacting Ruby GC and process memory and even crashing worker processes. These were then extracted into
separate processes. For reasons of efficiency we then decided to rewrite them in Go, which led to GME.

If a GME sidecar process goes away, the main application is not affected since they merely communicate via the pod file system.
gitlab-rails includes a monitoring thread ([ProcessSupervisor](https://gitlab.com/gitlab-org/gitlab/-/blob/e04a76eee1156ee10307d3daa66cd003fd526195/lib/gitlab/process_supervisor.rb)) that will notice if GME dies and will attempt to restart it.

- [ ] **If applicable, explain how this new feature will scale and any potential single points of failure in the design.**

It scales via Go routines. The system is quite simple so I am not aware of any single points of failure. A crash would result in the
process going away and Rails trying to restart it. Note that each Rails pod runs its own GME instance. Thus, it scales
horizontally with Rails deployments.

## Operational Risk Assessment

- [ ] **What are the potential scalability or performance issues that may result with this change?**

Delays in exporting application metrics to Prometheus would impact AlertManager notifications and most dashboards we use to observe
the health of `sidekiq` or `webservice` pods.

Memory and CPU use could be higher than anticipated, which affects pod resource requests and limits of `webservice` and `sidekiq` pods.

- [ ] **List the external and internal dependencies to the application (ex: redis, postgres, etc) for this feature and how the service will be impacted by a failure of that dependency.**

No dependencies.

- [ ] **Were there any features cut or compromises made to make the feature launch?**

One compromise we made was to not isolate GME into its own container for two reasons:

1. We need to support Omnibus and from-source installations, which cannot assume a container-managed
   runtime environment. For these cases, we would also have to have made GME its own `runit` service,
   in order to make sure that GME is being restarted should it go away. Instead, we rely on existing
   in-app logic in the Rails monolith to run a supervision thread that keeps the metrics server alive.
   This logic is also used for the existing Ruby exporters and for sidekiq-cluster workers, which allows
   us to ship GME as a drop-in replacement for the Ruby exporters.
1. Since GME and gitlab-rails need to share a filesystem, isolating GME into a separate container
   would have required extra thought and work to be put into making sure both containers have access
   to the shared metric files.

For [phase 2](https://gitlab.com/groups/gitlab-org/-/epics/8043) of the project, in which we hope to replace `gitlab-exporter` as well, we would
have to solve this problem because that would require running GME on a separate node.
It is unclear currently if or when that would happen, however.

- [ ] **List the top three operational risks when this feature goes live.**

1. Inability to export application metrics to Prometheus would impact AlertManager notifications and most dashboards we use to observe
the health of `sidekiq` or `webservice` pods.
1. Memory and CPU use could be higher than anticipated, which affects pod resource requests and limits of `webservice` and `sidekiq` pods.
1. Unexpected problems in the interaction between Puma/Sidekiq workers and the GME process, such as failing to keep the GME server alive should it stop unexpectedly.

- [ ] **What are a few operational concerns that will not be present at launch, but may be a concern later?**

Eventually, we are looking to extend GME to also cover all of the functionality currently provided by gitlab-exporter,
which may change the performance and stability characteristics of this system. This is referred to as **phase 2**.
Refer to the `Summary` section for more information.

Such functionality would include probing into production postgres and redis clusters as well as running GME as a standalone service.

- [ ] **Can the new product feature be safely rolled back once it is live, can it be disabled using a feature flag?**

It can be disabled with an environment variable in gitlab-rails, but this would require rolling over all Puma and Sidekiq pods.

- [ ] **Document every way the customer will interact with this new feature and how customers will be impacted by a failure of each interaction.**

This being a drop-in replacement for existing systems, we don't expect any visible changes for customers.

- [ ] **As a thought experiment, think of worst-case failure scenarios for this product feature, how can the blast-radius of the failure be isolated?**

We already reduced the blast radius by moving metrics into a separate Go process with GME (these used to be exported from web or sidekiq workers.)
Crashes or poor performance will therefore not affect Puma or Sidekiq.

The remaining impact is at the node/host level as mentioned above. Unexpected memory or CPU use could make k8s migrate or restart the pod, which
would affect Puma and Sidekiq availability.

We do have this risk already, however, since we already run Ruby exporters in exactly the same way and which exhibit the same risk profile.
The risk here is more in the uncertainty of introducing a completely new system performing the same tasks.

## Database

- [ ] **If we use a database, is the data structure verified and vetted by the database team?**

We do not use a database.

- [ ] **Do we have an approximate growth rate of the stored data (for capacity planning)?**

Does not apply; the way metrics data is stored on disk for communication between gitlab-rails and GME will not change with the introduction of this system. GME itself does not store any data on disk.

- [ ] **Can we age data and delete data of a certain age?**

Not that I'm aware of.

## Security and Compliance

- **Are we adding any new resources of the following type? (If yes, please list them here or link to a place where they are listed)**
  - [ ] **AWS Accounts/GCP Projects**
  - [ ] **New Subnets**
  - [ ] **VPC/Network Peering**
  - [ ] **DNS names**
  - [ ] **Entry-points exposed to the internet (Public IPs, Load-Balancers, Buckets, etc...)**
  - [ ] **Other (anything relevant that might be worth mention)**

None of the above.

- **Secure Software Development Life Cycle (SSDLC)**
    - [ ] **Is the configuration following a security standard? (CIS is a good baseline for example)**

    We support TLS encryption, which was added as part of the FIPS work: https://gitlab.com/gitlab-org/gitlab-metrics-exporter/-/issues/10

    - [ ] **All cloud infrastructure resources are labeled according to the [Infrastructure Labels and Tags](https://about.gitlab.com/handbook/infrastructure-standards/labels-tags/) guidelines**

    We are not introducing any new infrastructure resources. We are adding a process to existing pods. These pods are already labeled.

    - [ ] **Were the [GitLab security development guidelines](https://docs.gitlab.com/ee/development/secure_coding_guidelines.html) followed for this feature?**

    Yes, and we already performed a security audit of SAST and related scan results: https://gitlab.com/gitlab-org/gitlab-metrics-exporter/-/issues/4

    - [ ] **Do we have an automatic procedure to update the infrastructure (OS, container images, packages, etc...)**

    Yes, GME is versioned via a VERSION file in gitlab-rails, as it is bundled with gitlab-rails, and we have CNG images too.

    - [ ] **Do we use IaC (Terraform) for all the infrastructure related to this feature? If not, what kind of resources are not covered?**
        - [ ] **Do we have secure static code analysis tools ([kics](https://github.com/Checkmarx/kics) or [checkov](https://github.com/bridgecrewio/checkov)) covering this feature's terraform?**

    Infrastructure and configuration are managed via the GitLab Helm Chart.

  - **If there's a new terraform state:**
    - [ ] **Where is to terraform state stored, and who has access to it?**
  - [ ] **Does this feature add secrets to the terraform state? If yes, can they be stored in a secrets manager?**
  - **If we're creating new containers:**
    - [ ] **Are we using a distroless base image?**
    - **Do we have security scanners covering these containers?**
      - [ ] **`kics` or `checkov` for Dockerfiles for example**
      - [ ] **[GitLab's container](https://docs.gitlab.com/ee/user/application_security/container_scanning/#configuration) scanner for vulnerabilities**

- **Identity and Access Management**
  -  [ ] Are we adding any new forms of **Authentication** (New service-accounts, users/password for storage, OIDC, etc...)?
  -  [ ] **Does it follow the least privilege principle?**

  Does not apply.

- **If we are adding any new Data Storage (Databases, buckets, etc...)**
  -  [ ] **What kind of data is stored on each system? (secrets, customer data, audit, etc...)**
  -  [ ] **How is data rated according to our [data classification standard](https://about.gitlab.com/handbook/engineering/security/data-classification-standard.html) (customer data is RED)**
  -  [ ] **Is data it encrypted at rest? (If the storage is provided by a GCP service, the answer is most likely yes)**
  -  [ ] **Do we have audit logs on data access?**

  Does not apply.

- **Network security (encryption and ports should be clear in the architecture diagram above)**
  -  [ ] **Firewalls follow the least privilege principle (w/ network policies in Kubernetes or firewalls on cloud provider)**
  -  [ ] **Is the service covered by any DDoS protection solution (GCP/AWS load-balancers or Cloudflare usually cover this)**
  -  [ ] **Is the service covered by a WAF (Web Application Firewall)**

  Ports vary based on deployment.

  - `webservice`: `8083` (uses [Chart default](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/-/blob/606ef1f1cc8695f19c842056d8f9c2384c381779/vendor/charts/gitlab/pre/charts/gitlab/charts/webservice/values.yaml#L61))
  - `sidekiq`: `8083` (via [gitlab-com override](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/-/blob/4b2d5c93d4f620218b0383438872818a4b332617/releases/gitlab/values/values.yaml.gotmpl#L941))

  TLS is supported for FIPS compliance but is disabled by default. HTTP and HTTPS are mutually exclusive
  and are served over the same port.

  The service is not exposed to the public, it's only accessible internally. Its primary client is Prometheus, which runs within our network.

- **Logging & Audit**
  - [ ] **Has effort been made to obscure or elide sensitive customer data in logging?**

  Not necessary; we do not ingest any sensitive customer data.

- **Compliance**
    - [ ] **Is the service subject to any regulatory/compliance standards? If so, detail which and provide details on applicable controls, management processes, additional monitoring, and mitigating factors.**

    Our standard change management applies.

## Performance

- [ ] **Explain what validation was done following GitLab's [performance guidlines](https://docs.gitlab.com/ce/development/performance.html) please explain or link to the results below**

We ran extensive performance tests since a primary goal was to make this system at least as (if not more) efficient than the
existing Ruby exporters.

Benchmarks are part of the test suite and can be found [here](https://gitlab.com/gitlab-org/gitlab-metrics-exporter/-/blob/main/test/benchmark/bench_test.go).

Performance tests comparing GME with the existing Ruby exporter(s) on the same data sets can be found here:
https://gitlab.com/gitlab-org/gitlab-metrics-exporter/-/blob/main/performance-test-results/results.md

- [ ] **Are there any potential performance impacts on the database when this feature is enabled at GitLab.com scale?**

No.

- [ ] **Are there any throttling limits imposed by this feature? If so how are they managed?**

No.

- [ ] **If there are throttling limits, what is the customer experience of hitting a limit?**

Does not apply.

- [ ] **For all dependencies external and internal to the application, are there retry and back-off strategies for them?**

Does not apply.

- [ ] **Does the feature account for brief spikes in traffic, at least 2x above the expected TPS?**

Yes; exporters are very low-traffic systems and GME has a concurrency limit of 1 in place, so that we never service
more than a single request simultaneously, which caps resource requirements and reduces knock-on effects on
Puma or Sidekiq container resource utilization. Additional requests latch on to the first in-flight operation
and will receive its result.

## Backup and Restore

Does not apply.

- [ ] **Outside of existing backups, are there any other customer data that needs to be backed up for this product feature?**
- [ ] **Are backups monitored?**
- [ ] **Was a restore from backup tested?**

## Monitoring and Alerts

- [ ] **Is the service logging in JSON format and are logs forwarded to logstash?**

Yes; it uses labkit logging and writes to the same log files we already configure for the Ruby exporters.

We currently only ingest these logs into ELK for Puma nodes. For Sidekiq, we are working on doing the same
in https://gitlab.com/gitlab-org/gitlab-metrics-exporter/-/issues/19.

- [ ] **Is the service reporting metrics to Prometheus?**

Yes. It includes a self-probe to export standard Golang and labkit request metrics about itself via the same `/metrics` endpoint it uses to export
metrics about gitlab-rails.

- [ ] **How is the end-to-end customer experience measured?**

Does not apply.

- [ ] **Do we have a target SLA in place for this service?**

We aim to respond within the Prometheus scrape timeout of 10 seconds.

- [ ] **Do we know what the indicators (SLI) are that map to the target SLA?**

Prometheus reports scrape durations via the `scrape_duration_seconds` metric.

- [ ] **Do we have alerts that are triggered when the SLI's (and thus the SLA) are not met?**

Not currently.

We have no paging alerts in place for any Prometheus exporters, but meta-monitoring is out of scope for this readiness review.

- [ ] **Do we have troubleshooting runbooks linked to these alerts?**

Does not apply (see previous point.)

- [ ] **What are the thresholds for tweeting or issuing an official customer notification for an outage related to this feature?**

Does not apply.

- [ ] **do the oncall rotations responsible for this service have access to this service?**

Yes.

## Responsibility

- [ ] **Which individuals are the subject matter experts and know the most about this feature?**

Matthias Käppler (@mkaeppler)

- [ ] **Which team or set of individuals will take responsibility for the reliability of the feature once it is in production?**

Application Performance group

- [ ] **Is someone from the team who built the feature on call for the launch? If not, why not?**

Matthias Käppler (@mkaeppler) can make himself available.

## Testing

- [ ] **Describe the load test plan used for this feature. What breaking points were validated?**

We used Apache Bench to test the system under load (much more load than we expect it would receive in production).

I am not concerned about load so much as growth in metrics volume it must handle i.e. read and serialize.
We will only be able to verify this in production.

We did run performance tests and benchmarks with real-world sample data taken from SaaS Puma and Sidekiq nodes. (See above)

- [ ] **For the component failures that were theorized for this feature, were they tested? If so include the results of these failure tests.**

- Unexpected crashes: Verified that gitlab-rails will try to restart GME.
- Overrunning resource budgets: not tested; we would need help from SREs to figure out how to do that.

- [ ] **Give a brief overview of what tests are run automatically in GitLab's CI/CD pipeline for this feature?**

We run unit and integration tests in GME's pipeline, and we run an end-to-end QA test with our metrics exporters
in the gitlab-rails pipeline.
