# Istio Readiness Review

The Readiness Review document is designed to help you prepare your features and services for the GitLab Production Platforms.
Please engage with the relevant teams as soon as possible to begin review even if there are incomplete items below.
All sections should be completed up to the current [maturity level](https://docs.gitlab.com/ee/policy/experiment-beta-support.html).
For example, if the target maturity is "Beta", then items under "Experiment" and "Beta" should be completed.

_While it is encouraged for parts of this document to be filled out, not all of the items below will be relevant. Leave all non-applicable items intact and add 'N/A' or reasons for why in place of the response._
_This Guide is just that, a Guide. If something is not asked, but should be, it is strongly encouraged to add it as necessary._

## Experiment

Istio and Istio Ingress Gateways are at ~"Readiness::Experiment" maturity level. The features are currently deployed to the GKE clusters in the following environments:

- pre
- ops
- gstg

### Summary

[Istio](https://istio.io/latest/about/service-mesh/#what-is-istio) is an open platform-independent [service mesh](https://istio.io/latest/about/service-mesh/#what-is-a-service-mesh) that provides traffic management, policy enforcement, and telemetry collection.

Istio has two main components: the data plane and the control plane.

- The data plane is the communication between services. All traffic that mesh services send and receive (data plane traffic) is proxied through an Envoy proxy which is deployed along with each service that starts in the cluster, or runs alongside services running on VMs.
- The control plane (istiod) takes your desired configuration, and its view of the services, and dynamically programs the proxy servers, updating them as the rules or the environment changes.

#### Istio Gateways

[Istio](https://istio.io/) is a popular Ingress controller (and service mesh) that provides advanced traffic management, security, and observability features for microservice applications. Dynamic Ingress in the context of Istio typically refers to configuring Ingress resources to control how external traffic is routed to services running within the mesh.

Currently we are only using the `Istio Gateways` feature and the `Istio Control Plane` since it's always required for all Istio functionality.

### Service Catalog

- [x] Link to the [service catalog entry](https://gitlab.com/gitlab-com/runbooks/-/tree/master/services) for the service. Ensure that the following items are present in the service catalog, or listed here:
  - [Istio Service Catalog](https://gitlab.com/gitlab-com/runbooks/-/blob/master/services/service-catalog.yml#L2290-2313)
  - Link to the [Architecture Design Workflow](https://about.gitlab.com/handbook/engineering/architecture/workflow/) for this feature, if there wasn't a design completed for this feature please explain why.
    - [Istio Architectural Blueprint MR](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/134699)
  - List the feature group that created this feature/service and who are the current Engineering Managers, Product Managers and their Directors.
    - Group: SaaS Platforms - Foundations
    - Eng Manager: @amoter
    - Director: @marin
  - List individuals are the subject matter experts and know the most about this feature.
    - @f_santos
    - @pguinoiseau
    - @mchacon3
  - List the team or set of individuals will take responsibility for the reliability of the feature once it is in production.
    - SaaS Platforms - Foundations
  - List the member(s) of the team who built the feature will be on call for the launch.
    - Reliability
  - List the external and internal dependencies to the application (ex: redis, postgres, etc) for this feature and how the service will be impacted by a failure of that dependency.
    - Vault: Our Istio deployments contain secrets that are hosted in Vault. Secrets won't be able to be updated if Vault is unavailable.
    - Flux: We use Flux as the GitOps deployment mechanism for Istio. If Flux is unavailable we won't be able to deploy new versions of Istio or make changes to its configurations.

### Infrastructure

- [x] Do we use IaC (e.g., Terraform) for all the infrastructure related to this feature? If not, what kind of resources are not covered?
  - Istio is managed using GitOps (flux). The following are the repositories containing the Istio setup:
    - [Components - Istio](https://gitlab.com/gitlab-com/gl-infra/k8s-mgmt/components/-/tree/main/istio)
    - [gitlab-pre - Reliability Tenant Overlays - Istio](https://gitlab.com/gitlab-com/gl-infra/k8s-mgmt/tenants/reliability/-/tree/main/overlays/gke/gitlab-pre/us-east1/pre-gitlab-gke/istio)
    - [gitlab-pre - Delivery Tenant Overlays - Istio](https://gitlab.com/gitlab-com/gl-infra/k8s-mgmt/tenants/delivery/-/tree/main/overlays/gke/gitlab-pre/us-east1/pre-gitlab-gke/istio)
- [x] Is the service covered by any DDoS protection solution (GCP/AWS load-balancers or Cloudflare usually cover this)?
  - Istio Ingress Gateways will serve as origin to Cloudflare, so we can leverage Cloudflare DDOS protection capabilities, as well as it's CDN features.
  - Istio also has [rate-limit capabilities](https://istio.io/latest/docs/tasks/policy-enforcement/rate-limit/) that are native to Envoy proxy. Envoy support two kinds of rate limits:
    - [Global](https://www.envoyproxy.io/docs/envoy/latest/intro/arch_overview/other_features/global_rate_limiting#arch-overview-global-rate-limit): Uses a global gRPC rate limiting service to provide rate limiting for the entire mesh. 
    - [Local](https://www.envoyproxy.io/docs/envoy/latest/intro/arch_overview/other_features/local_rate_limiting#arch-overview-local-rate-limit): Local rate limiting is used to limit the rate of requests per service instance. Local rate limiting can be used in conjunction with global rate limiting to reduce load on the global rate limiting service.
- [x] Are all cloud infrastructure resources labeled according to the [Infrastructure Labels and Tags](https://about.gitlab.com/handbook/infrastructure-standards/labels-tags/) guidelines?
  - All Istio k8s resources are now labeled with the corresponding dept and stage labels:
    - gl_dept_group=eng-infra-reliability-foundations
    - gl_product_stage=eng-infra-reliability

### Operational Risk

- [x] List the top three operational risks when this feature goes live.
  - Additional infrastructure technology to maintain.
  - SREs are required to learn about Istio and service mesh methodologies to be able to support it.
  - Upgrade process for Istio and it's components can be complicated, as in-place upgrades can cause disruption of traffic. To make this easier we use Renovate to automatically create MRs to upgrade the Istio Helm Charts versions for each individual environment. We can then follow the [Istio upgrade procedure](https://gitlab.com/gitlab-com/runbooks/-/tree/master/docs/istio?ref_type=heads#upgrade-procedure) to get all components up to date.

- [x] For each component and dependency, what is the blast radius of failures? Is there anything in the feature design that will reduce this risk?
  - Istiod: The control plane manages and configures the proxies to route traffic. In a single cluster deployment model, it's important to have autoscaling enabled for istiod with multiple replicas. This will allow the control plane to continue working even if one of the pods becomes unhealthy.
    - We have defined a minimum of [2 replicas for istiod](https://gitlab.com/gitlab-com/gl-infra/k8s-mgmt/tenants/reliability/-/merge_requests/76), and the Pod Disruption Budget is set to 1 by default by the [istio helm chart](https://github.com/istio/istio/blob/master/manifests/charts/istio-control/istio-discovery/values.yaml#L220C10-L223)
  - Istio Gateways: These will be the primary entry point for traffic into the services. We have autoscaling enabled with a minimum of 2 replicas for each gateway deployment:
    - [istio-gateway](https://gitlab.com/gitlab-com/gl-infra/k8s-mgmt/tenants/reliability/-/blob/main/overlays/gke/gitlab-ops/shared/istio/istio-ingress-values.yaml#L20)
    - [istio-internal-gateway](https://gitlab.com/gitlab-com/gl-infra/k8s-mgmt/tenants/reliability/-/blob/main/overlays/gke/gitlab-ops/shared/istio/istio-internal-ingress-values.yaml#L20)

### Monitoring and Alerting

- [x] Link to the [metrics catalog](https://gitlab.com/gitlab-com/runbooks/-/tree/master/metrics-catalog/services) for the service
  - [Istio Metrics Catalog Service](https://gitlab.com/gitlab-com/runbooks/-/blob/master/metrics-catalog/services/istio.jsonnet)
  - [Istio Grafana Dashboards](https://gitlab.com/gitlab-com/runbooks/-/tree/master/dashboards/istio)
    - [Istio Control Plane Dashboard](https://dashboards.gitlab.net/d/istio-istio_control_plane/istio-istio-control-plane-dashboard)
    - [Istio Mesh Dashboard](https://dashboards.gitlab.net/d/istio-istio_mesh/istio-istio-mesh-dashboard)
    - [Istio Service Dashboard](https://dashboards.gitlab.net/d/istio-istio_service/istio-istio-service-dashboard)
    - [Istio Workload Dashboard](https://dashboards.gitlab.net/d/istio-istio_workload/istio-istio-workload-dashboard)
  - Kiali - Istio Mesh Observability Tool:
    - [gstg-gitlab-gke](https://kiali.gstg-gitlab-gke.us-east1.gitlab-staging-1.gke.gitlab.net/kiali)
    - [gstg-us-east1-b](https://kiali.gstg-us-east1-b.us-east1-b.gitlab-staging-1.gke.gitlab.net/kiali)
    - [gstg-us-east1-c](https://kiali.gstg-us-east1-c.us-east1-c.gitlab-staging-1.gke.gitlab.net/kiali)
    - [gstg-us-east1-d](https://kiali.gstg-us-east1-d.us-east1-d.gitlab-staging-1.gke.gitlab.net/kiali)
    - [ops-gitlab-gke](https://kiali.ops-gitlab-gke.us-east1.gitlab-ops.gke.gitlab.net/kiali)
    - [ops-central](https://kiali.ops-central.us-central1.gitlab-ops.gke.gitlab.net/kiali)

### Deployment

_The items below will be reviewed by the Delivery team._

- [X] Will a [change management issue](https://about.gitlab.com/handbook/engineering/infrastructure/change-management/) be used for rollout? If so, link to it here.
  - We will create a change issue when migrating each individual Gitlab service from their existing GKE ingress to Istio Ingress Gateways.

- [X] Can the new product feature be safely rolled back once it is live, can it be disabled using a feature flag?
  - Yes, we can rollback to the previous ingress method by reverting the DNS change to point to the previous GKE Ingress.

- [X] How are the artifacts being built for this feature (e.g., using the [CNG](https://gitlab.com/gitlab-org/build/CNG/) or another image building pipeline).
  - The Istio Helm Chart uses community provided images hosted in Dockerhub:
    - [istio/base](https://hub.docker.com/r/istio/base)
    - [istio/pilot](https://hub.docker.com/r/istio/pilot)
    - [istio/proxyv2](https://hub.docker.com/r/istio/proxyv2)

### Security Considerations

_The items below will be reviewed by the Infrasec team._

- [X] Link or list information for new resources of the following type:
  - DNS names:
    - [kiali fqdn](https://gitlab.com/gitlab-com/gl-infra/k8s-mgmt/components/-/blob/main/kiali/kiali-ingress.yaml?ref_type=heads#L16)
  - Entry-points exposed to the internet (Public IPs, Load-Balancers, Buckets, etc...):
    - [istio-gateway](https://gitlab.com/gitlab-com/gl-infra/k8s-mgmt/tenants/reliability/-/blob/main/overlays/gke/gitlab-staging-1/shared/istio/istio-gateway.yaml)
    - [istio-internal-gateway](https://gitlab.com/gitlab-com/gl-infra/k8s-mgmt/tenants/reliability/-/blob/main/overlays/gke/gitlab-staging-1/shared/istio/istio-internal-gateway.yaml)
  - Other (anything relevant that might be worth mention):
- [X] Were the [GitLab security development guidelines](https://docs.gitlab.com/ee/development/secure_coding_guidelines.html) followed for this feature?
- [ ] Was an [Application Security Review](https://handbook.gitlab.com/handbook/security/security-engineering/application-security/appsec-reviews/) requested, if appropriate? Link it here.
- [X] Do we have an automatic procedure to update the infrastructure (OS, container images, packages, etc...). For example, using unattended upgrade or [renovate bot](https://github.com/renovatebot/renovate) to keep dependencies up-to-date?
  - Yes. We use renovate to update the Helm Chart versions for all istio components.

### Identity and Access Management

_The items below will be reviewed by the Infrasec team._

- [X] Are we adding any new forms of Authentication (New service-accounts, users/password for storage, OIDC, etc...)?
  - Google Oauth OIDC Client for Kiali Authentication:
    - [pre kiali-google-oauth2](https://vault.gitlab.net/ui/vault/secrets/k8s/kv/env%2Fpre%2Fns%2Fistio-system%2Fkiali-google-oauth2/details?version=2)
    - [gstg kiali-google-oauth2](https://vault.gitlab.net/ui/vault/secrets/k8s/kv/env%2Fgstg%2Fns%2Fistio-system%2Fkiali-google-oauth2/details?version=1)
    - [ops kiali-google-oauth2](https://vault.gitlab.net/ui/vault/secrets/k8s/kv/env%2Fops%2Fns%2Fistio-system%2Fkiali-google-oauth2/details?version=1)
- [X] Was effort put in to ensure that the new service follows the [least privilege principle](https://en.wikipedia.org/wiki/Principle_of_least_privilege), so that permissions are reduced as much as possible?
- [X] Do firewalls follow the least privilege principle (w/ network policies in Kubernetes or firewalls on cloud provider)?
- [X] Is the service covered by a [WAF (Web Application Firewall)](https://cheatsheetseries.owasp.org/cheatsheets/Secure_Cloud_Architecture_Cheat_Sheet.html#web-application-firewall) in [Cloudflare](https://gitlab.com/gitlab-com/runbooks/-/tree/master/docs/cloudflare#how-we-use-page-rules-and-waf-rules-to-counter-abuse-and-attacks)?
  - We use Cloudflare WAF in front of the Istio Ingress for external traffic.

### Logging, Audit and Data Access

_The items below will be reviewed by the Infrasec team._

- [ ] Did we make an effort to redact customer data from logs?
- [ ] What kind of data is stored on each system (secrets, customer data, audit, etc...)?
- [ ] How is data rated according to our [data classification standard](https://about.gitlab.com/handbook/engineering/security/data-classification-standard.html) (customer data is RED)?
- [ ] Do we have audit logs for when data is accessed? If you are unsure or if using Reliability's central logging and a new pubsub topic was created, create an issue in the [Security Logging Project](https://gitlab.com/gitlab-com/gl-security/engineering-and-research/security-logging/security-logging/-/issues/new?issuable_template=add-remove-change-log-source) using the `add-remove-change-log-source` template.
  - [ ] Ensure appropriate logs are being kept for compliance and requirements for retention are met.
  - [ ] If the data classification = Red for the new environment, please create a [Security Compliance Intake issue](https://gitlab.com/gitlab-com/gl-security/security-assurance/security-compliance-commercial-and-dedicated/security-compliance-intake/-/issues/new?issue[title]=System%20Intake:%20%5BSystem%20Name%20FY2%23%20Q%23%5D&issuable_template=intakeform). Note this is not necessary if the service is deployed in existing Production infrastructure.

## Beta

### Monitoring and Alerting

_The items below will be reviewed by the Reliability team._

- [X] Link to examples of logs on https://logs.gitlab.net
  - Istio Ingress logs are being shipped to Loki and can be accessed using [Grafana](https://dashboards.gitlab.net/goto/fTI7YHVSg?orgId=1).
- [X] Link to the [Grafana dashboard](https://dashboards.gitlab.net) for this service.
  - [IstioD Metrics Catalog Service](https://gitlab.com/gitlab-com/runbooks/-/blob/master/metrics-catalog/services/istio.jsonnet)
  - [Istio Grafana Dashboards](https://gitlab.com/gitlab-com/runbooks/-/tree/master/dashboards/istio)
  - [Istio Control Plane Dashboard](https://dashboards.gitlab.net/d/istio-istio_control_plane/istio-istio-control-plane-dashboard)
  - [Istio Mesh Dashboard](https://dashboards.gitlab.net/d/istio-istio_mesh/istio-istio-mesh-dashboard)
  - [Istio Service Dashboard](https://dashboards.gitlab.net/d/istio-istio_service/istio-istio-service-dashboard)
  - [Istio Workload Dashboard](https://dashboards.gitlab.net/d/istio-istio_workload/istio-istio-workload-dashboard)

### Backup, Restore, DR and Retention

_The items below will be reviewed by the Reliability team._

- [X] Are there custom backup/restore requirements?
  - Istio is a stateless application, therefore backups are not required. We use Flux (GitOps) to deploy all Istio components, therefore as long as Flux is bootstraped on a GKE Cluster, it will reconcile and bring Istio online as configured on the [Components](https://gitlab.com/gitlab-com/gl-infra/k8s-mgmt/components/-/tree/main/istio) and [Reliability Tenants](https://gitlab.com/gitlab-com/gl-infra/k8s-mgmt/tenants/reliability/-/tree/main/overlays/gke/gitlab-ops/shared/istio) repositories.

### Deployment

_The items below will be reviewed by the Delivery team._

- [X] Will a [change management issue](https://about.gitlab.com/handbook/engineering/infrastructure/change-management/) be used for rollout? If so, link to it here.
  - We will create a change issue when migrating each individual Gitlab service from their existing GKE ingress to Istio Ingress Gateways.
- [X] Does this feature have any version compatibility requirements with other components (e.g., Gitaly, Sidekiq, Rails) that will require a specific order of deployments?
  - No. Only within Istio itself, the upgrade of the components matter. With the help of Renovate and Flux we follow the [Istio Upgrade Procedure](https://gitlab.com/gitlab-com/runbooks/-/tree/master/docs/istio?ref_type=heads#upgrade-procedure) to ensure all components are updated in the right order.
- [X] Is this feature validated by our [QA blackbox tests](https://gitlab.com/gitlab-org/gitlab-qa)?
  - No, this is an infrastructure component.
- [X] Will it be possible to roll back this feature? If so explain how it will be possible.
  - Yes, by rolling back the DNS change that points to Istio Ingress back to the previous GKE Ingress for the service.

### Security

_The items below will be reviewed by the InfraSec team._

- [ ] Put yourself in an attacker's shoes and list some examples of "What could possibly go wrong?". Are you OK going into Beta knowing that?
- [ ] Link to any outstanding security-related epics & issues for this feature. Are you OK going into Beta with those still on the TODO list?

## General Availability

### Monitoring and Alerting

_The items below will be reviewed by the Reliability team._

- [ ] Link to the troubleshooting runbooks.
- [ ] Link to an example of an alert and a corresponding runbook.
- [ ] Confirm that on-call engineers have access to this service.

### Operational Risk

_The items below will be reviewed by the Reliability team._

- [ ] Link to notes or testing results for assessing the outcome of failures of individual components.
- [ ] What are the potential scalability or performance issues that may result with this change?
- [ ] What are a few operational concerns that will not be present at launch, but may be a concern later?
- [ ] Are there any single points of failure in the design? If so list them here.
- [ ] As a thought experiment, think of worst-case failure scenarios for this product feature, how can the blast-radius of the failure be isolated?

### Backup, Restore, DR and Retention

_The items below will be reviewed by the Reliability team._

- [ ] Are there any special requirements for Disaster Recovery for both Regional and Zone failures beyond our current Disaster Recovery processes that are in place?
- [ ] How does data age? Can data over a certain age be deleted?

### Performance, Scalability and Capacity Planning

_The items below will be reviewed by the Reliability team._

- [ ] Link to any performance validation that was done according to [performance guidelines](https://docs.gitlab.com/ee/development/performance.html).
- [ ] Link to any load testing plans and results.
- [ ] Are there any potential performance impacts on the Postgres database or Redis when this feature is enabled at GitLab.com scale?
- [ ] Explain how this feature uses our [rate limiting](https://gitlab.com/gitlab-com/runbooks/-/tree/master/docs/rate-limiting) features.
- [ ] Are there retry and back-off strategies for external dependencies?
- [ ] Does the feature account for brief spikes in traffic, at least 2x above the expected rate?

### Deployment

_The items below will be reviewed by the Delivery team._

- [ ] Will a [change management issue](https://about.gitlab.com/handbook/engineering/infrastructure/change-management/) be used for rollout? If so, link to it here.
- [ ] Are there healthchecks or SLIs that can be relied on for deployment/rollbacks?
- [ ] Does building artifacts or deployment depend at all on [gitlab.com](https://gitlab.com)?
