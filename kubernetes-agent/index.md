# Kubernetes Agent Readiness Review

## Production Readiness Guide

For any new or existing system or a large feature set the questions in this guide help make the existing service more robust, and for new systems it helps prepare them and speed up the process of becoming fully production-ready.

Initially, this guide is likely to be used by Production Engineers who are embedded with other teams working on existing services and features. However, anyone working on a new service or feature set is encouraged to use this guide as well.

The goal of this guide is to help others understand how the new service or feature set may impact the rest of the (production) system; what steps need to be taken (besides deploying this new system) to ensure that it can be properly managed; and to understand what it will take to manage the reliability of the new system / feature / service beyond its' initial deployment.

## Summary

The GitLab Kubernetes Agent is an active in-cluster component for solving GitLab and Kubernetes integration tasks in a secure and cloud-native way. It enables:

* Integrating GitLab with a Kubernetes cluster behind a firewall or NAT (network address translation).
* Pull-based GitOps deployments by leveraging the GitOps Engine.
* Real-time access to API endpoints in a cluster.

More information can be found at https://docs.gitlab.com/ee/user/clusters/agent/

This service is currently in market validation. We wish to increase adoption of this new feature, especially on GitLab.com (self-managed instances can probably use an [existing feature](https://docs.gitlab.com/ee/user/project/clusters/add_remove_clusters.html), so that we can learn more about what users need, and determine next features to [build](https://gitlab.com/groups/gitlab-org/-/epics/3329).
Relevant business metrics:

- https://about.gitlab.com/handbook/product/ops-section-performance-indicators/#configureconfigure---amau---number-of-gitlab-kubernetes-agent-instances

## Architecture

```plantuml
top to bottom direction
skinparam roundcorner 20
skinparam shadowing false
skinparam rectangle {
  BorderColor DarkSlateGray
}
skinparam linetype ortho

card "Gitlab User Kubernetes Cluster" as GUKC {

  rectangle "agentk Pod" as AGENTK {
  }

}

cloud "Internet" as INTERNET {

}

card "kas.gitlab.com GCP Load Balancer" as LB {
}

rectangle "GKE Regional Cluster" as GKE {
  card "gitlab namespace" as GPRD {
    rectangle "KAS Pod" as KAS
  }

}
rectangle "Virtual Machines" as VMS {
  rectangle "GitLab.com /api" as GLAPI
  rectangle "Gitaly" as GITALY
}


AGENTK -- INTERNET
INTERNET --> LB
LB --> KAS
KAS --> GLAPI : Authn/Authz of agentk
KAS --> GITALY : Fetch data from git repo
```

### Ingress Architecture

The first layer in front of KAS is Cloudflare, but currently it is only used in raw tcp mode (not as a HTTPS proxy). This is done this way for simplicity around TLS setup (as we use Google managed certificates) and because KAS will migrate to GRPC, which needs additional testing with cloudflare (cloudflare GRPC support is in beta). It however is likely we will further change and evolve the cloudflare configuration in the future.

After cloudflare KAS uses it's own [GKE Ingress](https://cloud.google.com/kubernetes-engine/docs/concepts/ingress), currently running in HTTP1.1 with websockets. This is instead of using haproxy, and is on an entirely separate domain at https://kas.gitlab.co. The reason for such isolation is because `kas` is focussed on long-lived connections (living indefinately, always trying to remain connected). We didn't want to have `kas` connections tying up the same ingress resources that the rest of Gitlab uses (haproxy, ingress-nginx, etc). On top of this, it simplifies the deployment stack and gives us access to a number of features we can leverage including

* [Google Managed Certificate](https://cloud.google.com/kubernetes-engine/docs/how-to/managed-certs) for https://kas.gitlab.com, deployed and managed by a Kubernetes CRD in the [gitlab-extras helm release](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/-/blob/master/releases/gitlab-extras/values.yaml.gotmpl#L3-10)

* We configure the GKE Ingress (which is implemented by a GCP HTTPS Load Balancer) to use a custom HTTP healthcheck, as the readiness and liveness endpoints for `kas` live on a different port than the port used to serve traffic (due to the nature of websockets/grpc). It is also part of the [gitlab-extras helm release](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/-/blob/master/releases/gitlab-extras/values.yaml.gotmpl#L12-22)

* We also leverage [container native load balancing](https://cloud.google.com/kubernetes-engine/docs/concepts/container-native-load-balancing) to allow the GCP Load Balancer to map endpoints and route traffic **directly to the pods via their individual pod IPs**. This bypasses the usual mechanism of mapping endpoints as nodes, requiring a service being exposed as a `NodePort`, and utilising `kube-proxy` to configure `iptables` rules to route traffic in an even matter. This overall provides a much simpler network topology, with less hops. A Kubernetes `service` object is still needed for GKE/GCP to sync the endpoint list to the HTTPS load balancer, but the `ClusterIP` (or indeed any IP) of the `service` object is not actually used.

* [Google Cloud Armor](https://cloud.google.com/armor) to basically firewall/restrict the KAS GKE Ingress from only being externally accessable by cloudflare

Looking at the settings on the `BackendConfig` object for the GKE ingress, as well as the [default settings for GCP Loadbalancers](https://cloud.google.com/load-balancing/docs/https) ( [see also](https://cloud.google.com/load-balancing/docs/https#timeouts_and_retries) ), we can determine the following current settings are set

* The loadbalancer will determine the health of a pod (which is directly a loadbalancer backend) by polling port 8181 with URI `/liveness` every 5 seconds, with a timeout of 3 seconds

* Between the load balancer and the backends (pods) there is a HTTP keepalive timeout of 600 seconds which cannot be adjusted.

* Between the load balancer and the backends (pods) there is a configurable timeout which we set quite high as the connections are websockets. We currently set that to 30 minutes and 30 seconds.

* The clients have been configured to have a graceful timeout of 30 minutes, after which they will close and re-open a connection https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/blob/master/pkg/kascfg/config_example.yaml#L19

* The clients have been configured to have a keep-alive ping of 55 seconds (just under 1 minute)

* Google documentation does not reveal any maximum connection limit for their Load Balancers, only that it's limited by the capacity of your backend service (I think suggesting they can probably scale them
much higher than we can scale our infra)

As kas is behind both cloudflare and a GCP HTTPS Loadbalancer, the pods should see the `X-Forwarded-For` Header with all relevant IP addresses regardless of network path. If set by cloudflare, GCP will gracefully append its own data to it as documented [here](https://cloud.google.com/load-balancing/docs/https#x-forwarded-for_header)

### Agent, KAS, and Rails Architecture

See https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/blob/master/doc/architecture.md#high-level-architecture

We have two components for the Kubernetes agent:

- The GitLab Kubernetes Agent Server (`kas`). This is deployed server-side together with the GitLab web (Rails), and Gitaly. It's responsible for:
  - Accepting requests from `agentk`.
  - [Authentication of requests](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/blob/master/doc/identity_and_auth.md) from `agentk` by querying `GitLab RoR`.
  - Fetching agent's configuration from a corresponding Git repository by querying `Gitaly`.
  - Polling manifest repositories for [GitOps support](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/blob/master/doc/gitops.md) by talking to `Gitaly`.
- The GitLab Kubernetes Agent (`agentk`). This is deployed to the user's Kubernetes cluster. It is responsible for:
  - Keeping a connection established to a `kas` instance
  - Processing GitOps requests from `kas`

## Operational Risk Assessment

### Use cases

1. The user can configure an agent by committing a configuration file in a GitLab project. We refer to this project as the [configuration repository](https://docs.gitlab.com/ee/user/clusters/agent/#define-a-configuration-repository).
  1. `kas` will then periodically poll Gitaly for configuration updates for each connected agent. When there is a new commit, `kas` fetches the agent's configuration file from the configuration repository, and sends it to the corresponding agent. Agent then configures itself with the new configuration.
1. The user specifies in the configuration file which `manifest_projects` to sync. These manifest projects are other GitLab projects. The agent needs to be authorized to read the repository of a manifest project. The user then commits YAML definitions for Kubernetes resources into each manifest project.
  1. For each connected agent, `kas` polls Gitaly for the manifest repositories, and sends the latest manifests to the agent. The agent then applies the latest manifest to the Kubernetes cluster.

For the first release, note that we will only sync one file per manifest project, and this file must be named `manifest.yaml`. Additionally, only public projects are allowed as manifest projects for the first release.

### Scalability

1. The `kas` chart is configured by default to autoscale by using a [HorizontalPodAutoscaler](https://gitlab.com/gitlab-org/charts/gitlab/-/blob/master/charts/gitlab/charts/kas/templates/hpa.yaml). The HorizontalPodAutoscaler is configured to target an average value of 100m CPU. It will initially default to two pods, with the ability to scale up to a maximum of ten. These settings will be reviewed and adjusted later as needed https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/1548

1. The current implmentation of the liveness check simply returns a HTTP 200 OK, so is only reliabile for basic determination of a pods health https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/blob/master/internal/module/observability/metric_server.go#L59-72 . Likewise the chart configuration uses basic TCP connectivity for readiness and liveness checks https://gitlab.com/gitlab-org/charts/gitlab/-/blob/master/charts/gitlab/charts/kas/templates/deployment.yaml#L61-70

### Dependencies

1. Cloudflare, which is in front of the kas GKE Ingress
1. GCP HTTPS Load Balancer, is used to load balance requests between the `agentk` (and the internet)  and `kas`.
1. GitLab Web (Rails) server, which serves the internal API for `kas`.
1. Gitaly, which provides repository blobs for the agent configuration, and K8s resources to be synced.

### Operational Risks

1. Abuse of many concurrent connections, affecting the `kas` service or blast radius such as GitLab or Gitaly. As KAS is behind cloudflare (though only in TCP mode), we can leverage all the standard controls
and mechanisms we have with cloudflare to stop abuse. Internal rate limiting by IP has been developed and will be rolled out at a later date https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/12519
### Controls

1. The internal API for `kas` can be disabled using a feature flag (`kubernetes_agent_internal_api`). When this flag is disabled, `kas` will see 404s when accessing the internal API. There might be a delay when `kas` sees 404s depending on cache configurations. The `agentk` clients will receive no indication that the flag is disabled.

## Database

There is no database for either the `kas`, nor the `agentk` components.

Note that we are considering Redis in the future - please see https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/90

## Security and Compliance

An initial security review was done at https://gitlab.com/gitlab-com/gl-security/appsec/appsec-reviews/-/issues/30 and the summary is as follows

1. The team audited the `gitlab-agent` codebase from the `kas` part of the source code. They also audited the `agentk` to local cluster communication, and `agentk` to `kas` communication.
1. The team noted "The data flow within kas makes a good impression with respect to security practices. The only information which comes from the agent is the agent token. All other information is pulled from the GitLab API. This helps a lot to avoid logic errors and bypasses based on input from the agent. "
1. While currently every agent uses a generated token to authenticate itself to Gitlab, further expansion is needed on the authentication and authorization model of `kas` in order to better control which agent has access to which repositories (inside the users permissions structure). This is being tracked in https://gitlab.com/gitlab-org/gitlab/-/issues/220912

## Performance

For the initial rollout, rate limiting is not necessary. Setting up rate limiting for production is [planned](https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/12519). A rate limit on a per client basis can be configured with the `connections_per_token_per_minute` setting] - the default is 100 new connections per minute per agent. This requires Redis in order to track connections per agent. This rate limiting was introduced in https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/103.

The frequency of gRPC calls from `kas` to `Gitaly` can be configured (see https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/blob/master/pkg/kascfg/config_example.yaml).

The rate limiting of calls to Gitaly has been tested, [and the results are documented here](https://gitlab.com/gitlab-org/gitlab/-/issues/297682#note_507982184). In short, the results were the following

* The test was done in our staging environment, against a 2 pod kas deployment, with 1000 agentk (client) pods, syncing from a single git repository (thus on a single Gitaly node)

* As `agentk` polls every 20 seconds, it was expected that it would at most take 20 seconds for each `agentk` to pick up the change. Instead it took up to 10 minutes for all `agentk` to obtain the change. This was not due to resourcing
of kas itself, but rather the built in rate limiting in kas to prevent overloading gitaly

* While this does affect KAS "responsiveness", it does prevent overloading Gitaly, which is working as expected, and is good safety for Gitlab.com

## Backup and Restore

N/A as components are stateless

## Monitoring and Alerts

### Kibana

Select the pubsub-kas-inf-gprd-* index pattern. (pubsub-kas-inf-gstg-* for staging)

staging: https://nonprod-log.gitlab.net/goto/9f205372ad310869528fc2cb5336baff

production: https://log.gprd.gitlab.net/goto/33a5e2d548b67b2247de5aa8169c47e8

### Grafana Dashboards

Kubernetes Pods : httpis://dashboards.gitlab.net/d/kubernetes-pods/kubernetes-pods?orgId=1&var-datasource=Global&var-cluster=gstg-gitlab-gke&var-namespace=gitlab

Kube container detail : https://dashboards.gitlab.net/d/kas-kube-containers/kas-kube-containers-detail?orgId=1&var-PROMETHEUS_DS=Global&var-environment=gstg&var-stage=main

https://dashboards.gitlab.net/d/kas-kube-deployments/kas-kube-deployment-detail?orgId=1&var-PROMETHEUS_DS=Global&var-environment=gstg&var-stage=main

KAS pod detail: https://dashboards.gitlab.net/d/kas-pod/kas-pod-info?orgId=1&var-PROMETHEUS_DS=Global&var-environment=gstg&var-cluster=gstg-gitlab-gke&var-stage=main&var-namespace=gitlab&var-Node=All&var-Deployment=gitlab-kas

Overview and SLIs : https://dashboards.gitlab.net/d/kas-main/kas-overview?orgId=1&var-PROMETHEUS_DS=Global&var-environment=gstg&var-stage=main&var-sigma=2&from=now-15m&to=now

### Thanos Queries

Metrics are being collected from kas via the prometheus job name `gitlab-kas`. E.g. for staging https://thanos-query.ops.gitlab.net/graph?g0.range_input=1h&g0.max_source_resolution=0s&g0.expr=%7Bjob%3D%22gitlab-kas%22%2C%20env%3D%22gstg%22%7D&g0.tab=1

E.g. Total agent connections - https://thanos-query.ops.gitlab.net/new/graph?g0.expr=sum(grpc_server_requests_in_flight%7Bapp%3D%22kas%22%2C%20grpc_method%3D%22GetConfiguration%22%7D)&g0.tab=0&g0.stacked=0&g0.range_input=12h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D&g0.end_input=2021-02-01%2000%3A54%3A18&g0.moment_input=2021-02-01%2000%3A54%3A18

Observability is continued to be worked on in https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/12156

The initial SLA for this service will be targeting error rates on the `GetConfiguration()` gRPC method, which should be under 1%. After launch, Configure will implement the metrics necessary for a [more relevant SLA](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/issues/90).

## Responsibility

The [Configure group](https://gitlab.com/groups/gitlab-org/configure/-/group_members?with_inherited_permissions=exclude) are the subject matter experts for this service.

`@nicholasklick`, `@tkuah`, and `@ash2k` are available to be on-call for the launch.

## Testing

The [project](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent) has extensive unit tests that are run in GitLab's CI pipelines.

Automatic GitLab QA end-to-end tests are planned in https://gitlab.com/groups/gitlab-org/-/epics/4949. We have [detailed manual steps for QA](https://gitlab.com/gitlab-org/charts/gitlab/-/blob/master/doc/charts/gitlab/kas/index.md#developement-how-to-manual-qa) for the chart.

There has been no load testing done on this service.

