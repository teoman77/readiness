---
layout: handbook-page-toc
title: "Blueprint: Enabling checksums on the database of GitLab.com, how to detect possible data corruption.
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Resources

Epic: [Postgresql Enabling Checksums](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/171)

## Idea or problem statement


Data corruption refers to the data errors that happen when the computer reads, writes, stores, transmits, or processes it. Those errors introduce unintentional changes to the original data.
When data corruption occurs, let's suppose in a file, we would be in a scenario of unexpected results when accessed by the system or an application; that might be from a minor data loss to a system crash.

Those errors can happen due to :

 - Bugs in the OS or application.
 - Hardware issues.
 - Wrong software administration . e.g., update directly any PostgreSQL data file when the database is open.

### How to detect data corruption in PostgreSQL

In our company values, avoiding data loss is a major factor in our [decision-making](https://about.gitlab.com/handbook/engineering/#prioritizing-technical-decision). Considering a relational database, an essential component, we need to verify how to add all the mechanisms to avoid or detect any possible data corruption as soon as possible, to restore or mitigate the problem.

In PostgreSQL, when we create an empty database, since version 9, we could enable the flag data-checksums.

When we enable checksums, we are adding a small integer checksum to each data page, every time we read this page, the checksum is recalculated and compared with the value saved on the data page.  

This feature’s main benefit will be adding checksums on the data pages to detect any data corruption when interacting with the PostgreSQL buffer cache. Without checksums, data corruption could be happening without any notification. 

After enabling checksums would imply a possible performance degradation from 2% to 3%. We have some analysis of the performance impact in staging at this [issue.](https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/11534#note_454998499)

The [PostgreSQL native feature to enable checksums](https://www.postgresql.org/docs/12/app-pgchecksums.html) in an existing database was implemented in the major version 12.

Currently we are on the major version 11 and we will use the [pg_checksums project](https://github.com/credativ/pg_checksums), that allow us to enable checksums.

Our current PostgreSQL database cluster does not have the flag data-checksums enabled since this would imply longer downtime. We will explain the process in the design document on how to enable the checksums without downtime. 

We decided to enable checksums using pg_checksums, because it is the best solution in the market to enable checksums in Postgresql. If we consider that PostgreSQL in the major version 12, added the feature of enable checksums from databases that started without, it is important. Additionally looking to all the major enterprise consultancies and community it is a standard to enable this feature in new databases. Our case our cluster is from before this feature so now is a great moment to enable it. We do not recommend to maintain the checksum integrity check on the filesystem, since we have a much more detailed troubleshooting in PostgreSQL, as explained in the desing. Also we are agnostic from OS, filesystem and hardware, that would be easier to migrate.


