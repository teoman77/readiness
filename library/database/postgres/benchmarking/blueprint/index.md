---
layout: handbook-page-toc
title: "Blueprint: Database benchmarks
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Resources

Epic: [Create a database benchmark environment](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/274)

## Idea or problem statement

Currently, it is hard to calculate the risks of a configuration change in our database ecosystem. Considering the high level of traffic we have in our primary database, we need to properly test any changes before rolling them out in production.

The goal is to have an environment that would simulate the real traffic in a similar environment to production. When in this environment, we will have the same hardware and software specifications.
 
In this test environment we will be able to: 
 
- Benchmark what is the maximum throughput that our current setup can manage.
- Benchmark changes and verify if the real goal is achieved.
- Benchmark and execute regression tests between different parameters, versions of software and OS.


There are three points that we can explore in detail: 

- Improving the production rollout, where we can execute tests until we reduce the impact of the change that we are executing. The goal is to smoothly implement a change that would not affect our production traffic, by any blip or change in the response time to the database.

- Evaluating the side effects of the change, the goal is to know the performance of the database when it is at a peak time. It is not necessary to have an issue in production to visualize the side effects of some changes. Since we have the correct framework to test the changes and benchmark the results.

- Evaluating a possible rollback plan. As important as it is to execute changes in production minimizing the impact, the rollbacks have to respect the same criteria, reducing the impact and the time to restore performance.


The approach consists of reproducing two workloads from the production traffic, based on:

* Most executed queries. Trying to simulate the statements that are executed more often in the production workload.
 
* Most time-consuming queries. Trying to simulate the statements that are consuming more time to be resolved in the production workload.

Both workloads were generated after an investigation of patterns based on the collected data from the production environment with the extension `pg_stat_statements`.

Our workloads will not consider 100% of the statements from production. It would require an enormous amount of time, so at the moment we are considering 85% of the total workload to initiate our tests. 
Additionally, we need a database with the volume and data from production to execute the proper iteration. To properly simulate the production traffic we are using production IDs. Our goal is to simulate the traffic at a similar level, considering eventually the test would execute trivial operations to disk to fetch and update data.

The tool that we are using for this project is JMeter, since we can easily simulate workload, controlling the number of threads connecting to the database and the number of workloads.


### Summary of past efforts

In the past, we executed some regression tests between the versions of PostgreSQL for our last upgrade. 
We collected a reduced set of the 10 statements most executed and the top 10 total time consumers from the workload on the database.
Our results were positive and we could proceed with the upgrade without fear of any major performance regression.


### Costs

We estimate this project to be developed with a cost of 90 days of engineering work.


