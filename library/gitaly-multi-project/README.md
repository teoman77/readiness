# Gitaly Multi Project

Epic: <https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/935>

[[_TOC_]]

## Introduction

As part of the [Disaster Recovery Working Group](https://about.gitlab.com/company/team/structure/working-groups/disaster-recovery/)
we will be splitting the Gitaly VMs across multiple GCP projects. Each project will have 9 Gitaly VMs spread evenly between 3 zones.

This is to prevent network throttling when we are taking snapshots and
recovering from snapshots, and multiple projects will give us a high number of
concurrency on snapshots, which will improve our RTO target.

We will be migrating project repositories from the current Gitaly VMs to the
new ones with no downtime. We will have a service running the migration in
the background to drain storages 1 by 1, and we'll have the possibility to
increase the concurrency of how many repositories we are migrating. When we are
moving a repository from the current storage to the new one it will be
temporarily set to read-only.

## Architecture

<details>
<summary> Click to expand staging architecture </summary>

![overview of staging infrastructure](./img/gstg.png)

[gstg.excalidraw](./img/gstg.excalidraw)

</details>

<details>
<summary> Click to expand production architecture </summary>

![overview of production infrastructure](./img/gprd.png)

[gprd.excalidraw](./img/gprd.excalidraw)

</details>

## Naming Convention

We are restructuring our Gitaly instances to be hosted in multiple projects.
This presents an opportunity to establish a standardized naming convention for
both storage servers and virtual machines (VMs).

Currently, the naming convention carries some historical baggage from the time
when Gitaly service did not exist and NFS servers were used:

- Storage: `nfs-file01`
- FQDN/Hostname: `file-01-stor-gprd.c.gitlab-production.internal`

New Naming Convention:

- GCP project name hosting Gitaly: `gitlab-gitaly-<env>-<uid>`
  - `env`: either `gstg` or `gprd`.
  - `uid`: a randomly generated alphanumeric string as unique identifier.
- VM hostname/fqdn: `gitaly[-<shard>]-<id>-<tier>-<env>.c.<project>.internal`
  - `shard`: optional parameter representing the shard, which can be default,
    praefect, or hdd (if not specified, default is assumed).
  - `id`: an incrementing 2 digit number like `01`, `02`.
  - `tier`: `stor`
  - `env`: either `gstg` or `gprd`
  - `project`: the GCP project name
- Storage name: `[<uid>-]gitaly[-<shard>]-<instance-id>-<tier>-<env>.c.<project>.internal`
  - `uid`: an optional randomly generated alphanumeric string used only when a
    single VM has multiple disks attached to it
  - `shard`: optional parameter representing the shard, which can be default,
    praefect, or hdd (if not specified, default is assumed)
  - `instance-id`: matches the id of the VM hosting the Gitaly service
  - tier: `stor`
  - env: either `gstg` or `gprd`
  - project: the GCP project name

Examples:

- GCP project: `gitlab-gitaly-gprd-e824`
- FQDN/Hostname: `gitaly-01-stor-gprd.c.gitlab-gitaly-gprd-e826.internal`
- Storage name: `gitaly-01-stor-gprd.c.gitlab-gitaly-gprd-e826.internal`

## Migrating to new infrastructure

We currently have
[1.5PB](https://thanos.gitlab.net/graph?g0.expr=sum%20(node_filesystem_size_bytes%7Benv%3D%22gprd%22%2C%20mountpoint%3D%22%2Fvar%2Fopt%2Fgitlab%22%2C%20type%3D%22gitaly%22%7D%20-%20node_filesystem_avail_bytes%7Benv%3D%22gprd%22%2C%20mountpoint%3D%22%2Fvar%2Fopt%2Fgitlab%22%2C%20type%3D%22gitaly%22%7D)&g0.tab=1&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D)
of Git data in one project. The plan is to move this data to separate GCP
projects to avoid network congestion when taking disk snapshots and recovering
from them. This migration will improve our RPO and RTO times for Gitaly.

### Requirements

1. Use the [move project API](https://docs.gitlab.com/ee/api/project_repository_storage_moves.html#schedule-a-repository-storage-move-for-a-project),
   [move Group Wiki API](https://docs.gitlab.com/ee/api/group_repository_storage_moves.html#schedule-a-repository-storage-move-for-a-group),
   and [move Snippets API](https://docs.gitlab.com/ee/api/snippet_repository_storage_moves.html#schedule-a-repository-storage-move-for-a-snippet)
    - Safest, and async.
    - Handles Rails database migration.
    - Battle-tested, but not at a large scale.
1. We need to have permanent log storage of the migrations:
    - Can't use GitLab CI/CD
    - Job logs are maxed at a specific number of bytes.
    - We need to run this 24/7 to migrate nodes safely.
1. Health checks after the project has finished migration:
    - Repository is in read/write mode
    - Requests are succeeding
    - No corruption on the git data.
1. Able to drain single storage in 1 Go just by specifying the node, no need to
   give it a list of projects, it will find it on its own.
    - Can move a single repo or multiple concurrently.
    - Can move single storage or multiple concurrently.
1. Able to skip a single repository just in case it's problematic and for it not
   to block the migration.
1. Show progress of how much we've drains from a storage node.

#### Scrapped ideas

1. [Run housekeeping before we move a repository](https://gitlab.com/gitlab-org/gitaly/-/issues/5399) to save on storage and transfer time.
    - We can only [start housekeeping](https://docs.gitlab.com/ee/api/projects.html#start-the-housekeeping-task-for-a-project) but don't know when it finished.
    - Increase the risk of migration since we are running more things that could go wrong, even if we execute it after we migrate it.
    - We already have a daily maintenance task that does this, even though it will never go through all of the repositories.
1. Adding a long sidekiq job inside of the monolith that will drain these projects
    - We didn't want it to be interupted by deployments on the monolith.
    - Deploying this inside of the monolith would have slowed iteration because
      deployment takes so long. We've never done a migration this large so we
      have a lot of unkown unkowns and need to be able to react fast.
    - If we release this in the product we have to take into consideration
      backwards compatibility..
    - Building a tool for GitLab.com will help educate us what is actually
      needed for the product. We need to experiment outside of the critical
      path.
    - Team has little ruby experices and we need high concurrent requests for
      migrating this large amount of projects which Go is a better fit.

### Solution

1. Create a new [woodhouse](https://gitlab.com/gitlab-com/gl-infra/woodhouse) command `gitalyctl storage drain` that starts a daemon
   process:
    - A new kubernetes deployment for woodhouse will be created to start this process.
    - Read a `config.yaml` file from a config map and specify the storage servers and the concurrency values.
    - The service is stateless and can be restarted at any time.
    - To update which storages we are migrating we'll update a config map
      through a merge request.
    - Favour [Woodhouse](https://gitlab.com/gitlab-com/gl-infra/woodhouse/)
      over a dedicated GitLab project because woodhouse is the repository for
      infrastructure tooling. If customers ask to have `gitalyctl` as a
      standalone binary that we distribute for them we can extract the codebase
      out of `woodhouse` in the future. At the moment infrastructure doesn't
      have the bandwidth to do so. If customers really want to use it they can
      always build woodhouse if they want.
1. `gitalyctl drain storage` will drain group wikis by sending requests to [GET api/v4/groups?repository_storage=XXX&statistics=true](https://docs.gitlab.com/ee/api/groups.html#list-groups)
    - Get the group ID and then send a request to [`POST /groups/:group_id/repository_storage_moves`](https://docs.gitlab.com/ee/api/group_repository_storage_moves.html#schedule-a-repository-storage-move-for-a-group).
      We won't specify the `destination_storage_name` since rails will
      automatically use the new Gitaly nodes since we'll set the weight of the
      old nodes to 0.
    - Poll [`GET
      /groups/:group_id/repository_storage_moves/:repository_storage_id`](https://docs.gitlab.com/ee/api/group_repository_storage_moves.html#get-a-single-repository-storage-move-for-a-group)
      to get the status of the move until it is `state: finished`.
1. `gitaly drain storage` will drain snippets by sending requests to [GET api/v4/snippets/all](https://docs.gitlab.com/ee/api/snippets.html#list-all-snippets)
    - Get the snippet ID and then send a request to [POST api/v4/snippets/:snippet_id/repository_storage_moves](https://docs.gitlab.com/ee/api/snippet_repository_storage_moves.html#schedule-a-repository-storage-move-for-a-snippet).
      We won't specify the `destination_storage_name` since rails will
      automatically use the new Gitaly nodes since we'll set the weight of the
      old nodes to 0.
    - Poll [`GET /snippets/:snippet_id/repository_storage_moves/:repository_storage_id`](https://docs.gitlab.com/ee/api/snippet_repository_storage_moves.html#get-a-single-repository-storage-move-for-a-snippet)
      to get the status of the move until it is `state: finished`.
1. `gitalyctl drain storage` will drain project repositories by sending requests to
   [GET /api/v4/projects?repository_storage=XXX&statistics=true](https://docs.gitlab.com/ee/api/projects.html#list-all-projects)
    - Get the project ID and then send a request to [`POST
      /projects/:project_id/repository_storage_moves`](https://docs.gitlab.com/ee/api/project_repository_storage_moves.html#schedule-a-repository-storage-move-for-a-project).
      Don't specify `destination_storage_name` since rails will automatically
      use the new Gitaly nodes since we'll set the weight of the old nodes to
      be 0.
    - Poll [`GET
      /projects/:project_id/repository_storage_moves/:repository_storage_id`](https://docs.gitlab.com/ee/api/project_repository_storage_moves.html#get-a-single-repository-storage-move-for-a-project)
      to get the status of the move until it is `state: finished`.
1. Implement `--dry-run` that will only print out the projects that it will
   migrate, and their `statistics.repository_size`, `repository_storage` so we
   can check if we are picking the right projects both for local development
   and the production deployment.
1. Allow to specify the concurrency on 2 levels, these can be flags or specified in the `config.yaml`:
    - Number of storages that it will drain concurrently.
    - Number of repositories that it will move from single storage per repository type like `project`, `group`, and `snippets`.
    - Updating these values would require a process restart unless we make it
      check the configuration fields to see if there was an update.
1. Provide Prometheus metrics on the following
    - Success project moves counter with label for storage name
    - Failed project moves counter with label for storage name
    - Project move duration histogram with label for storage name
    - Current number of projects we are migrating withlabel for storage name
    - Pagination information to show progress with label for storage name
        - `x-page`
        - `x-per-page`
    - Concurrency on storage.
    - Concurrency on repositories
    - Create dashboard to visualize this information.
1. Provide logs for the following
    - Source repository storage
    - Target repository storage
    - Status of Move
    - repository size
1. Process termination should be handled gracefully. When `SIGTERM` is sent to
   the process, it should stop looping for the projects to migrate and finish
   the polling of the on-going migrations to make sure they are successful.
    - Since a migration can take a few minutes that means that we have to
      update the Kubernetes lifecycle to wait at least a few minutes before it
      sends the `SIGKILL`
1. Specify a list of projects that we ignore, so if they are problematic and
   require investigation we can skip them and unblock the migration.
1. To get the progress of how far we are in the migration our single source of
   truth is going to be our database by running the following queries.

    ```sql
    -- old storage project count for single repositories
    SELECT COUNT(*), p.repository_storage
    FROM projects p
    LEFT JOIN fork_network_members fnm ON p.id = fnm.project_id OR p.id = fnm.forked_from_project_id
    WHERE p.repository_storage LIKE 'nfs-file%'
        AND fnm.project_id IS NULL
        AND fnm.forked_from_project_id IS NULL
    GROUP BY p.repository_storage;

    -- new storage project count for single repositories
    SELECT COUNT(*), p.repository_storage
    FROM projects p
    LEFT JOIN fork_network_members fnm ON p.id = fnm.project_id OR p.id = fnm.forked_from_project_id
    WHERE p.repository_storage LIKE 'gitaly-%'
        AND fnm.project_id IS NULL
        AND fnm.forked_from_project_id IS NULL
    GROUP BY p.repository_storage;

    -- old storage project count
    SELECT COUNT(*), repository_storage FROM projects WHERE repository_storage LIKE 'nfs-file%' GROUP BY repository_storage;

    -- new storage project count
    SELECT COUNT(*), repository_storage FROM projects WHERE repository_storage LIKE 'gitaly-%' GROUP BY repository_storage;
   ```

   These query take a while to execute so we'll need to run them on the archive
   DB rather then on the production one which will have some delays.

<details>
<summary> example config.yml </summary>

```yml
src_storage:
- nfs-file01
- nfs-file02

concurrency:
  repository: 2
  storage: 2

ignored_projects:
- 1234
- 5678
```

</details>

### Migration Process

The migration will be split into 2 phases. The first phase will migrate the
single repositories that don't have forks, then the second phase will migrate
the pooled repositories, which are repositories that are in a fork network. The
reason for this is becuase the [move
API](https://docs.gitlab.com/ee/api/project_repository_storage_moves.html#schedule-a-repository-storage-move-for-a-project)
does not support pooled repositories work and there is a lot of work to make it
work, so we'll be splitting it into two phases not to block the migration.

#### Staging

We need to migrate all this Git data to the new Gitaly servers with little
disruption.

##### Infrastructure (gstg)

Our Gitaly setup in staging doesn't match production at all and we already have
<https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/874> that tracks this
work. While we are building the new infrastructure we should try and tackle
some of them. Some of the resources are also wasteful

Free Disk space: <https://thanos.gitlab.net/graph?g0.expr=node_filesystem_free_bytes%7Benv%3D%22gstg%22%2C%20type%3D%22gitaly%22%2C%20mountpoint%3D%22%2Fvar%2Fopt%2Fgitlab%22%7D%20%2F%201024%20%2F1024%2F1024&g0.tab=1&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D>

<details>
<summary> current Gitaly machines </summary>

```shell
$ gcloud --project='gitlab-staging-1' compute instances list --filter='labels.type=gitaly' --format='table(name, disks[].deviceName, disks[].diskSizeGb, machineType)'

NAME                            DEVICE_NAME                                        DISK_SIZE_GB          MACHINE_TYPE
file-praefect-cny-03-stor-gstg  ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '2000', '50']  t2d-standard-32
file-01-stor-gstg               ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '2000', '50']  n2-standard-32
file-02-stor-gstg               ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '2000', '50']  n2-standard-32
file-03-stor-gstg               ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '2000', '50']  n2-standard-32
file-04-stor-gstg               ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '2000', '50']  n2-standard-32
file-05-stor-gstg               ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '2000', '50']  n2-standard-32
file-06-stor-gstg               ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '2000', '50']  n2-standard-32
file-cny-01-stor-gstg           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '2000', '50']  t2d-standard-32
file-hdd-01-stor-gstg           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '2000', '50']  n2-standard-8
file-praefect-cny-01-stor-gstg  ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '2000', '50']  t2d-standard-32
file-praefect-cny-02-stor-gstg  ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '2000', '50']  t2d-standard-32
```

</details>

New Infrastructure:

1. `default` shard (main)
    - Disk: 2TB `pd-balanced` from (`pd-ssd`)
       - Keep the same total amount of disk space in `default` shard
       - Loose some read/write throughput because we are moving from
         `pd-ssd` to `pd-balanced`. However, we'll have uniformity with
         production.
    - Compute: `t2d-standrd-32`
        - Keep the same machine since we are using [50GB+ for file
          cache](https://dashboards.gitlab.net/d/bd2Kl9Imk/host-stats?orgId=1&var-env=gstg&var-node=file-01-stor-gstg.c.gitlab-staging-1.internal)
          and don't want to make this migration riskier because we'll have
          worse performance.
    - 2 GCP projects
    - 2 nodes per project
1. `default` shard (cny):
   - 1 GCP project
   - 1 node per project
   - `pd-balanced` 500GB disk
       - We only have 40GB
1. `hdd` shard: This shard will be ignored as part of this migration, it will be covered in <https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/902>
1. `praefect` shard: This shard will be ignored since praefect will not leverage
   snapshots since [it's a bad
   idea](https://docs.gitlab.com/ee/administration/gitaly/#snapshot-backup-and-recovery-limitations)
   and has a different [recovery
   process](https://docs.gitlab.com/ee/administration/gitaly/recovery.html#replace-an-existing-gitaly-node)

##### Single Repositories Migration (gstg)

###### `default` shard (gstg cny)

Since this is a single server hosting `gitlab-org/gitlab` and its forks only we
won't use the [move
api](https://docs.gitlab.com/ee/api/project_repository_storage_moves.html#schedule-a-repository-storage-move-for-a-project)
to move 4TB in a single request. Using a disk snapshot is faster and safer.
This will result in downtime, but we can do this over the weekend to have
minimal impact on engineering productivity.

Schedule maintenance might be required to announce to the rest of the
customers.

A rough migration plan:

1. Create a snapshot.
1. Shutdown Gitaly.
1. Create an incremental snapshot from the previous one.
1. Create a new VM using recovering from the snapshot above.
1. Run some manual QA.

###### `default` shard (gstg main)

1. Create the new `default` nodes
1. Update the weight of old Gitaly nodes to `0` so no new projects get assigned
   to them.
1. Verify that project creation is working as expected.
    - Running a full QA pipeline might be the best.
1. Migrate `nfs-file24` with a concurrency.repository of 1 (1 storage, 1 repository)
    - Measure the time it takes to migrate X amount of GB
    - Validate that the migrated projects are working as expected and are read/write
1. Migrate `nfs-file20` with a concurrency.repository of 2 (1 storage, 2 repositories)
    - Measure the time it takes to migrate X amount of GB
    - Validate that the migrated projects are working as expected and are read/write
1. Migrate `nfs-file[19,18]` with a concurrency.repository of 2 and
   concurrency.storage of 2 (2 storage, 2 repositories)
    - Measure the time it takes to migrate X amount of GB
    - Validate that the migrated projects are working as expected and are read/write
1. Migrate `nfs-file[01-17]` with higher concurrency if our system scales well, by
   gathering data from before.
    - The higher the concurrency.repository the more CPU usage on Gitaly servers.

###### `hdd` shard (gstg)

This shard will be ignored as part of this migration, it will be covered in
<https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/902>

###### `praefect` node (gstg)

This shard will be ignored since praefect will not leverage snapshots since [it's
a bad
idea](https://docs.gitlab.com/ee/administration/gitaly/#snapshot-backup-and-recovery-limitations)
and has a different [recovery
process](https://docs.gitlab.com/ee/administration/gitaly/recovery.html#replace-an-existing-gitaly-node)

#### Production

##### Infrastructure (gprd)

<details>
<summary> current Gitaly machines </summary>

```shell
~ gcloud --project='gitlab-production' compute instances list --filter='labels.type=gitaly' --format='table(name, disks[].deviceName, disks[].diskSizeGb, machineType, labels.shard)'
NAME                        DEVICE_NAME                                        DISK_SIZE_GB           MACHINE_TYPE     SHARD
file-102-stor-gprd          ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-21-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-24-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-27-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-30-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-33-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-36-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-39-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-42-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-45-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-48-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-51-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-54-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-57-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-60-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-63-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-66-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-69-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-72-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-75-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-78-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-81-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-84-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-87-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-90-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-93-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-96-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-99-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-hdd-03-stor-gprd       ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-8    default
file-hdd-06-stor-gprd       ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-8    default
file-hdd-09-stor-gprd       ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-8    default
file-marquee-03-stor-gprd   ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '4000', '50']   n1-standard-32   default
file-praefect-03-stor-gprd  ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   praefect
file-01-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-02-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-03-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-04-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-05-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-06-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-07-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-08-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-09-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-10-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-100-stor-gprd          ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-11-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-12-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-13-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-14-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-15-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-16-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-17-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-18-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-19-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-20-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-22-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-25-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-28-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-31-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-34-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-37-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-40-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-43-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-46-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-49-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-52-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-55-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-58-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-61-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-64-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-67-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-70-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-73-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-76-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-79-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-82-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-85-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-88-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-91-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-94-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-97-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-cny-01-stor-gprd       ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '8000', '50']   t2d-standard-32  default
file-hdd-01-stor-gprd       ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-8    default
file-hdd-04-stor-gprd       ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-8    default
file-hdd-07-stor-gprd       ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-8    default
file-hdd-10-stor-gprd       ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-8    default
file-marquee-01-stor-gprd   ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '4000', '50']   n1-standard-32   default
file-praefect-01-stor-gprd  ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   praefect
file-101-stor-gprd          ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-23-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-26-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-29-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-32-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-35-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-38-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-41-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-44-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-47-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-50-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-53-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-56-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-59-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-62-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-65-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-68-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-71-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-74-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-77-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-80-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-83-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-86-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-89-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-92-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-95-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-98-stor-gprd           ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   default
file-hdd-02-stor-gprd       ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-8    default
file-hdd-05-stor-gprd       ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-8    default
file-hdd-08-stor-gprd       ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-8    default
file-marquee-02-stor-gprd   ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '4000', '50']   n1-standard-32   default
file-praefect-02-stor-gprd  ['persistent-disk-0', 'persistent-disk-1', 'log']  ['40', '16000', '50']  n1-standard-32   praefect
```

</details>

New infrastructure:

1. `defaut` (main) shard:
    - Disk: 16TB `pd-balanced` (from `pd-ssd`)
        - <https://gitlab.com/gitlab-com/gl-infra/reliability/-/issues/16314>
    - Compute: `t2d-standard-32`
        - Brings cost savings projected in <https://gitlab.com/gitlab-com/gl-infra/reliability/-/issues/23929>
    - 12 GCP projects
        - 9 nodes in each project
        - 3 per zone
        - We might need more projects for natural growth
1. `default` (cny) shard:
    - Disk: 8TB `pd-ssd` (from `pd-ssd`)
        - We will lose IOPS if we move to `pd-balanced`, and `cny` is very performance sensitive.
            - Disk performance <https://cloud.google.com/compute/docs/disks/performance#t2d_vms>
            - Throughput (MB/s):  2240 = (8000 * 0.28). Exceeding instance max (1200)
            - Read IOPS: 48000 = (8000 * 6) (50k instance max)
            - Write IOPS: 48000 = (8000 * 6) (50k instance max)
    - Compute: `t2d-standard-32`
    - 1 GCP project
1. `hdd` shard: This shard will be ignored as part of this migration, it will be
   covered in <https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/902>
1. `marquee` shard: This will be migrated to the `default` shard since this was an
   experiment we ran where we put our premium customers in a few Gitaly servers
   which resulted in a high blast radius.
1. `praefect` shard: This shard will be ignored since praefect will not leverage
   snapshots since [it's a bad
   idea](https://docs.gitlab.com/ee/administration/gitaly/#snapshot-backup-and-recovery-limitations)
   and has a different [recovery
   process](https://docs.gitlab.com/ee/administration/gitaly/recovery.html#replace-an-existing-gitaly-node)

##### Repositories Migration (gprd)

###### `default` shard (gprd cny)

Since this is a single server hosting `gitlab-org/gitlab` and its forks and no
other repository we won't use the [move
api](https://docs.gitlab.com/ee/api/project_repository_storage_moves.html#schedule-a-repository-storage-move-for-a-project)
to move 4TB in a single request. Using a disk snapshot is faster and safer.
This will result in downtime, but we can do this over the weekend to have
minimal impact on engineering productivity.

Schedule maintenance might be required to announce to the rest of the
customers.

A rough migration plan:

1. Create a snapshot.
1. Shutdown Gitaly.
1. Create an incremental snapshot from the previous one.
1. Create a new VM using recovering from the snapshot above.
1. Run some manual QA.

###### `default` shard (gprd main)

When we are migrating this shard we have to be aware of 2 things:

- Leave 60TB+ for new projects
- Leave space for the `marquee` shard to be merged in later in the migration.

1. Create 9 Gitaly servers in a single GCP project 3 per zone.
    - This will give us 26TB `((16000 * 0.6) * 9) - 60000` of available storage for repositories which amounts to 2 Gitaly nodes.
1. Update [gitaly-shard-weights-assigner](https://ops.gitlab.net/gitlab-com/gl-infra/gitaly-shard-weights-assigner/-/blob/master/assigner.rb):
    - Only take into consideration the new Gitaly shard
    - Change the cut-off to be at 75% capacity to leave space for marquee migrations so they are spread equally across multiple storages when we do them.
    - Increase the frequency of runs to every few hours, since we are going to be migrating storage constantly we need to set the weights accordingly.
1. Wait for new projects to start getting created on the new nodes and validate
   everything is working fine.
1. Migrate the newest Gitaly nodes that will use up around 26TB of data leaving 60TB of free space for new projects
    - Start with the newest Gitaly nodes to make [decommission easier](#decommission-old-gitaly-server).
    - Migrate `nfs-file110` with a concurrency.repository of 1 (1 storage, 1 repository) and time it.
    - Migrate `nfs-file109` with a concurrency.repository of 2 (1 storage, 2 repositories) and time it.
    - Migrate `nfs-file108` with a concurrency.repository of 2 (1 storage, 3 repositories) and time it.
    - Continue migration until we have around [60TB of free space](https://thanos.gitlab.net/graph?g0.expr=sum%20by%20(environment%2C%20tier%2C%20type%2C%20stage%2C%20shard)%20(%0A%20%20%20%20%20%20%20%20%20%20node_filesystem_avail_bytes%7Benv%3D%22gprd%22%2Cstage%3D%22main%22%2Ctype%3D%22gitaly%22%2C%20device%3D%22%2Fdev%2Fsdb%22%2Cshard%3D~%22default%22%7D%0A%20%20%20%20%20%20%20%20%20%20and%0A%20%20%20%20%20%20%20%20%20%20(instance%3Anode_filesystem_avail%3Aratio%7Benv%3D%22gprd%22%2Cstage%3D%22main%22%2Ctype%3D%22gitaly%22%2C%20device%3D%22%2Fdev%2Fsdb%22%2Cshard%3D~%22default%22%7D%20%3E%200.2)%0A%20%20%20%20%20%20%20%20)&g0.tab=0&g0.stacked=0&g0.range_input=12h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D) whilst increasing the concurrency. The higher the concurrency.repository the more CPU usage on Gitaly servers.
1. Create 18 Gitaly servers in 2 new GCP projects.
    - This will give us 112TB `(16000 * 0.6) * 18 - 60000` of available storage for single repositories which amount to 9 Gitaly nodes.
1. Migrate 112TB of Gitaly nodes
    - Since we are starting with the newest provisioned Gitaly servers they might be full yes (higher than 80% disk space) we need to calculate how many Gitaly servers 112TB is.
    - Starting from `nfs-file-107` and backward with a concurreny.repository and concurrency.storage.
    - concurrency.storage can be set higher given that `gitalyctl storage drain` is not using a lot of CPU.
    - concurrency.repository can be higher given that it's not adding a lot of load to Gitaly.
1. Repeat the last 2 steps until the complition of the single repository migration.

###### `hdd` shard (gprd)

Ignored, and will be done part of <https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/902>

###### `marquee` shard (gprd)

This shard will be merged into the `default` shard since we don't want to have
these Gitaly nodes with a high blast radius since it only hosts premium+
customers. We need to make sure that we distribute these projects across all of
our Gitaly nodes so we don't have any hot spots.

1. Update [gitaly-shard-weights-assigner](https://ops.gitlab.net/gitlab-com/gl-infra/gitaly-shard-weights-assigner/-/blob/master/assigner.rb):
    - Update cutoff back to 80%. Our largest repository
      on those servers is 5GB so everything should fit and get distributed
      to multiple servers.
1. Migrate the 3 nodes with concurrency.storage 3 and concurrency.repository of 1 (3 storages, 1 repository)
    - Since these are paying customers we should try and effect 1 repository at
      a time per storage to provide the least distribution.

#### Decommission old Gitaly server

The main way to decommission a Gitaly server is to reduce the node count in
[terraform](https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt/-/blob/7f367213ecc3bf7daff0b0e27fa733865ac13715/environments/gprd/variables.tf#L569)
which is why we need to start with the newest provisioned Gitaly servers.

Starting with the newest provisioned Gitaly servers will give us the following advantages:

1. Deleting a Gitaly server is automated, by just reducing `node_count`.
1. Lower risk in the beginning because we'll be moving smaller repositories.
1. Safer because we don't have to deal with state drift if we start with `nfs-file01` instead.

The only disadvantage we get is that these projects can have higher activity
but we'll still have to migrate them at some point.

Deleting a Gitaly server that is no longer in use is safe but we should add
extra steps to be cautious because this could result in data loss.

A high overview of what we should do when we want to decommission a Gitaly
server:

1. Check if there are any more git repositories: `sudo find /var/opt/gitlab/git-data/repositories/ -maxdepth 4 -type d -name '*.git'`
1. Check Gitaly logs for that node to see if we are getting any more requests.
1. Turn off Gitaly's VM
1. Take a snapshot of the data disk
    - Can be automated with <https://gitlab.com/gitlab-com/gl-infra/reliability/-/issues/24126> or <https://github.com/hashicorp/terraform-provider-google/issues/15103>
1. Look at logs to see if any requests are going to the storage we've turned off.
    - This depends on <https://gitlab.com/gitlab-org/gitlab/-/issues/418901>
1. Reduce [count](https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt/-/blob/7f367213ecc3bf7daff0b0e27fa733865ac13715/environments/gprd/variables.tf#L569)
