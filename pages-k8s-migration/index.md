[[_TOC_]]

# Migrating Web-Pages Service to Kubernetes

## Definition

For this document, the "Web Pages Service" is the GitLab Pages feature and will
be commonly shortened to Pages.  This is NOT to be confused with application
traffic that is serviced by our Ruby on Rails monolith.  Pages is instead a very
specific feature that is run by a Golang application.

### Reference

* Documentation: https://docs.gitlab.com/ee/administration/pages/index.html
* Feature Repository: https://gitlab.com/gitlab-org/gitlab-pages

## Overview

This work is part of the ongoing epic to migrate
[Gitlab.com to Kubernetes](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/112)

After our [Web](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/272),
Pages will be our last frontend feature deployed via our Helm chart to migrate
over into Kubernetes.  This should result in reduced deploy times and better
resource usage while making use of the official helm chart. Completing this work
is one of the OKR's for the Delivery team for Q3FY22:
https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/112

This readiness review document is part of
https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/1971

This guide will cover 2 facets of the GitLab Pages migration into Kubernetes:

1. The addition of a Canary stage
1. The migration of this feature over to Kubernetes off of the classic Virtual
   Machine infrastructure
1. Introduction of zonal boundaries for traffic

## Architecture

```mermaid
graph TB

subgraph GCP
  subgraph gitlab-production
    G[GCP-TCP-LB-HTTP]
    GS[GCP-TCP-LB-HTTPS]
    H[HAPROXY]

    subgraph GKE-zonal-cluster
      GT

      subgraph Namespaces

        subgraph gitlab
          subgraph pages-node-pool
            subgraph Pages-pods
              gp
            end
          end
        end
      end
    end
  end

  API
  GCS
end

G -- 80 --> H
GS -- 443 --> H
H -- 80 -->
GT[GCP-TCP-LB-GKE]
H -- 443 --> GT
GT -- 8090 --> gp[gitlab-pages]
GT -- 8091 --> gp

gp -- 8181 --> API
gp -- 443 --> GCS

style GCP fill:#9BF;
style gitlab-production fill:#1AD;
style GKE-zonal-cluster fill:#EAD;
style pages-node-pool fill:#ECD;
style Namespaces fill:#FED;
style Pages-pods fill:#3E7;
style gitlab fill:#FAA;
```

## Performance

We currently (Oct  2021) serve just shy of 2k requests/s to
GitLab Pages.  Traffic behavior can be seen [on our Overview Dashboard](https://dashboards.gitlab.net/d/web-pages-main/web-pages-overview?orgId=1&from=now-24h&to=now&var-PROMETHEUS_DS=Global&var-environment=gprd&var-stage=main)

Performance of the Pages service mainly depends on these factors:

* amount of nodes (as of writing 8 in our main stage + 0 for the canary stage)
* node type (n1-standard-8)

It is important to always have enough capacity so that rolling deployments,
zonal outages, or turning off canary is not affecting user experience. Currently
our VM's are over provisioned as our saturation metrics are all well below 50%
for all measured items. Historically the only time we suffer issues with this
feature is usually abuse or abnormal temporary traffic behavior (most recent
example is when a company releases a new version, we manage to handle a surge of
requests for a particular endpoint).

## Canary

It's been decided to leverage the Canary stage as a method to help us roll this
service out into production.  Doing so will provide us with the following
benefits:

1. A canary stage is being added to this service - though will be feature
   incomplete.
1. We'll have the ability to ease the transition of this service into Kubernetes
   in an easier administrative fashion.

### Canary Completeness

While we are adding a canary stage, our existing method of sending traffic to
canary will not appropriately apply.  Our existing canary stages rely on the
cookie `gitlab-canary` that is set upon the `gitlab.com` domain.  This check is
not available to the Pages HAProxy configuration.  As GitLab Pages handles many
domains, this is not an appropriate method of directing traffic towards canary.
Our only method of sending traffic to canary for the Pages feature will be via
configuring multiple backends and utilizing traffic weights in HAProxy.

This method of configuration is appropriately configured in staging and works
well.

All of our existing tooling and observability to interact with this new stage
has been tested to work as desired in staging.  We expect the same to work just
as well in production.  During the rollout, testing will occur naturally to find
and fix any new found gaps in usability and administration.

## Kubernetes

In a Kubernetes deployment we need to tune resource requests, autoscaling and
rollingUpdate settings to ensure Pages is able to meet it's SLOs and can react
to surges in traffic. Pages Pods take approximately 10 seconds to start up.
Being a golang process, it runs quite efficiently.

We are focusing on the following configuration:

* Horizontal Pod Autoscaler (HPA) target values
* rollingUpdate strategy maxSurge
* minReplicas and maxReplicas
* resource requests and limits for CPU and memory

We will test those parameters by slowly shifting traffic from our VM fleet to
Kubernetes and adapting if necessary. The Pages node pool is using the same
machine type as our current Pages VMs. For most of the above values, we'll start
with similar values that we've come up with based on learnings from rudimentary
testing in the staging environment plus analysis of the processes running
individually on a given VM.

* HPA Targets:
  * 80% average Pod CPU utilization
  * 2 min Pods per cluster - totaling 6 minimum Pods during migration start
  * 20 max Pods per cluster - totaling 60 maximum Pods
  * Canary will have 2 minimum Pods and 20 maximum Pods as well
* Deployment Strategy configuration:
  * Default strategy is RollingUpdate
  * 0 max unavailable Pods
  * 25% max surge

With maxUnavailable set to 0, we will always scale up new Pods prior to tearing
down any older Pods during a deployment or configuration change.

A few details on Pod counts; We currently run 8 Pages nodes on our main stage,
we are adding our first canary stage. When this is added, we'll be paying
careful attention to metrics as we ramp up traffic flow into canary to 100%.  If
we being to run out of Pods, a sign of inefficient use of resources, we may need
to adjust our resource limits and requests as needed.  More about performance
below.

For the canary stage, we run 0 VM's.

## Zonal Traffic Boundary

Currently the traffic coming into our HAProxies is evenly spread across all 8
VM's that service those requests.  While this works very well, this has been
identified as a heavy burden in terms of network costs in the past.  While no
cost analysis was performed, we will follow the same migration strategies that
we've worked in the past.  In doing so, when the zonal clusters are added as
backends in HAProxy, the HAProxy servers will begin to favor the clusters that
live closest to them.  This work was something we first had configured with
various frontends that serve our main application and was a problem we resolved
with zonal clusters.  We are using this migration as an opportunity to perform
the same configuration for the Pages network traffic.  Being that we are
intending to deploy this service highly redundant from the start, we do not
anticipate any issues with this.

## Migration Plan

### Current Virtual Machine configuration with HAProxy

```plantuml
skinparam roundcorner 20
skinparam shadowing false
rectangle "Frontend HAProxy" as A {
  card "pages" as A1
}
rectangle "web-pages-XX VMs" as B {
  card "main" as B2 {
      rectangle "gitlab-pages" as B21
   }
}

A1 --> B21 : main
```

The Main stage for Pages accepts all customer traffic is and evenly distributed
to all backends configured in HAProxy.

#### Phase 1

The first phase will be to move all traffic in the Kubernetes cluster on the
canary stage.  This will be done via weights and during a change procedure to
minimize risk.

```plantuml
skinparam roundcorner 20
skinparam shadowing false
rectangle "Frontend HAProxy" as A {
  rectangle "pages backend" as A1
  rectangle "canary_pages backend" as A2
}
rectangle "web-pages-XX VMs" as B {
  card "main" as B2 {
      rectangle "gitlab-pages" as B21
    }
}
rectangle "Production GKE" as C {
  card "gitlab-cny namespace" as C1 {
      rectangle "pages pod" as C111
    }
}

A1 --> B21 : X%
A2 --> C111 : X%
```

* Doing so will assist us in finding any new potential blockers and validate
  that the service is working as desired.

#### Phase 2

Once we are confident in canary working appropriately, we'll begin slowly
shifting traffic from our regional cluster (which is technically canary) into
our zonal main stage Kubernetes clusters. We'll do this using weights on
HAProxy. After this transition is complete, the VMs can be scaled
down.

```plantuml
skinparam roundcorner 20
skinparam shadowing false
rectangle "Frontend HAProxy" as A {
  rectangle "pages backend" as A1
  rectangle "canary_pages backend" as A2
}
rectangle "pages-XX VMs" as B {
  card "main" as B2 {
      rectangle "gitlab-pages" as B21
    }
}
rectangle "Production GKE" as C {
  card "gitlab-cny namespace" as C1 {
      rectangle "pages pod" as C112
    }

  card "gitlab namespace" as C2 {
      rectangle "pages pod" as C212
    }
}

A1 --> C212 : 0-100%
A1 --> B21 : 0-100%
A2 --> C112 : 0-100%
```

### Network

The migration from Virtual Machines to GKE introduces additional network hops
due to the use of Kubernetes.  With the addition of canary, we will see a minor
topology change to the configuration of where traffic is sent for a given zone
from our HAProxy nodes.

#### Remote IP

We currently do not properly get our remote IP address from our Kubernetes
infrastructure for non-SSL traffic.  Our helm chart currently does not support
setting the `listen-proxy` configuration.  Therefore for all traffic coming in
via HTTP, we will see the last IP address that the packet was sent from,
normally this would be a google load balancer or HAProxy.

For HTTPS traffic, we are appropriately using `listen-https-proxyv2`, therefore
we are properly receiving the remote-ip of client connections.

The above is listed as a risk going into production.

#### SSL

GitLab Pages is the terminator SSL connections.  This is different from how most
of our services operate where HAProxy is the SSL terminator.  The reason for
this is a feature of GitLab Pages where users can bring their own domain as well
as SSL certificate.  Due to this Pages must be the service that terminates the
SSL connection.  With this in mind, it is natural for GitLab Pages to suffer a
slightly higher amount of latency to serve the HTTPS connection, but should not
be any greater than we see today already.

HAProxy also operates in TCP mode for HTTPS connections to the GitLab Pages
backends.  This behavior and configuration is not changing during this
migration.  For HTTP traffic, HAProxy operates in http mode.

#### Virtual Machine Network Topology

We currently run the `web-pages*` fleet on Virtual Machines that are directly
attached to HAProxy VMs. Unlike most of our frontend workloads, this traffic
is load balanced across all nodes that HAProxy knows about.  This provides the
utmost redundancy at the expense of additional network charges due to zonal
egress.

```plantuml
skinparam roundcorner 20
skinparam shadowing false

cloud "Public Internet" as PI
rectangle "TCP LB :443" as TCP_LB

folder "us-east1-b" as F1B {
  rectangle "HAProxy" as HAProxyB
  storage "Pages VM fleet" as WebB
}

folder "us-east1-c" as F1C  {
  rectangle "HAProxy" as HAProxyC
  storage "Pages VM fleet" as WebC

}

folder "us-east1-d" as F1D {
  rectangle "HAProxy" as HAProxyD
  storage "Pages VM fleet" as WebD

}

PI -- TCP_LB
TCP_LB -- HAProxyB
TCP_LB -- HAProxyC
TCP_LB -- HAProxyD
HAProxyB -- WebB
HAProxyB -- WebC
HAProxyB -- WebD
HAProxyC -- WebB
HAProxyC -- WebC
HAProxyC -- WebD
HAProxyD -- WebB
HAProxyD -- WebC
HAProxyD -- WebD
```

Notes:

* Lines that cross availability zones are where we are billed at a higher rate
* In our current configuration there are a few points of cross-zone network
  traffic, it occurs between the `web-pages-*` fleet and our API.

### GKE Network Topology for Pages

```plantuml
skinparam roundcorner 20
skinparam shadowing false

cloud "Public Internet" as PI


rectangle "TCP LB :443" as TCP_LB

PI -- TCP_LB

folder "us-east1-b" as F1B {
  rectangle "HAProxy" as HAProxyB
  node "pages pod" as WebB
}

folder "us-east1-c" as F1C  {
  rectangle "HAProxy" as HAProxyC
  node "pages pod" as WebC
}


folder "us-east1-d" as F1D {
  rectangle "HAProxy" as HAProxyD
  node "pages pod" as WebD
}

TCP_LB -- HAProxyB
TCP_LB -- HAProxyC
TCP_LB -- HAProxyD


HAProxyB -- WebB
HAProxyC -- WebC
HAProxyD -- WebD
```

## Operational Risk Assessment

## What are the internal and external dependencies of this service, are there SPOFs?

### API

GitLab Pages depends on the API for certain functionality, specifically
authorization of private pages end user content.  Unlike today, the GitLab Pages
service will be configured to speak to the local API service that is operating
in the same cluster.  Just like the rest of our services that depend on the API,
if we suffer an API outage of a given zone, Pages will also fail specific types
of requests.  The API being down in one cluster would be very problematic in a
wide variety of ways.  However, we can mitigate a Pages degradation by
temporarily modifying the configuration to have GitLab Pages utilize a differing
API endpoint.  Or if we know this to be temporary, we can drain all Pages
traffic to a given problematic zone.

### GCS

GitLab Pages serves content out of object storage.  This service is out of our
control and is instead managed by Google Cloud Platform.  While we do have
minimal visibility into this service, a degradation of this component will
greatly impact GitLab Pages.

### Dependants of GitLab Pages

GitLab Pages is the provider of artifacts.

* [Job Artifacts](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html)
* [Pipeline Artifacts](https://docs.gitlab.com/ee/ci/pipelines/pipeline_artifacts.html#pipeline-artifacts)

## What are the potential scalability or performance issues that may result with this change?

### Scaling

There is a risk that we will not be able to adapt quickly enough to increases in
traffic, or sudden increases will cause saturation on the service. To mitigate
this we have:

* Isolating this workload in its own node-pool will minimize impact to other
  services.
* The ability to tweak the HPA configuration
* Tweaking the node pool instance types that this workload runs is also an option
* During the migration, we'll be carefully monitoring the metrics associated
  with this service and adjust our scaling configuration as necessary until we
  are taking 100% of the traffic in Kubernetes.
* Should we conveniently suffer a DDOS attack of sorts, we already have a couple
  of helpful items:
  * [Block things on HAProxy](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/frontend/block-things-in-haproxy.md)
  * [Block IPs on HAProxy](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/frontend/ban-netblocks-on-haproxy.md)
  * :warning: We do not have the Pages service behind CloudFlare, and thus are
    unable to utilize that platform for various traffic management.  This is due
    to [how GitLab Pages works](https://gitlab.com/gitlab-org/gitlab-pages/#how-it-should-be-run) in
    order to serve custom domains.
  * Please see the [Known Issues](#known-issues-going-into-production) section for
    additional details about rate limiting

### Log volume

We don't expect any performance issues due to logging, but will be monitoring
this closely during the migration. We will see an increase in the data being
captured that differs from our normal traffic patterns.  This includes the
following items:

* an increase in logging data for the `/-/readiness` check log messages, due to
  the reconfiguration of HAProxy and readiness requests in Kubernetes.
* The start and stopping of Pods will create a larger influx of messages related
  to those services' start up and shutdown events.  Currently we only see this
  from our VM's during deployments.  However, we'll be rotating through more
  Pods during a deployment along with extra events from Pods that scale with our
  Auto-scaling mechanism
* We'll also have more data for each log line as we have more metadata
  associated with the Pods running inside of Kubernetes - this is relatively
  negligible and we exclude a lot of fields on purpose.
  * Should we notice impact on our logging infrastructure we may need to adjust
    the number of shards required to store this additional log data.  This is a
    common method of alleviating the pressure as we've seen in the past.

### Prometheus metrics

Prometheus metrics are how we observe the behavior of our entire stack.  The
additional Pods will greatly increase the amount of metrics our systems will
need to process.  We will be monitoring Thanos, specifically the thanos-rule
process to ensure that we do not overwhelm our rule aggregations.  We monitor
this service heavily with its own set of SLO alerts.  Should any alerts trip
when we begin running this service in production, we'll need to stop and
evaluate how we can ease the pressure on our monitoring infrastructure.

## List the top operational risks when this feature goes live.

* Unexpected configuration differences between Cloud Native and Omnibus. We try
  to reduce the risk by auditing configuration in all environments.
  https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/1972
  * The same audit has occurred when we first deployed the service into each
    environment.
  * Luckily the amount of configuration and the rate of change of configuration
    for this service has been very minimal overall.
* Issues with HPA, not enough reserved capacity and saturation problems related
  to scaling
  * This is captured as a Saturation Alert that pages the EOC.
* Issues with metrics overwhelming the current in-cluster Prometheus and
  our Thanos rule aggregation systems.
* HAProxy does not entirely behave as desired when clusters are pulled from
  rotation.  We have an issue to address this in the future.  All prior
  migrations have completed successfully, so at the moment this is considered
  low risk.  Issue to address: https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/14190

## Security and Compliance

The security stature of this service does change a little bit, but hopefully for
the better.  The scope of managing security, however, will change as this is a
shift in where the service is running (Virtual Machines vs Kubernetes).  This
service will receive security updates in accordance to the [GitLab Release and
Maintenance Policy](https://docs.gitlab.com/ee/policy/maintenance.html)

### Security Changes of Note

* Pods do not have any ability to run or change to a privileged user.
  * In order to accomplish this, we must utilize our tooling documented here:
    [Attaching to a Container](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/kube/k8s-operations.md#attaching-to-a-running-container)
  * However, note that when following this procedure, this does not allow the
    process to gain elevated privileges, only the user performing the necessary
    troubleshooting
* We make use of [Network Policies](https://kubernetes.io/docs/concepts/services-networking/network-policies/)
  * One can view our configuration for the GitLab Pages [by clicking these
    words](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/-/blob/3b259b3438eaafe366bb3bfc3e55ab96a986f763/releases/gitlab/values/values.yaml.gotmpl#L455-475)
  * Highlights, we limit access to only DNS, the GitLab API, and the GCP
    metadata

### Security Items that did not change

* Access to the GitLab API is required for this service, and remains configured
  the same as our omnibus installed VM's.  This is managed slightly differently,
  however.  These are installed as Kubernetes secrets via the
  [gitlab-secrets](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/-/blob/3b259b3438eaafe366bb3bfc3e55ab96a986f763/releases/gitlab-secrets/helmfile.yaml#L268-289)
  release.
  * Details of this [are documented at the project](https://gitlab.com/gitlab-org/gitlab-pages/#gitlab-access-control)
* Rate limiting is currently not possible with GitLab Pages.  See the [Known
  Issues](#known-issues-going-into-production) section for details.

### Configurations

Infrastructure changes and configurations are not lightweight and must go
through a rigorous review procedure and be vetted and tested prior to being
brought into our production environments.  Configurations that involve changes
that would modify our exposure or security posture, should also be reviewed by
our security team to ensure any proposal is appropriately vetted for any
potential risk.  We have the procedure configuration change documented in the
repository's [DEPLOYMENT.md](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/-/blob/master/DEPLOYMENT.md#configuration-changes) document.
Secret management does not change.  Documentation for this is in our
repository's [README.md](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/-/tree/master#gitlab-secrets).

If a proposed change contains unknown security concerns, there should be no
hesitation to rope in the security team to provide the needed confidence prior
to allowing such a change into our environments.

More details on reaching out to the appropriate team members can be found here:
https://about.gitlab.com/handbook/engineering/security/security-engineering-and-research/application-security/

### Application Upgrade and Rollback

The deployment mechanism for the Pages will mirror that of all other services
which utilize our own helm chart.  Please see our [existing Kubernetes
deployment documentation.](https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/deploy/gitlab-com-deployer.md#kubernetes-upgrade)

## Performance

### Resource Configuration

For the Pages container:

```yaml
resources:
  limits:
    memory: 6G
  requests:
    cpu: 1000m
    memory: 4G
```

For CPU, our nodes hover at around 10% CPU usage with periodic spikes up to 25%.
This usage is primary of type `user` and is thus mostly driven mostly by the
Pages service.  Our nodes have 8 cores, or 8000m available.  Going conservative
of requesting 25% of 8000m equates to approximately 3200m.  Doing so will allow
us to configure a decent HPA scale of 80% CPU saturation which should situate us
in alignment with existing saturation levels on a per node level.

Memory use for the Pages service can be gathered directly by `go_memstats_*`
metrics coming from the service itself.  Using `go_memstats_sys_bytes` we see
that Pages demands approximately 2.5GB to 4.5GB of RAM during normal daily
usage.  RAM requested remains relatively steady with a very sluggish incline
until some interruption, example deploy, restarts the service.  With this, I'm
using those exact numbers with a 500MB buffer when first starting off this
service in Production.

On instance type `n1-standard-8`, which provides us with 8 cores and 30 GB of
RAM, we can theoretically schedule around 3 Pods per node.  I expect less due to
other Kubernetes services that also require resources.

Details about the decisions above are highlighted in this investigative issue
here: https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/1970

## Backup and Restore

This is a stateless component of GitLab.  No work to be done regarding backups
and restoration of customer data with this migration.

## Monitoring and Alerts

### Logging

Logging remains the same before and after this migration. One can view [our
runbook for
Logging](https://gitlab.com/gitlab-com/runbooks/-/tree/master/docs/logging#logging-infrastructure-overview)
which contains a lot of details as to how logging works for this infrastructure.

We will mostly be using the same log indexes when running in Kubernetes. Logging
coming from the Pod is serviced is handled natively by stdout written by the
GitLab pages service.  This differs from our rails applications where a
differing tool is used to prepare our logs for consumption by fluentd.  This is
good as there's one less component involved in our logging infrastructure.

### Monitoring

We currently have a wide scope of monitoring that comes from the use of
`kube-state-metrics` as well as the exporter that is built into the GitLab Pages
service. We'll continue to leverage our existing alerting rules to warn us of
problems related to the operational state of these Pods from the scope of
running inside of Kubernetes.

We also have a suite of metrics specifically monitoring the Pages backend in
HAProxy which will alert us if we see excessive error rates or a drop in Apdex.

## Responsibility

SME's:

* Infrastructure:
  * Delivery
  * Reliability
* Deployment: ~team::Delivery
* Helm Chart: ~team::Distribution
* Pages:
  * ~group::release

## Testing

No testing was performed as this is one that already exists in our environment.
All testing has been circled around the migration and operational aspects of this
service into Kubernetes.  Refer to the associated epic for testing that was
performed. https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/273

We leveraged knowledge of recent migrations, API, https_git to assist us with
helping guide decision making for this migration.

## Known issues going into production

Our Helm chart does not support setting the `listen-proxy` configuration.  As
describe above, any unencrypted traffic will not provide us with the appropriate
client IP address.  Issue to fix: https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/2083

GitLab Pages also has an issue to help with rate limiting as well.  This is
being tracked here: https://gitlab.com/gitlab-org/gitlab-pages/-/issues/629

## Readiness Reviewers

### Engineering

### Infrastructure

* @igorwwwwwwwwwwwwwwwwwwww 
* @cmcfarland 

### Security

