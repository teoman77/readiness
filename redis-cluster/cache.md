_While it is encouraged for parts of this document to be filled out, not all of the items below will be relevant. Leave all non-applicable items intact and add the reasons for why in place of the response._
_This Guide is just that, a Guide. If something is not asked, but should be, it is strongly encouraged to add it as necessary._

## Summary

- [x] **Provide a high level summary of this new product feature. Explain how this change will benefit GitLab customers. Enumerate the customer use-cases.**

As covered in [ratelimiting](ratelimiting.md), Redis has 2 critical resources that are hard to vertically scale beyond a certain point:
* Reaching single-core CPU saturation abruptly and severely impacts Redis latency and the apdex and error rates for its clients.
* Reaching memory saturation can lead to a variety of bad outcomes depending on config and circumstances, including severe latency spikes, service outage, or data loss.

Redis Cluster will give a predictable and much lower friction means of addressing both CPU and memory saturation, by allowing us to scale up the number of Redis instances as the workload grows. Once implemented, it will natively provide horizontal scaling and online resharding without further client-side changes.

In particular, caching via Redis is important to the GitLab application as it takes load off other services like patroni and patroni-ci while improving client-side latency.

- [x] **What metrics, including business metrics, should be monitored to ensure will this feature launch will be a success?**

As outlined in the epic https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/878, this is our vision of what "done" looks like:
* The `cache` workload is running on Redis Cluster instead of vanilla Redis.
* All Redis nodes in the new cluster are below 70% CPU and memory utilization at their daily peak workload.
* All new failure modes we determine to be critical to address before production go-live have a well understood and documented recovery mechanism.
* Metrics, dashboards, alerts, and log ingestion have been added to provide observability for the new cluster.
* Profiling tools work with the cluster's redis build.
* Tamland supports capacity forecasting for Redis Cluster.
* Runbooks documentation provides an overview of Redis Cluster's architecture, observability, and troubleshooting.

Metrics to watch on the [new dashboard](https://dashboards.gitlab.net/d/redis-cluster-cache-main/redis-cluster-cache-overview) for `redis-cluster-cache` include:
* [Redis Cluster Slots OK](https://dashboards.gitlab.net/d/redis-cluster-cache-main/redis-cluster-cache-overview?orgId=1&viewPanel=111) - Healthy slot count should remain consistently at 16K.
* [Operation Rate - Primary](https://dashboards.gitlab.net/d/redis-cluster-cache-main/redis-cluster-cache-overview?orgId=1&viewPanel=79) - Operations should be spread among all of the cluster's primary nodes.
* [Redis CPU per Node - Primary](https://dashboards.gitlab.net/d/redis-cluster-cache-main/redis-cluster-cache-overview?orgId=1&viewPanel=81) - Redis main thread CPU usage should remain below 70% for all primary nodes.
* [Memory Saturation](https://dashboards.gitlab.net/d/redis-cluster-cache-main/redis-cluster-cache-overview?orgId=1&viewPanel=96) - Redis resident memory usage should remain below 50% of the total host memory for all nodes.
* [Latency per command type](https://dashboards.gitlab.net/d/redis-cluster-cache-main/redis-cluster-cache-overview?orgId=1&viewPanel=91) - Redis server-side latency measurement for each Redis command type (`GET`, `INCR`, `DEL`, etc.).
* [Cluster redirect responses](https://dashboards.gitlab.net/d/redis-cluster-cache-main/redis-cluster-cache-overview?orgId=1&viewPanel=116) - Rate of Redis "redirect" responses (`MOVED` or `ASK`). These add an extra round-trip of latency to affected requests, but they should only occur during rare events like resharding or failovers. This dashboard panel is normally empty due to no such redirects occurring.

On the client-side, metrics to watch would be web and api service related dashboards:
- [web service apdex](https://dashboards.gitlab.net/d/web-main/web-overview?orgId=1&viewPanel=1888663568)
- [api service apdex](https://dashboards.gitlab.net/d/api-main/api-overview?orgId=1&viewPanel=3077952111)

Due to the overhead of CRC16 calculations and routing performed in the [cross-slot pipelining components](https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/redis/cross_slot.rb), we need to keep an eye on client-side apdex. However, note that web and api apdex are affected by many other factors.

Other metrics to track would be the existing datastore (`redis-cache`) which is on the [existing dashboard](https://dashboards.gitlab.net/d/redis-cache-main/redis-cache-overview?orgId=1). The alerts for this service will be silenced during initial stages of migration and downgraded to S4 severity after read traffic is migrated to the Redis Cluster datastore to avoid false alarms to on-call engineers. The observability components will be removed only after we have concluded the migration and observed the new datastore for a week or so.

## Architecture

- [x] **Add architecture diagrams to this issue of feature components and how they interact with existing GitLab components. Make sure to include the following: Internal dependencies, ports, encryption, protocols, security policies, etc.**

From an application architecture perspective, Redis Cluster wholly replaces the existing vanilla Redis and its Sentinels for the `cache` workload.

The service is still provided by the same Redis software using the same mechanisms for authentication, replication, persistence, etc.

Our current ruby client library [`redis-rb`](https://github.com/redis/redis-rb/tree/v4.8.0) natively supports Redis Cluster.

The main architectural differences arise from Redis Cluster's [design choices](https://redis.io/docs/reference/cluster-spec/) related data sharding and request routing among cluster nodes, as summarized in the next section.

To visualize, here is an example client interacting with a 5-shard Redis Cluster:

```mermaid
graph LR
   C(Client) -- Redis requests --> RS1_P[(Redis shard 1 primary)]
   C -- Redis requests --> RS2_P[(Redis shard 2 primary)]
   C -- Redis requests --> RS3_P[(Redis shard 3 primary)]
   C -- Redis requests --> RS4_P[(Redis shard 4 primary)]
   C -- Redis requests --> RS5_P[(Redis shard 5 primary)]
   RS1_P -- Replication --> RS1_R1[(Redis shard 1 replica 1)]
   RS1_P -- Replication --> RS1_R2[(Redis shard 1 replica 2)]
   RS2_P -- Replication --> RS2_R1[(Redis shard 2 replica 1)]
   RS2_P -- Replication --> RS2_R2[(Redis shard 2 replica 2)]
   RS3_P -- Replication --> RS3_R1[(Redis shard 3 replica 1)]
   RS3_P -- Replication --> RS3_R2[(Redis shard 3 replica 2)]
   RS4_P -- Replication --> RS4_R1[(Redis shard 4 replica 1)]
   RS4_P -- Replication --> RS4_R2[(Redis shard 4 replica 2)]
   RS5_P -- Replication --> RS5_R1[(Redis shard 5 replica 1)]
   RS5_P -- Replication --> RS5_R2[(Redis shard 5 replica 2)]
```

For context, the `cache` Redis DB is used extensively by the GitLab Rails application either via `Rails.cache` or directly via `Gitlab::Redis::Cache`. Some [notable workloads](https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/2320) which had to be modified to be compatible with Redis Cluster are:

- `Rails.cache` use of `read_multi/fetch_multi/delete_multi`. Some examples are [cache helpers](https://gitlab.com/gitlab-org/gitlab/-/blob/8aff4bf849027e1f3859c7709b0237f92f7603e1/lib/gitlab/cache/helpers.rb#L119) and [project batch counting](https://gitlab.com/gitlab-org/gitlab/-/blob/8aff4bf849027e1f3859c7709b0237f92f7603e1/app/services/projects/batch_count_service.rb#L17)
- `MarkdownCache` which uses a pipeline of `hmget` to [bulk fetch markdown details](https://gitlab.com/gitlab-org/gitlab/-/blob/8aff4bf849027e1f3859c7709b0237f92f7603e1/lib/gitlab/markdown_cache/redis/store.rb#L9)
- [Bulk deletion](https://gitlab.com/gitlab-org/gitlab/-/blob/8aff4bf849027e1f3859c7709b0237f92f7603e1/lib/gitlab/avatar_cache.rb#L69) of avatar cache
- [Bulk writing and reading](https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/discussions_diff/highlight_cache.rb) for `DiscussionsDiff::HighlightCache`
- [Bulk writes](https://gitlab.com/gitlab-org/gitlab/-/blob/8aff4bf849027e1f3859c7709b0237f92f7603e1/lib/gitlab/cache/import/caching.rb#L162) import caching keys
- [Bulk deletion](https://gitlab.com/gitlab-org/gitlab/-/blob/8aff4bf849027e1f3859c7709b0237f92f7603e1/lib/gitlab/reactive_cache_set_cache.rb#L20) of keys in reactive set cache and [set cache](https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/set_cache.rb#L25)

The workloads are modified by breaking up single multi-key Redis commands into single key commands and sending them in a a pipeline. The pipeline are fan-out and routed to the node containing the key via an [application-side router](https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/redis/cross_slot.rb). Note that the application-side router can be replaced with the native implementation upon upgrading to `redis-rb` `v5.x`. The current implementation is a simplified and lightweight version of the implementation in [redis-cluster-client](https://github.com/redis-rb/redis-cluster-client/blob/master/lib/redis_client/cluster/pipeline.rb).

- [x] **Describe each component of the new feature and enumerate what it does to support customer use cases.**

In most ways, Redis Cluster functions as a drop-in replacement for vanilla Redis.

Similarities:
* The Redis nodes use the same `redis-server` binary, just running in cluster mode: [`cluster-enabled yes`](https://github.com/redis/redis/blob/7.0.7/redis.conf#L1572-L1576).
* The same Redis commands and data types are available. However, there are new constraints on multi-key operations to accommodate sharding, as noted below.
* The same replication mechanism is used for asynchronously propagating writes from primaries to replicas.

Differences:
* There are no Sentinels.
  * In vanilla Redis with Sentinels for high availability, clients discover the current Redis primary node by asking any of the Sentinels. Sentinels collaboratively detect node failures and coordinate failovers.
  * In Redis Cluster, clients query any cluster node to discover all nodes, their roles, and which hash slots (i.e. subset of keys) they own. Instead of using Sentinels, the Redis nodes themselves perform node failure detection (via mutual health checks and gossip), and primary nodes vote to authorize a failover to promote a replica of a failed primary node. If a majority of masters are down, failover cannot be authorized, even if healthy replicas are available for the failed masters. Redis Cluster does not natively address this situation, but we have added a sidecar process (described later) to automate recovery from this scenario.
* Clients route their requests to the primary node owning the key they are accessing.
  * In vanilla Redis, all keys are owned by the single primary node, so there are no key-specific routing decisions to make.
  * In Redis Cluster, each key is hashed into one of 16K "hash slots", and each hash slot is owned/stored by exactly one of the cluster's primary nodes (and cloned to its replicas). If a client sends a request to an inappropriate node (e.g. due to failover or resharding), the Redis node redirects the client to the appropriate node, letting the client both retry its current request and update its map for routing future requests.
* Multi-key operations can only act on a single hash slot at a time. (See docs: [Redis Cluster data sharding](https://redis.io/docs/management/scaling/#redis-cluster-data-sharding))
  * In Redis Cluster, clients must send requests to the correct primary that owns each key's hash slot. Most client requests act on a single key and implicitly satisfy this constraint. When clients need to act on multiple keys (e.g. `MGET`), that request must be split along hash slot boundaries. A hash slot is the smallest unit of sharding and is the granularity at which keys are assigned to primary nodes. If a group of keys must be acted on atomically together, those keys must map to the same hash slot so they are guaranteed to be stored on the same primary node. Clients can use hash tags for this purpose.
  * Alternatively, we validate this client-side via [`RedisClusterValidator`](https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/instrumentation/redis_cluster_validator.rb). In the application, we can use pipelines to circumvent this offence as long as the keys in the pipeline are belong to the Redis server that receives the pipeline, i.e. the pipeline can contain commands for keys of different hash slot as long as the receiving node is the owner of all the hash slots in the pipeline. See [docs](https://docs.gitlab.com/ee/development/redis.html#multi-key-commands) for more info.

* The cluster can be resized via online resharding to accommodate future growth.
  * In vanilla Redis, only vertical scaling is supported.
  * In Redis Cluster, online resharding (which should be a rare event in our environment) migrates hash slots between primary nodes to rebalance the CPU and memory usage among nodes. During the transitional state of migrating a hash slot between primaries, clients may experience transient errors in pipelined operations that access that hash slot. These transient errors can be avoided by either patching or upgrading the clients' redis-rb library.  That work is out of scope for the current project.


- [x] **For each component and dependency, what is the blast radius of failures? Is there anything in the feature design that will reduce this risk?**

As a replacement for the existing `redis-cache` Redis database, the blast radius for failure is identical.

There are no new failure modes other than the ones already addressed in the [readiness review for rate-limiting](ratelimiting.md).

- [x] **If applicable, explain how this new feature will scale and any potential single points of failure in the design.**

Better scaling is the entire goal of adopting Redis Cluster. We anticipate a roughly equal workload on each of the cluster's primary nodes.  If the workload turns out to have (or later acquire) some very hot keys, we can fine-tune the distribution of hash slots among shards to rebalance the workload.

As the workload grows over time, we can increase the number of shards and redistribute keys among them.

## Operational Risk Assessment

- [x] **What are the potential scalability or performance issues that may result with this change?**

Low risk.

This change will use pipelined single key Redis commands in place of multi-key commands to handle the cross-slot problem introduced by Redis Cluster. E.g. a `MGET A B C ...` will be converted into `n` pipelines of `GET` to various shards. Such bulk operations will be slower on the client-side than before. However, `redis-cache`'s workload is predominantly read-dominant where the top 3 commands are `GET`, `HGET` and `SET`. As discussed in our [performance benchmarks](https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/2320#results), the performance degradations are within expectations and is a justifiable trade-off as it will allow the datastore to scale horizontally.

The cluster nodes have been sized with 33% more vCPU than the existing `redis-cache` to avoid the risk of worse performance than the current non-clustered Redis primary node (see [sizing analysis](https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/2009#note_1237946792)).

This change spreads keys among Redis nodes to provide better and more flexible scaling and to avoid the performance impact of CPU saturation.

If the workload becomes unevenly spread among the cluster's primary nodes (e.g. if the workload has too many very hot keys on the same shard), online resharding would let us rebalance the workload. Resharding incurs some transitional overhead (i.e. extra latency from multiple round-trips, and pipelined commands may temporarily fail), so we would only do that if the workload distribution was very uneven (which is possible but unlikely).

- [x] **List the external and internal dependencies to the application (ex: redis, postgres, etc) for this feature and how the service will be impacted by a failure of that dependency.**

Replacing Redis with Redis Cluster does not materially change dependencies. The same Redis binaries are used on the server side, and the same client library is used on the client side.

Both sides switch to using the cluster mode of operation. This involves clients routing each request to the appropriate Redis primary node. Clients discover cluster nodes and topology by querying a known Redis node directly (rather than querying Sentinels, which are not used in cluster mode). If a client sends a request to the wrong Redis node (e.g. due to a failover or resharding), Redis will redirect the client to the correct node, and the client will update its local routing map for future queries.

- [x] **Were there any features cut or compromises made to make the feature launch?**

**Resharding:** Among the [failure modes we explored](https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/2071#results-summary), we deferred 2 that will cause some transient client errors for certain usage patterns during online resharding. Resharding will be very rare in our environments, so this is not an immediate concern. More details on resharding are in a later section of this doc.

- [x] **List the top three operational risks when this feature goes live.**

We have mitigated the known critical risks in addition to the ones mentioned in [ratelimiting](ratelimiting.md).

Highlights:
* Excessive load on the databases due to cache miss: we will migrate keys with excessively long TTL (> 1 day) so that there will not be a large volume of cache misses when read traffic is cut-over to `redis-cluster-cache`. This is done via an [external script](https://gitlab.com/gitlab-com/runbooks/-/blob/master/scripts/redis_diff.rb). This process was trialed during `gstg` migrations in https://gitlab.com/gitlab-com/gl-infra/production/-/issues/15875.
* Testing in nonprod uncovered the possibility of a data race between 2 Rails client during the dual-write phase. However, this is not a problem unique to this product feature but is faced by all migrations which uses [MultiStore](https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/redis/multi_store.rb), a wrapper layer which sends the same write command to both old and new data store sequententially. This data race is resolved by using a validator script to sync the new datastore with the old datastore. We will toggle the feature flag to cut over read traffic to the new datastore only after new datastore is in sync. Note that this is NOT a critical failure as it only delays the migration process. The client will continue to reads from the original `redis-cache` instance which remains the source of truth during the migration process.

- [x] **What are a few operational concerns that will not be present at launch, but may be a concern later?**

**Error rate may temporarily spike during online resharding:**

Low risk: We are documenting this for completeness but do not expect this to be a practical problem for the reasons noted below.

Redis Cluster supports online resharding -- reassigning hash slots to a different primary node and migrating the associated keys.

During resharding, Redis uses redirects (`MOVED` or `ASK` responses) to guide clients to the appropriate node during a hash slot's transitional states. Redis clients can then use the redirect to retry their command, adding latency (another round trip) but getting a correct and consistent result.

Unfortunately, our current version of the `redis-rb` client library (4.8) does not gracefully handle redirects when they occur in a pipelined usage pattern.  When any command in a pipelined set of redis calls results in a redirect response, the pipeline aborts rather than following the redirect for the affected command. This only occurs when accessing keys in hash slots that are actively migrating, but it will increase the client error rate during that timespan.

This is a low urgency concern because:
* Resharding will be very rare in our operating environment (likely less than once per year for any cluster).
* The transient error rate spike ends as soon as each hash slot migration finishes, so pacing is controllable.
* Upgrading to the next major version of `redis-rb` will address this problem. The work will be tracked in [epic 941](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/941).
* `redis-cluster-cache` is sized with 33% more vCPU. See [sizing details](https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/2009#note_1237946792).

For more on this topic, see https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/2112.

- [x] **Can the new product feature be safely rolled back once it is live, can it be disabled using a feature flag?**

Yes. Toggling a feature flag switches between the old and new datastores. While dual-writes are still ongoing, read traffic can be rolled back immediately. However, once dual-writes are stopped, we will need to perform a external script-based migration to sync up the old datastore with the new datastore before we can rollback. Rolling back with to a stale datastore will result in cache misses and increased read traffic to the databases.

- [x] **Document every way the customer will interact with this new feature and how customers will be impacted by a failure of each interaction.**

No functional changes to customer interactions. This change just replaces the backing datastore for the existing caching functionality.

- [x] **As a thought experiment, think of worst-case failure scenarios for this product feature, how can the blast-radius of the failure be isolated?**

Since this is replacing an existing datastore, its scope of impact is circumscribed by all features which uses caching on `redis-cache`.

Rapid rollback via a feature flag toggle limits the duration of failure during the initial rollout.

We revisit the proactive analysis of [failure modes](https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/2071#results-summary) for Redis Cluster helped reduce risk of future incidents by making configuration adjustments and supplemental fault detection/mitigation. The [deferred concerns](https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/2071#deferred-concerns) are of low priority in this product feature and will be deferred.

At this point, we appear to have good coverage for known failure modes and good observability for diagnosis of unknown failures.

## Database

- [x] **If we use a database, is the data structure verified and vetted by the database team?**

Not applicable. Redis is a schemaless datastore.

- [x] **Do we have an approximate growth rate of the stored data (for capacity planning)?**

Tamland covers growth forecasting. The same metrics apply for Redis Cluster as for vanilla Redis: max single-core CPU usage on primary nodes and max memory usage on primary nodes.

- [x] **Can we age data and delete data of a certain age?**

Already done. Nothing is changing about Redis keys' TTL.

For project pipeline status keys, a TTL was added recently. As a result, there are several million stale keys without TTLs. We will not migrate them via the external script, instead letting a background migration perform the clean up on the old database. For the new database, this will not be an issue as we will only cut traffic after a minimal 24 hour wait (the new configured TTL is 8 hours). For more information, see the [application issue](https://gitlab.com/gitlab-org/gitlab/-/issues/416007) and [infra discussions](https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/2075) for more background information.

## Security and Compliance

- **Are we adding any new resources of the following type? (If yes, please list them here or link to a place where they are listed)**
  - [ ] **AWS Accounts/GCP Projects**
  - [ ] **New Subnets**
  - [ ] **VPC/Network Peering**
  - [ ] **DNS names**
  - [ ] **Entry-points exposed to the internet (Public IPs, Load-Balancers, Buckets, etc...)**
  - [ ] **Other (anything relevant that might be worth mention)**

No, none of the above.

- **Secure Software Development Life Cycle (SSDLC)**

Some of this set of questions are not applicable, as this change is replacing a datastore, not developing a new product feature.

  - [x] **Is the configuration following a security standard? (CIS is a good baseline for example)**

The server and client configurations are managed by our standard tools (chef, terraform, helm charts).

The client configuration improves on GitLab's existing pattern for connecting to Redis by introducing support for seamless password rotation. Historically Redis only supported a single default user with a single password, but Redis 6.0 [introduced an ACL system](https://redis.io/docs/management/security/acl/) that supports multiple users and fine grained access control (see also [`ACL SETUSER` docs](https://redis.io/commands/acl-setuser/)). In particular, a Redis user can have multiple valid passwords. For password rotation, we can add a second password to the Redis server ACL for our application user, and then add the new password to Hashicorp Vault. The new password can then be rolled out to all clients gradually before removing the old password from the Redis user. This pattern allows for graceful password rotation without breaking authentication during the transitional phase. This password rotation works with either vanilla Redis or Redis Cluster, and would be useful to implement for all Redis datastores in the future.

We have defined [4 different passwords for rails, console, replica and exporter](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/redis/provisioning-redis-cluster.md#1-generate-redis-passwords) for `redis-cluster-cache`.

  - **All cloud infrastructure resources are labeled according to the [Infrastructure Labels and Tags](https://about.gitlab.com/handbook/infrastructure-standards/labels-tags/) guidelines**

Yes. The [merge request to provision the relevant infrastructure](https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt/-/merge_requests/6128/diffs) added the labels listed as required in the handbook.

  - **Were the [GitLab security development guidelines](https://docs.gitlab.com/ee/development/secure_coding_guidelines.html) followed for this feature?**

This project is not new feature development, but yes the associated GitLab MRs followed standard practice.

  - **Do we have an automatic procedure to update the infrastructure (OS, container images, packages, etc...)**

The same level of automation exists for the new Redis Cluster infrastructure as for the existing Redis Sentinel-based infrastructure.

There is a [redis-cluster-reconfigure.sh script in runbooks](https://gitlab.com/gitlab-com/runbooks/-/merge_requests/5864) to reconfigure the Redis Cluster in the same manner as a sentinel-based Redis via [redis-reconfigure.sh](https://gitlab.com/gitlab-com/runbooks/-/blob/master/scripts/redis-reconfigure.sh).

  - **Do we use IaC (Terraform) for all the infrastructure related to this feature? If not, what kind of resources are not covered?**
    - **Do we have secure static code analysis tools ([kics](https://github.com/Checkmarx/kics) or [checkov](https://github.com/bridgecrewio/checkov)) covering this feature's terraform?**

This infrastructure is part of our [`gitlab-com` Terraform repo](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com) and adheres to all of its standard practices and tooling.

  - **If there's a new terraform state:**
    - [ ] **Where is to terraform state stored, and who has access to it?**

No, the existing standard environment terraform state files are used.

  - [ ] **Does this feature add secrets to the terraform state? If yes, can they be stored in a secrets manager?**

Yes. This feature follows the standards set in [ratelimiting](ratelimiting.md)

  - **If we're creating new containers:**
    - [ ] **Are we using a distroless base image?**
    - **Do we have security scanners covering these containers?**
      - [ ] **`kics` or `checkov` for Dockerfiles for example**
      - [ ] **[GitLab's container](https://docs.gitlab.com/ee/user/application_security/container_scanning/#configuration) scanner for vulnerabilities**

No, this change does not introduce new container images.

- **Identity and Access Management**
  -  [ ] Are we adding any new forms of **Authentication** (New service-accounts, users/password for storage, OIDC, etc...)?
  -  [ ] **Does it follow the least privilege principle?**

No new authentication services are added, but as described above, we have started switching to Redis's new ACL-based auth scheme, which allows for multiple Redis users with multiple passwords and fine-grained permissions.

For both the old and new Redis datastores, clients (e.g. rails containers) use password-based authentication to Redis, with the password stored in a vault and exposed to client containers via an external secret object.

In terms of effects, switching to ACLs means we can now support disruption-free password rotation and can use a separate less privileged user account for scraping Prometheus metrics from Redis nodes.

- **If we are adding any new Data Storage (Databases, buckets, etc...)**

This replaces an existing datastore (`redis-cache`), with no changes to its storage properties, data classification, encryption, or audit log.

  -  [x] **What kind of data is stored on each system? (secrets, customer data, audit, etc...)**

The data content are Rails models and other webservice-related metadata for the purposes of caching.

  -  [x] **How is data rated according to our [data classification standard](https://about.gitlab.com/handbook/engineering/security/data-classification-standard.html) (customer data is RED)**

The data is rated RED since the cache keys and value will contain project/group/user/etc ids and metadata.

  -  [x] **Is data it encrypted at rest? (If the storage is provided by a GCP service, the answer is most likely yes)**

Yes. For context, Redis is an in-memory database, but it can periodically write backups (RDB) and transaction logs (AOF) to disk. GCP Persistent Disk volumes are natively encrypted. No additional encryption is applied at the guest OS or Redis layers.

That said, we are NOT performing rdb backups for cache (both `redis-cache` and `redis-cluster-cache`) as `save` is configured to `""` and the `gitlab_redis_backup::default` cookcook is not added.

  -  [x] **Do we have audit logs on data access?**

Redis queries are not discretely logged. However, standard logging includes shell access to Redis nodes, command-line invocations of `redis-cli` (requires sudo which is logged), and rails console access.

- **Network security (encryption and ports should be clear in the architecture diagram above)**
  -  [ ] **Firewalls follow the least privilege principle (w/ network policies in Kubernetes or firewalls on cloud provider)**
  -  [ ] **Is the service covered by any DDoS protection solution (GCP/AWS load-balancers or Cloudflare usually cover this)**
  -  [ ] **Is the service covered by a WAF (Web Application Firewall)**

No changes to any of these layers, as this is a drop-in replacement for the existing Redis cache database.

- **Logging & Audit**
  - [x] **Has effort been made to obscure or elide sensitive customer data in logging?**

Not applicable. There are no extra logs being produced other than the existing ones on `redis-cache`.

- **Compliance**
    - [x] **Is the service subject to any regulatory/compliance standards? If so, detail which and provide details on applicable controls, management processes, additional monitoring, and mitigating factors.**

No changes to compliance requirements. The data flows are not changing. The same data is stored (request rate counters), and it is accessed by the same internal dependent services (rails containers).


## Performance

- [x] **Explain what validation was done following GitLab's [performance guidelines](https://docs.gitlab.com/ee/development/performance.html). Please explain which tools were used and link to the results below.**
- [x] **Are there any potential performance impacts on the database when this feature is enabled at GitLab.com scale?**
- [ ] **Are there any throttling limits imposed by this feature? If so how are they managed?**
- [ ] **If there are throttling limits, what is the customer experience of hitting a limit?**
- [ ] **For all dependencies external and internal to the application, are there retry and back-off strategies for them?**
- [ ] **Does the feature account for brief spikes in traffic, at least 2x above the expected TPS?**

A small fraction of caching workload will be impacted as this change will use pipelined single key Redis commands in place of multi-key commands to handle the cross-slot problem introduced by Redis Cluster. e.g. a `MGET A B C ...` will be converted into `n` pipelines of `GET` to various shards. Such bulk operations will be slower on the client-side than before. However, `redis-cache`'s workload is predominantly read-dominant where the top 3 commands are `GET`, `HGET` and `SET`.

We have [performed tests](https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/2320) on the existing `redis-cache` to estimate the impact of using pipelined single-key commands over multi-key commands. On the client-side, apdex was not visibly impacted. On the server-side we see greater CPU utilisation but this is mitigated as we are spreading out the workload over 5 shards with the option to horizontally scale in the future.

As discussed in [the readiness review for ratelimting](/redis-cluster/ratelimiting.md), Redis capacity is generally bound by 2 machine resources: single-core CPU usage and memory usage.

Switching to clustered mode increases both of those resources, making them harder to saturate and easier to grow as needed:
* **CPU:** Redis latency and throughput regressions are generally caused by starving for CPU time, and switching to a sharded architecture reduces the odds of that occurring. That risk is further reduced by choosing VM specs that can tolerate any one node taking the same amount of traffic as the current primary node does.
* **Memory:** Increasing the cluster-wide memory capacity means Redis can handle a larger key count, which for the `cache` workload means it can take on more caching workloads without the need for functional sharding.

Redis clients send requests directly to the appropriate primary Redis node in both cluster and non-cluster modes, so there is no additional latency between the client and server for proxying requests through an external coordinator.

The above points are all beneficial to performance.

The following point can potentially negatively impact performance under certain conditions:
* **Resharding:** During resharding, redirecting clients to resend their requests to a different shard incurs an extra round trip, increasing latency. However, this overhead only lasts as long as each hash slot migration, and we expect resharding to be very rare (essentially only needed when we grow the cluster). So as noted earlier, because it is rare and tunable, this is not an urgent concern.

## Backup and Restore

- [ ] **Outside of existing backups, are there any other customer data that needs to be backed up for this product feature?**
- [ ] **Are backups monitored?**
- [ ] **Was a restore from backup tested?**

Backups are not needed for cache keys. Keys have a natural lifespan ranging from a few minutes to approximately a week. The application will look up the values elsewhere on a cache miss.

## Monitoring and Alerts

- [ ] **Is the service logging in JSON format and are logs forwarded to logstash?**

Yes. It is resolved in https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/2247.

- [ ] **Is the service reporting metrics to Prometheus?**

Yes. For examples, see dashboard links in earlier sections of this doc.

- [ ] **How is the end-to-end customer experience measured?**

No change. The customer-facing aspects of caching should remain transparent to the user experience.

- [ ] **Do we have a target SLA in place for this service?**

No change. The SLO for the Redis cache database is still applicable in cluster mode.

- [ ] **Do we know what the indicators (SLI) are that map to the target SLA?**

Yes, the dashboard includes the standard Redis SLI metrics. It also includes Cluster-specific metrics for node health, shard health, slot coverage, redirect counters, etc.

- [ ] **Do we have alerts that are triggered when the SLI's (and thus the SLA) are not met?**

Yes, and we will enable those alerts as part of the go-live change issue.

- [ ] **Do we have troubleshooting runbooks linked to these alerts?**

We do, in https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/redis/redis-cluster.md.

- [ ] **What are the thresholds for tweeting or issuing an official customer notification for an outage related to this feature?**

No change. Caching is on the critical path for HTTP request handling, so user-facing services like web, api, git, websockets depend on its availability. If caching is not present, the load will trickle down to patroni and patroni-ci which will lead to slower responses and possibly further outages. This is why we made availability-oriented design choices for the cluster config.

- [ ] **do the oncall rotations responsible for this service have access to this service?**

Yes.

## Responsibility

- [ ] **Which individuals are the subject matter experts and know the most about this feature?**

SREs: @stejacks-gitlab

Backend: @schin1

And more broadly everyone on the Scalability Team has accrued relevant domain expertise.

- [ ] **Which team or set of individuals will take responsibility for the reliability of the feature once it is in production?**

Historically this feature is not formally owned by a team, but Scalability will continue supporting it for the near term.

- [ ] **Is someone from the team who built the feature on call for the launch? If not, why not?**

Yes, we will be actively involved in the rollout.

## Testing

- [ ] **Describe the load test plan used for this feature. What breaking points were validated?**
- [ ] **For the component failures that were theorized for this feature, were they tested? If so include the results of these failure tests.**
- [x] **Give a brief overview of what tests are run automatically in GitLab's CI/CD pipeline for this feature?**

All web and API endpoints use caching, so QA smoke tests give ample coverage. The CI pipeline [now](https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/2327) runs all specs against a Redis Cluster container to check for cross-slot violations.

