_While it is encouraged for parts of this document to be filled out, not all of the items below will be relevant. Leave all non-applicable items intact and add the reasons for why in place of the response._
_This Guide is just that, a Guide. If something is not asked, but should be, it is strongly encouraged to add it as necessary._

## Summary

- [x] **Provide a high level summary of this new product feature. Explain how this change will benefit GitLab customers. Enumerate the customer use-cases.**

To provide horizontal scalability for Redis databases, we will start using Redis Cluster for redis workloads that outgrow vertical scaling and have a compatible usage pattern, starting with `ratelimiting`. See the Architecture section below for background on how the `ratelimiting` Redis DB is used.

Redis has 2 critical resources that are hard to vertically scale beyond a certain point:
* Reaching single-core CPU saturation abruptly and severely impacts Redis latency and the apdex and error rates for its clients.
* Reaching memory saturation can lead to a variety of bad outcomes depending on config and circumstances, including severe latency spikes, service outage, or data loss.

In the past few years, we have addressed Redis CPU and memory saturation through an iterative series of efficiency improvements and by splitting portions of the workload into separate functional partitions. That approach has served us well, but it carries a high cost of effort, a long lead time to deliver results, and does not scale indefinitely.

Redis Cluster will give a predictable and much lower friction means of addressing both CPU and memory saturation, by allowing us to scale up the number of Redis instances as the workload grows. Once implemented, it will natively provide horizontal scaling and online resharding without further client-side changes.

Most of the adoption cost is up-front, to address the constraints of using cluster mode. The benefit is providing a predictable low-effort solution for preventing both near-term and long-term saturation-induced SLO violations.

- [x] **What metrics, including business metrics, should be monitored to ensure will this feature launch will be a success?**

As outlined in the epic https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/823, this is our vision of what "done" looks like:
* The `ratelimiting` workload is running on Redis Cluster instead of vanilla Redis.
* All Redis nodes in the new cluster are below 70% CPU and memory utilization at their daily peak workload.
* All new failure modes we determine to be critical to address before production go-live have a well understood and documented recovery mechanism.
* Metrics, dashboards, alerts, and log ingestion have been added to provide observability for the new cluster.
* Profiling tools work with the cluster's redis build.
* Tamland supports capacity forecasting for Redis Cluster.
* Runbooks documentation provides an overview of Redis Cluster's architecture, observability, and troubleshooting.

Metrics to watch on the [new dashboard](https://dashboards.gitlab.net/d/redis-cluster-ratelimiting-main/redis-cluster-ratelimiting-overview) for `redis-cluster-ratelimiting` include:
* [Redis Cluster Slots OK](https://dashboards.gitlab.net/d/redis-cluster-ratelimiting-main/redis-cluster-ratelimiting-overview?viewPanel=119) - Healthy slot count should remain consistently at 16K.
* [Operation Rate - Primary](https://dashboards.gitlab.net/d/redis-cluster-ratelimiting-main/redis-cluster-ratelimiting-overview?viewPanel=88) - Operations should be spread among all of the cluster's primary nodes.
* [Redis CPU per Node - Primary](https://dashboards.gitlab.net/d/redis-cluster-ratelimiting-main/redis-cluster-ratelimiting-overview?viewPanel=90) - Redis main thread CPU usage should remain below 70% for all primary nodes.
* [Memory Saturation](https://dashboards.gitlab.net/d/redis-cluster-ratelimiting-main/redis-cluster-ratelimiting-overview?viewPanel=90) - Redis resident memory usage should remain below 50% of the total host memory for all nodes.
* [Latency per command type](https://dashboards.gitlab.net/d/redis-cluster-ratelimiting-main/redis-cluster-ratelimiting-overview?viewPanel=102) - Redis server-side latency measurement for each Redis command type (`GET`, `INCR`, `DEL`, etc.).
* [Latency per client type](https://dashboards.gitlab.net/d/redis-cluster-ratelimiting-main/redis-cluster-ratelimiting-overview?viewPanel=36) - Redis client-side latency measurement for each type of client (`web`, `api`, `sidekiq`, etc.)
* [Cluster redirect responses](https://dashboards.gitlab.net/d/redis-cluster-ratelimiting-main/redis-cluster-ratelimiting-overview?viewPanel=25) - Rate of Redis "redirect" responses (`MOVED` or `ASK`). These add an extra round-trip of latency to affected requests, but they should only occur during rare events like resharding or failovers. This dashboard panel is normally empty due to no such redirects occurring.

## Architecture

- [x] **Add architecture diagrams to this issue of feature components and how they interact with existing GitLab components. Make sure to include the following: Internal dependencies, ports, encryption, protocols, security policies, etc.**

From an application architecture perspective, Redis Cluster wholly replaces the existing vanilla Redis and its Sentinels for the `ratelimiting` workload.

The service is still provided by the same Redis software using the same mechanisms for authentication, replication, persistence, etc.

Our current ruby client library [`redis-rb`](https://github.com/redis/redis-rb/tree/v4.8.0) natively supports Redis Cluster.

The main architectural differences arise from Redis Cluster's [design choices](https://redis.io/docs/reference/cluster-spec/) related data sharding and request routing among cluster nodes, as summarized in the next section.

To visualize, here is an example client interacting with a 3-shard Redis Cluster:

```mermaid
graph LR
   C(Client) -- Redis requests --> RS1_P[(Redis shard 1 primary)]
   C -- Redis requests --> RS2_P[(Redis shard 2 primary)]
   C -- Redis requests --> RS3_P[(Redis shard 3 primary)]
   RS1_P -- Replication --> RS1_R1[(Redis shard 1 replica 1)]
   RS1_P -- Replication --> RS1_R2[(Redis shard 1 replica 2)]
   RS2_P -- Replication --> RS2_R1[(Redis shard 2 replica 1)]
   RS2_P -- Replication --> RS2_R2[(Redis shard 2 replica 2)]
   RS3_P -- Replication --> RS3_R1[(Redis shard 3 replica 1)]
   RS3_P -- Replication --> RS3_R2[(Redis shard 3 replica 2)]
```

For context, the `ratelimiting` Redis DB is used in two ways, both of which will migrate to Redis Cluster:
* [`Rack::Attack`](https://github.com/rack/rack-attack#how-it-works) - Rack middleware for rate-limiting by client IP. Redis provides its backing storage. As Rack middleware, this occurs early during request processing. ([development docs](https://docs.gitlab.com/ee/development/application_limits.html#implement-rate-limits-using-rackattack))
* [`Gitlab::ApplicationRateLimiter`](https://gitlab.com/gitlab-org/gitlab/-/tree/master/lib/gitlab/application_rate_limiter) - More targeted resource-specific rate limiting, used by specific Rails Controllers. ([development docs](https://docs.gitlab.com/ee/development/application_limits.html#implement-rate-limits-using-gitlabapplicationratelimiter))

The following sequence diagram copied from [a previous readiness review doc](https://gitlab.com/gitlab-com/gl-infra/readiness/-/blob/master/redis-ratelimiting-gke/index.md#architecture) illustrates the RackAttack control flow:

```mermaid
sequenceDiagram
participant Client
participant RackAttack Middleware
participant Rails
participant Redis RateLimiting

Client ->> RackAttack Middleware: HTTP Request
  RackAttack Middleware -->> Redis RateLimiting: Increment request count
  Redis RateLimiting -->> RackAttack Middleware: Total request count
alt Within rate limit
  RackAttack Middleware ->> Rails: HTTP Request
  Rails ->> Client: 2xx Response
else Rate limited
  RackAttack Middleware ->> Client: 429 Response
end
```

- [x] **Describe each component of the new feature and enumerate what it does to support customer use cases.**

In most ways, Redis Cluster functions as a drop-in replacement for vanilla Redis.

Similarities:
* The Redis nodes use the same `redis-server` binary, just running in cluster mode: [`cluster-enabled yes`](https://github.com/redis/redis/blob/7.0.7/redis.conf#L1572-L1576).
* The same Redis commands and data types are available. However, there are new constraints on multi-key operations to accommodate sharding, as noted below.
* The same replication mechanism is used for asynchronously propagating writes from primaries to replicas.

Differences:
* There are no Sentinels.
  * In vanilla Redis with Sentinels for high availability, clients discover the current Redis primary node by asking any of the Sentinels. Sentinels collaboratively detect node failures and coordinate failovers.
  * In Redis Cluster, clients query any cluster node to discover all nodes, their roles, and which hash slots (i.e. subset of keys) they own. Instead of using Sentinels, the Redis nodes themselves perform node failure detection (via mutual health checks and gossip), and primary nodes vote to authorize a failover to promote a replica of a failed primary node.
* Clients route their requests to the primary node owning the key they are accessing.
  * In vanilla Redis, all keys are owned by the single primary node, so there are no key-specific routing decisions to make.
  * In Redis Cluster, each key is hashed into one of 16K "hash slots", and each hash slot is owned/stored by exactly one of the cluster's primary nodes (and cloned to its replicas). If a client sends a request to an inappropriate node (e.g. due to failover or resharding), the Redis node redirects the client to the appropriate node, letting the client both retry its current request and update its map for routing future requests.
* Multi-key operations can only act on a single hash slot at a time. (See docs: [Redis Cluster data sharding](https://redis.io/docs/management/scaling/#redis-cluster-data-sharding))
  * In vanilla Redis, all operations go to the same primary node, so there is no key-specific routing.
  * In Redis Cluster, clients must send requests to the correct primary that owns each key's hash slot. Most client requests act on a single key and implicitly satisfy this constraint. When clients need to act on multiple keys (e.g. `MGET`), that request must be split along hash slot boundaries. A hash slot is the smallest unit of sharding and is the granularity at which keys are assigned to primary nodes. If a group of keys must be acted on atomically together, those keys must map to the same hash slot so they are guaranteed to be stored on the same primary node. Clients can use hash tags for this purpose.
  * Note: The `ratelimiting` workload has no cross-slot operations (see [analysis](https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/2036)), so we anticipate no cross-slot errors.
* The cluster can be resized via online resharding to accommodate future growth.
  * In vanilla Redis, only vertical scaling is supported.
  * In Redis Cluster, online resharding (which will be a rare event in our environment) migrates hash slots between primary nodes to rebalance the CPU and memory usage among nodes. During the transitional state of migrating a hash slot between primaries, clients may experience transient errors in pipelined operations that access that hash slot. These transient errors can be avoided by either patching or upgrading the clients' redis-rb library.  That work is out of scope for the current project.
* Failovers are authorized and coordinated by primary nodes rather than Sentinels.
  * In vanilla Redis, the Sentinel processes are the voters who must reach consensus on detecting node failure and authorizing a failover.
  * In Redis Cluster, the primary (master) nodes are the voters. If a majority of masters are down, failover cannot be authorized, even if healthy replicas are available for the failed masters. Redis Cluster does not natively address this situation, but we have added a sidecar process (described later) to automate recovery from this scenario.

- [x] **For each component and dependency, what is the blast radius of failures? Is there anything in the feature design that will reduce this risk?**

As a replacement for the existing `redis-ratelimiting` Redis database, the blast radius for failure is identical.

There are some [new failure modes](https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/2071), particularly involving automatic recovery from node failures. To mitigate these, we have [prevented](https://gitlab.com/gitlab-cookbooks/gitlab-redis-cluster/-/merge_requests/5) all copies of a shard from aggregating in a single availability zone and [added a sidecar agent to detect and mitigate](https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/2140) the kind of multi-node failure that Redis Cluster cannot self-heal from. This augments the cluster's native auto-recovery and aims to avoid making the on-call engineer need to take manual actions.

- [x] **If applicable, explain how this new feature will scale and any potential single points of failure in the design.**

Better scaling is the entire goal of adopting Redis Cluster.  We anticipate a roughly equal workload on each of the cluster's primary nodes.  If the workload turns out to have (or later acquire) some very hot keys, we can fine-tune the distribution of hash slots among shards to rebalance the workload.

As the workload grows over time, we can increase the number of shards and redistribute keys among them.

## Operational Risk Assessment

- [x] **What are the potential scalability or performance issues that may result with this change?**

Very low risk.

This change spreads keys among Redis nodes to provide better and more flexible scaling and to avoid the performance impact of CPU saturation.

The cluster nodes have been sized to avoid the risk of worse performance than the current non-clustered Redis primary node (see [sizing analysis](https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/2216#note_1297977036)).

If the workload becomes unevenly spread among the cluster's primary nodes (e.g. if the workload has too many very hot keys on the same shard), online resharding would let us rebalance the workload. Resharding incurs some transitional overhead (i.e. extra latency from multiple round-trips, and pipelined commands may temporarily fail), so we would only do that if the workload distribution was very uneven (which is possible but unlikely).

- [x] **List the external and internal dependencies to the application (ex: redis, postgres, etc) for this feature and how the service will be impacted by a failure of that dependency.**

Replacing Redis with Redis Cluster does not materially change dependencies. The same Redis binaries are used on the server side, and the same client library is used on the client side.

Both sides switch to using the cluster mode of operation. This involves clients routing each request to the appropriate Redis primary node. Clients discover cluster nodes and topology by querying a known Redis node directly (rather than querying Sentinels, which are not used in cluster mode). If a client sends a request to the wrong Redis node (e.g. due to a failover or resharding), Redis will redirect the client to the correct node, and the client will update its local routing map for future queries.

- [x] **Were there any features cut or compromises made to make the feature launch?**

**No data migration:** The `ratelimiting` data is all ephemeral, so for the go-live, we plan to cut over to an empty dataset rather than migrate the existing hit counters. In the future we plan to implement Redis Cluster for some other Redis databases (e.g. `redis-cache`, `redis-persistent`). For those, we will need a more robust data migration plan (which we have already started exploring), but for `ratelimiting` the added complexity was not worthwhile.

**Resharding:** Among the [failure modes we explored](https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/2071#results-summary), we deferred 2 that will cause some transient client errors for certain usage patterns during online resharding. Resharding will be very rare in our environments, so this is not an immediate concern. More details on resharding are in a later section of this doc.

- [x] **List the top three operational risks when this feature goes live.**

We have mitigated the known critical risks.

Highlights:
* Too many voting masters down to reach quorum for recovering cluster availability: https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/2140 created a sidecar service to (a) detect if any shard in the cluster has become masterless and (b) force promotion of a surviving replica if Redis Cluster has not already done so after a configurable delay (15 seconds).
* Master and its replicas could be in the same availability zone: We initialize the cluster topology to spread each shard's nodes among 3 availability zones and [disable `cluster-allow-replica-migration`](https://gitlab.com/gitlab-cookbooks/gitlab-redis-cluster/-/merge_requests/5/diffs) so that nodes remain assigned to their shards. Together, this gives a static distribution of nodes among availability zones: Each shard has 1 node per zone. This partially compensates for Redis Cluster's lack of zone awareness; the remaining concern that too many voting masters can end up in the same zone is addressed by the previous point in this list.
* Client initialization bug: Patched a bug in the `redis-rb` client library, so it correctly parses the response from Redis for initializing the client's map of which nodes own each hash slot. Fixing this avoids every client thread incurring 16K redirects. See https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/2212 for details on the investigation and fix.

- [x] **What are a few operational concerns that will not be present at launch, but may be a concern later?**

**Error rate may temporarily spike during online resharding:**

Low risk: We are documenting this for completeness but do not expect this to be a practical problem for the reasons noted below.

Redis Cluster supports online resharding -- reassigning hash slots to a different primary node and migrating the associated keys.

During resharding, Redis uses redirects (`MOVED` or `ASK` responses) to guide clients to the appropriate node during a hash slot's transitional states. Redis clients can then use the redirect to retry their command, adding latency (another round trip) but getting a correct and consistent result.

Unfortunately, our current version of the `redis-rb` client library (4.8) does not gracefully handle redirects when they occur in a pipelined usage pattern.  When any command in a pipelined set of redis calls results in a redirect response, the pipeline aborts rather than following the redirect for the affected command. This only occurs when accessing keys in hash slots that are actively migrating, but it will increase the client error rate during that timespan.

This is a low urgency concern because:
* Resharding will be very rare in our operating environment (likely less than once per year for any cluster).
* Cross-slot pipelines are an uncommon usage patter in our client code and are not used in the `ratelimiting` workload.
* The transient error rate spike ends as soon as each hash slot migration finishes, so pacing is controllable.
* Upgrading to the next major version of `redis-rb` will address this problem.

For more on this topic, see https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/2112.

- [x] **Can the new product feature be safely rolled back once it is live, can it be disabled using a feature flag?**

Yes. Toggling a feature flag switches between the old and new datastores. We are using MultiStore for this. The `ratelimiting` data does not need to match between the old and new datastores; losing recent writes effectively just means briefly relaxing the rate limits applied to active users.

- [x] **Document every way the customer will interact with this new feature and how customers will be impacted by a failure of each interaction.**

No functional changes to customer interactions. This change just replaces the backing datastore for the existing ratelimiting functionality.

- [x] **As a thought experiment, think of worst-case failure scenarios for this product feature, how can the blast-radius of the failure be isolated?**

Since this is replacing an existing datastore, its scope of impact is circumscribed by the features using that datastore: `Rack::Attack` and `Gitlab::ApplicationRateLimiter`, both of which provide request rate limits in their respective calling contexts.

Rapid rollback via a feature flag toggle limits the duration of failure during the initial rollout.

Proactive analysis of [failure modes](https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/2071#results-summary) for Redis Cluster helped reduce risk of future incidents by making configuration adjustments and supplemental fault detection/mitigation.

Testing in nonprod uncovered a client initialization bug that would have caused extra latency and a spuriously elevated error rate measurement. That has been fixed.

At this point, we appear to have good coverage for known failure modes and good observability for diagnosis of unknown failures.

## Database

- [x] **If we use a database, is the data structure verified and vetted by the database team?**

Not applicable. Redis is a schemaless datastore.

- [x] **Do we have an approximate growth rate of the stored data (for capacity planning)?**

Tamland covers growth forecasting. The same metrics apply for Redis Cluster as for vanilla Redis: max single-core CPU usage on primary nodes and max memory usage on primary nodes.

- [x] **Can we age data and delete data of a certain age?**

Already done. Nothing is changing about Redis keys' TTL.

## Security and Compliance

- **Are we adding any new resources of the following type? (If yes, please list them here or link to a place where they are listed)**
  - [ ] **AWS Accounts/GCP Projects**
  - [ ] **New Subnets**
  - [ ] **VPC/Network Peering**
  - [ ] **DNS names**
  - [ ] **Entry-points exposed to the internet (Public IPs, Load-Balancers, Buckets, etc...)**
  - [ ] **Other (anything relevant that might be worth mention)**

No, none of the above.

- **Secure Software Development Life Cycle (SSDLC)**

Some of this set of questions are not applicable, as this change is replacing a datastore, not developing a new product feature.

  - [x] **Is the configuration following a security standard? (CIS is a good baseline for example)**

The server and client configurations are managed by our standard tools (chef, terraform, helm charts).

The client configuration improves on GitLab's existing pattern for connecting to Redis by introducing support for seamless password rotation. Historically Redis only supported a single default user with a single password, but Redis 6.0 [introduced an ACL system](https://redis.io/docs/management/security/acl/) that supports multiple users and fine grained access control (see also [`ACL SETUSER` docs](https://redis.io/commands/acl-setuser/)). In particular, a Redis user can have multiple valid passwords. For password rotation, we can add a second password to the Redis server ACL for our application user, and then add the new password to Hashicorp Vault. The new password can then be rolled out to all clients gradually before removing the old password from the Redis user. This pattern allows for graceful password rotation without breaking authentication during the transitional phase. This password rotation works with either vanilla Redis or Redis Cluster, and would be useful to implement for all Redis datastores in the future.

  - **All cloud infrastructure resources are labeled according to the [Infrastructure Labels and Tags](https://about.gitlab.com/handbook/infrastructure-standards/labels-tags/) guidelines**

Yes. Issue https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/2243 added the labels listed as required in the handbook.

  - **Were the [GitLab security development guidelines](https://docs.gitlab.com/ee/development/secure_coding_guidelines.html) followed for this feature?**

This project is not new feature development, but yes the associated GitLab MRs followed standard practice.

  - **Do we have an automatic procedure to update the infrastructure (OS, container images, packages, etc...)**

The same level of automation exists for the new Redis Cluster infrastructure as for the existing Redis Sentinel-based infrastructure.

One noteworthy difference is that the new [chef cookbook for redis cluster](https://gitlab.com/gitlab-cookbooks/gitlab-redis-cluster/) deploys and manages the redis binaries and config without using the omnibus `gitlab-ee` package. Omnibus currently provides Redis 6.2, but Redis 7.0 includes [several fixes and improvements specifically for Redis Cluster](https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/1963#note_1151945032). In addition to the version bump, deploying the Redis binaries without the omnibus wrapping also simplified configuration management, avoiding the extra layer of indirection (`gitlab.rb -> redis.conf`). Consequently, the chef roles can directly specify Redis configuration (leaving less opportunity for config drift between `gitlab.rb`, `redis.conf`, and the Redis runtime state).

  - **Do we use IaC (Terraform) for all the infrastructure related to this feature? If not, what kind of resources are not covered?**
    - **Do we have secure static code analysis tools ([kics](https://github.com/Checkmarx/kics) or [checkov](https://github.com/bridgecrewio/checkov)) covering this feature's terraform?**

This infrastructure is part of our [`gitlab-com` Terraform repo](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com) and adheres to all of its standard practices and tooling.

  - **If there's a new terraform state:**
    - [ ] **Where is to terraform state stored, and who has access to it?**

No, the existing standard environment terraform state files are used.

  - [ ] **Does this feature add secrets to the terraform state? If yes, can they be stored in a secrets manager?**

Following standard practice, the redis credentials are stored in vaults (specifically GKMS Vault for Chef and Hashicorp Vault for Kubernetes). Details below.

For Kubernetes-managed containers, the Redis password for the application user (`rails`) is [injected as an external secret](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/-/blob/359e958e37151bc31996a34ce885348078b21dc9/releases/gitlab-external-secrets/values/values.yaml.gotmpl#L318-329) from Hashicorp Vault:

```
$ vault kv get k8s/env/gstg/ns/gitlab/redis-cluster-ratelimiting-rails
$ vault kv metadata get k8s/env/gstg/ns/gitlab/redis-cluster-ratelimiting-rails
```

For the Redis server nodes, the Redis ACL directives specifying the credentials and permissions are stored in GKMS Vault and [injected by Chef](https://gitlab.com/gitlab-cookbooks/gitlab-redis-cluster/-/blob/67a47327b020d498ba77f794e1a723873a77959b/recipes/default.rb#L52-53) into `redis.conf`:

```
$ cd chef-repo/
$ ./bin/gkms-vault-cat redis-cluster gstg
{
  "redis-cluster-ratelimiting": {
    "redis_conf": {
      <REDACTED>
    }
  }
}
```

Similarly, on each Redis server node Chef configures the Prometheus exporter `redis_exporter` to scrape Redis for metrics. For nodes in the `ratelimiting` cluster, it now authenticates to Redis using a dedicated user account (also named `redis_exporter`). Chef [injects this password from GKMS Vault](https://gitlab.com/gitlab-cookbooks/gitlab-exporters/-/blob/6e3cfd1cc3d40c5b9c0c465c61fdf713f22c4d93/recipes/redis_exporter.rb#L35-36):

```
$ ./bin/gkms-vault-cat redis-exporter gstg
{
  "redis_exporter": {
    "redis-cluster-ratelimiting": {
      "env": {
        "REDIS_PASSWORD": <REDACTED>
      }
    }
  }
}
```

  - **If we're creating new containers:**
    - [ ] **Are we using a distroless base image?**
    - **Do we have security scanners covering these containers?**
      - [ ] **`kics` or `checkov` for Dockerfiles for example**
      - [ ] **[GitLab's container](https://docs.gitlab.com/ee/user/application_security/container_scanning/#configuration) scanner for vulnerabilities**

No, this change does not introduce new container images.

- **Identity and Access Management**
  -  [ ] Are we adding any new forms of **Authentication** (New service-accounts, users/password for storage, OIDC, etc...)?
  -  [ ] **Does it follow the least privilege principle?**

No new authentication services are added, but as described above, we have started switching to Redis's new ACL-based auth scheme, which allows for multiple Redis users with multiple passwords and fine-grained permissions.

For both the old and new Redis datastores, clients (e.g. rails containers) use password-based authentication to Redis, with the password stored in a vault and exposed to client containers via an external secret object.

In terms of effects, switching to ACLs means we can now support disruption-free password rotation and can use a separate less privileged user account for scraping Prometheus metrics from Redis nodes.

- **If we are adding any new Data Storage (Databases, buckets, etc...)**

This replaces an existing datastore (`redis-ratelimiting`), with no changes to its storage properties, data classification, encryption, or audit log.

  -  [x] **What kind of data is stored on each system? (secrets, customer data, audit, etc...)**

The data content is request rate counters for the purposes of rate limiting.

  -  [x] **How is data rated according to our [data classification standard](https://about.gitlab.com/handbook/engineering/security/data-classification-standard.html) (customer data is RED)**

These rate counters are per IP and per user, so that any rate-based throttling is scoped narrowly to bad actors. Even though these are only request counters, the per-IP granularity may qualify this as [orange data](https://about.gitlab.com/handbook/security/data-classification-standard.html#orange). It indicates usage of the GitLab SaaS service by a user's IP within a rough timespan (the counter's TTL).

  -  [x] **Is data it encrypted at rest? (If the storage is provided by a GCP service, the answer is most likely yes)**

Yes. For context, Redis is an in-memory database, but it does periodically write backups (RDB) and transaction logs (AOF) to disk. GCP Persistent Disk volumes are natively encrypted. No additional encryption is applied at the guest OS or Redis layers.

  -  [x] **Do we have audit logs on data access?**

Redis queries are not discretely logged. However, standard logging includes shell access to Redis nodes, command-line invocations of `redis-cli` (requires sudo which is logged), and rails console access.

- **Network security (encryption and ports should be clear in the architecture diagram above)**
  -  [ ] **Firewalls follow the least privilege principle (w/ network policies in Kubernetes or firewalls on cloud provider)**
  -  [ ] **Is the service covered by any DDoS protection solution (GCP/AWS load-balancers or Cloudflare usually cover this)**
  -  [ ] **Is the service covered by a WAF (Web Application Firewall)**

No changes to any of these layers, as this is a drop-in replacement for the existing Redis rate-limiting database.

- **Logging & Audit**
  - [x] **Has effort been made to obscure or elide sensitive customer data in logging?**

Not applicable. Individual request details are not logged.

- **Compliance**
    - [x] **Is the service subject to any regulatory/compliance standards? If so, detail which and provide details on applicable controls, management processes, additional monitoring, and mitigating factors.**

No changes to compliance requirements. The data flows are not changing. The same data is stored (request rate counters), and it is accessed by the same internal dependent services (rails containers).


## Performance

- [ ] **Explain what validation was done following GitLab's [performance guidelines](https://docs.gitlab.com/ee/development/performance.html). Please explain which tools were used and link to the results below.**
- [ ] **Are there any potential performance impacts on the database when this feature is enabled at GitLab.com scale?**
- [ ] **Are there any throttling limits imposed by this feature? If so how are they managed?**
- [ ] **If there are throttling limits, what is the customer experience of hitting a limit?**
- [ ] **For all dependencies external and internal to the application, are there retry and back-off strategies for them?**
- [ ] **Does the feature account for brief spikes in traffic, at least 2x above the expected TPS?**

The above questions are generally useful to ask about a new feature, but in this case, we are replacing the backing datastore of the feature that implements throttling for most of the rest of the application code. Instead, the following notes walk through performance-related topics specific to Redis and Redis Cluster.

We expect this change (i.e. migrating from Redis to Redis Cluster) to present comparable performance under normal conditions and to avoid performance regressions under certain abusive workloads.

Redis capacity is generally bound by 2 machine resources: single-core CPU usage and memory usage.

Switching to clustered mode increases both of those resources, making them harder to saturate and easier to grow as needed:
* **CPU:** Redis latency and throughput regressions are generally caused by starving for CPU time, and switching to a sharded architecture reduces the odds of that occurring. That risk is further reduced by choosing VM specs that can tolerate any one node taking the same amount of traffic as the current primary node does.
* **Memory:** Increasing the cluster-wide memory capacity means Redis can handle a larger key count, which for the `ratelimiting` workload means it can tolerate larger spikes of certain kinds of abuse (e.g. high IP cardinality).

Redis clients send requests directly to the appropriate primary Redis node in both cluster and non-cluster modes, so there is no additional latency between the client and server for proxying requests through an external coordinator.

The above points are all beneficial to performance.

The following 2 points can potentially negatively impact performance under certain conditions:
* **Resharding:** During resharding, redirecting clients to resend their requests to a different shard incurs an extra round trip, increasing latency. However, this overhead only lasts as long as each hash slot migration, and we expect resharding to be very rare (essentially only needed when we grow the cluster). So as noted earlier, because it is rare and tunable, this is not an urgent concern.
* **Enabling persistence:** For `ratelimiting`, persistence is considered optional, because it is operationally acceptable to zero the rate counters during rare events (e.g. multi-node failures). The current (non-clustered) `redis-ratelimiting` database disables persistence. For the new cluster nodes, we have tentatively enabled AOF persistence (`appendonly yes`). Performance in the `gstg` environment appears to show no negative impact, but that environment's request rate is far less than production's. For context, Redis appends every write to its AOF file, but those writes are normally cached by kernel. Redis only explicitly asks kernel to flush those writes to disk once per second. Redis runs those `fsync` syscalls from a separate thread to try to avoid impacting foreground request processing. This all aims to make it unlikely to impact Redis response latency. However, if the backing disk cannot keep up with flushing writes system-wide, there is a corner case where appending logs to the AOF file could become foreground writes instead of cached writes. (See kernel docs for `vm.dirty_ratio`.) I suspect this bad outcome is very unlikely, given the disk's high IOPS ceiling, the database's small size, and the filesystem cache's large size. However, making AOF appends become foreground synchronous writes instead of cached writes would terribly degrade Redis throughput, so this risk is low probability but high impact. If it turns out to be a practical problem, we have several options: disable AOF, enable `no-appendfsync-on-rewrite`, increase the disk size to gain more IOPS, or tune the kernel to allow more dirty pages before forcing foreground synchronous writes.

## Backup and Restore

- [ ] **Outside of existing backups, are there any other customer data that needs to be backed up for this product feature?**
- [ ] **Are backups monitored?**
- [ ] **Was a restore from backup tested?**

Backups are not needed for rate-limiting counters. They have a naturally short lifespan (1 minute), and resetting them to zero effectively just briefly relaxes the rate limits for all users.

## Monitoring and Alerts

- [ ] **Is the service logging in JSON format and are logs forwarded to logstash?**

Not yet, but soon. Log ingestion is being addressed by https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/2247.

- [ ] **Is the service reporting metrics to Prometheus?**

Yes. For examples, see dashboard links in earlier sections of this doc.

- [ ] **How is the end-to-end customer experience measured?**

No change. The customer-facing aspects of rate limiting should remain transparent to the user experience.

- [ ] **Do we have a target SLA in place for this service?**

No change. The SLO for the Redis ratelimiting database is still applicable in cluster mode.

- [ ] **Do we know what the indicators (SLI) are that map to the target SLA?**

Yes, the dashboard includes the standard Redis SLI metrics. It also includes Cluster-specific metrics for node health, shard health, slot coverage, redirect counters, etc.

- [ ] **Do we have alerts that are triggered when the SLI's (and thus the SLA) are not met?**

Yes, and we will enable those alerts as part of the go-live change issue.

- [ ] **Do we have troubleshooting runbooks linked to these alerts?**

We will.  Runbooks MR is in progress: https://gitlab.com/gitlab-com/runbooks/-/merge_requests/5519 

- [ ] **What are the thresholds for tweeting or issuing an official customer notification for an outage related to this feature?**

No change. Rate limiting is on the critical path for HTTP request handling, so user-facing services like web, api, git, websockets depend on its availability. This is why we made availability-oriented design choices for the cluster config.

- [ ] **do the oncall rotations responsible for this service have access to this service?**

Yes.

## Responsibility

- [ ] **Which individuals are the subject matter experts and know the most about this feature?**

SREs: @igorwwwwwwwwwwwwwwwwwwww, @msmiley

Backend: @schin1

And more broadly everyone on the Scalability Team has accrued relevant domain expertise.

- [ ] **Which team or set of individuals will take responsibility for the reliability of the feature once it is in production?**

Historically this feature is not formally owned by a team, but Scalability will continue supporting it for the near term.

- [ ] **Is someone from the team who built the feature on call for the launch? If not, why not?**

Yes, we will be actively involved in the rollout.

## Testing

- [ ] **Describe the load test plan used for this feature. What breaking points were validated?**
- [ ] **For the component failures that were theorized for this feature, were they tested? If so include the results of these failure tests.**
- [ ] **Give a brief overview of what tests are run automatically in GitLab's CI/CD pipeline for this feature?**

All web and API endpoints use rate-limiting, so QA smoke tests give ample coverage.

Redis client error rate metrics correctly detected a performance-related bug in redis-rb, which we investigated and resolved in https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/2212. After fixing that, the [error ratio](https://dashboards.gitlab.net/d/redis-cluster-ratelimiting-main/redis-cluster-ratelimiting-overview?orgId=1&var-PROMETHEUS_DS=Global&var-environment=gstg&var-shard=All&viewPanel=3422679610&from=now-6h&to=now) dropped to 0 as expected.

The shard-checker sidecar testing notes are in its MR [here](https://gitlab.com/gitlab-cookbooks/gitlab-redis-cluster/-/merge_requests/4#note_1301198051) and [here](https://gitlab.com/gitlab-cookbooks/gitlab-redis-cluster/-/merge_requests/4#note_1302828244).
