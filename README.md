# Production Library and Readiness

This project contains a library of blueprints and designs, and their outcome - production readiness review.

## Overview

The Infrastructure Library contains a snapshot in time of our thinking about the problems we are solving. This happens through two stages:

1. We begin with **blueprints**, which define and scope a problem and provide options on how we might approach said problem.
1. The **design** stage, which fleshes out the approach and provides the technical design on the solution.

Once we have executed on the design, we create a production readiness review whenever there is a significant change to GitLab.com infrastructure. This may be a new service, a new feature, or a major infrastructure change.

After completing the readiness review, we update the relevant *architectural* and/or *operational* documentation.

## Getting Started

See our handbook page [Production Readiness Review](https://about.gitlab.com/handbook/engineering/infrastructure/production/readiness/)

## Workflow

See the [workflow documentation](doc/workflow.md) for more details.

Note: Some documents are [Controlled Documents](https://about.gitlab.com/handbook/engineering/security/controlled-document-procedure.html) and should only be merged by codeowners. They will have a heading indicating they are controlled documents.

## Directory structure

The directory structure of this project has

* documentation directory, `doc`
* directory storing blueprints and designs, `library`
* a single directory per change. In each directory there will be one or more markdown documents that break the change into different audiences for reviews. It is not necessary to have more than one markdown document, it will depend on the size of the change.

### Example

```
doc
  \
   - workflow.md
library
  \
   - index.md
   - <topic>/index.md
<feature name>
  \
   - overview.md
      * architecture overview
      * deployment and timeline
      * risk assessment
      * security considerations, secrets
      * database (pg/redis) impacts
   - monitoring.md
      * metrics
      * logging
      * alerting
   - testing.md
      * all testing procedures
```
