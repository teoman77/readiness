# ExternalDNS

## Summary

Some services rely on hostnames for inter-service communications. ExternalDNS allows us to bind a service to a DNS entry managed via Google Cloud DNS, allowing us to automatically keep up-to-date services to hostname mappings. No external customer will interact directly with this feature, but it will become a critical part of the infrastructure, as user-facing services will become dependent on the correct functioning of it. At the moment, the only service depending on this feature is Redis clusters running on Kubernetes.

## Architecture

```mermaid
sequenceDiagram
  participant k8s as Kubernetes API
  participant dns as ExternalDNS
  participant gcp as Google Cloud DNS
  participant gr as Gitlab Rails
  participant r as Redis

  dns->>k8s: queries for objects annotated with `external-dns.alpha.kubernetes.io`
  dns->>gcp: configures a DNS record pointing to the redis service running in k8s
  gr->>gcp: asks for the IP that a given FQDN resolves to
  gcp->>gr: points to the internal IP of the Redis cluster
  gr->>r: communicates over internal network with Redis
```

Under this architecture, Google Cloud DNS is the Single Point Of Failure; if the DNS records pointing at our Redis clusters are not available, Rails will not be able to communicate with Redis, which depending on the cluster can result in a site-wide outage. It is also critical that ExternalDNS is configured properly so it can create, update, or delete such DNS records.

## Operational Risk Assessment

- [ ] **What are the potential scalability or performance issues that may result with this change?**

ExternalDNS is deployed as a Kubernetes Workload. The number of records it creates depends on the amount of Kubernetes services and nodes it's configured for. We can consider the number of services and nodes to be relatively stable, or in any case to not increase at a rate where the number of DNS records to handle becomes a problem. Thus, we consider that no scalability or performance issues should arise on this feature.

- [ ] **List the external and internal dependencies to the application (ex: redis, postgres, etc) for this feature and how it will be impacted by a failure of that dependency.**

  - Google Cloud DNS: Single point of failure. If DNS propagation lags, discovery of new services or IP changes for existing services will also lag. In case of downtime, the feature won't work at all.
  - Kubernetes API: Queried by ExternalDNS to look for objects that want a DNS entry maintained for them. During cluster maintenance operations, our zonal cluster, which currently only has a single API node, will no longer respond to ExternalDNS' queries. This is only a problem if a Pod or Service is changed at the time maintenance is occurring. Since the rate of maintenance operations and Pod/Services changes in our infrastructure are both low, we expect this to be a rare occurrence.

- [ ] **Were there any features cut or compromises made to make the feature launch?**

N/A

- [ ] **List the top three operational risks when this feature goes live.**

  - ExternalDNS doesn't generate the needed DNS records, or the records point to the wrong address. We must verify (e.g. with `dig`) that the records exist and are correct before changing the configuration on GitLab Rails to point to them.
  - ExternalDNS overrides or deletes the wrong DNS entry. At the moment, we create a DNS zone and a corresponding Service Account for each Kubernetes cluster (regional or zonal), but each Service Account is given the permission `roles/dns.admin`, meaning it can manage any DNS zone inside its GCP project. Alternatively, we could use [per DNS zone IAM permissions](https://cloud.google.com/dns/docs/zones/iam-per-resource-zones), which would address the following risks:
    - ExternalDNS modifying an entry for the wrong GKE zone. This could happen if for example the wrong DNS prefix is specified by our deployments (for example [here](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/tanka-deployments/blob/9cb41ef91e73c2cfe3c21623da5f5d514d184971/lib/redis/redis.libsonnet#L133)). [We have configured ExternalDNS with `txtOwnerId`](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-helmfiles/-/blob/4c68ffd6674ef48b69a5bf9d8d4237119f302c3d/releases/external-dns/values.yaml.gotmpl#L23), which associates ownership of each DNS record to our environments, and should help prevent this. See https://github.com/kubernetes-sigs/external-dns/#the-latest-release for more details
    - ExternalDNS modifying or deleting an entry not related to GKE. We only manage GKE-related DNS entries on Google Cloud DNS, with other entries handled via Cloudflare. Even if ExternalDNS create records not related to GKE, they will to be served to clients.

- [ ] **What are a few operational concerns that will not be present at launch, but may be a concern later?**
- [ ] **Can the new product feature be safely rolled back once it is live, can it be disabled using a feature flag?**

In case of catastrophic failure, ExternalDNS can be bypassed completely by manually creating or updating the relevant DNS entries (via the Google Cloud Console, `gcloud` command line tool, or via Google's API) to point to the internal IP of the relevant service. Note that with manually created DNS entries we don't get the benefit of automatically tracking changes to the service's internal IP.

Another bypass option is to point clients (e.g. gitlab-rails) directly to the IPs of the redis resources. This approach also needs manual action any time the resources IPs change.

- [ ] **Document every way the customer will interact with this new feature and how customers will be impacted by a failure of each interaction.**

- SREs annotate Kubernetes resources with `external-dns.alpha.kubernetes.io` and a `hostname` to have ExternalDNS manage a DNS entry for that resource. For example [as done here in our tanka-deployments](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/tanka-deployments/-/blob/54b29fe05ea3f27fedac1b9c581dd18f5bf5564d/lib/thanos/store.libsonnet#L163-166), which results in the following YAML:

  ```yml
  apiVersion: v1
  kind: Service
  metadata:
    annotations:
      external-dns.alpha.kubernetes.io/hostname: thanos-store-internal-0.pre.gke.gitlab.net.
  # ...
  ```

  In case of failure, for example because of misconfiguration of the Service Account used by ExternalDNS, the entry will not be generated properly. SREs should always verify the DNS entries created before routing traffic to them.

- [ ] **As a thought experiment, think of worst-case failure scenarios for this product feature, how can the blast-radius of the failure be isolated?**

  - Google Cloud DNS outage leads to the feature not working. To mitigate this risk we could deploy a second instance of ExternalDNS that keeps a parallel set of records on another DNS provider (like AWS Route 53). See the SLA for Google Cloud DNS [here](https://cloud.google.com/dns/sla)
## Database

- [ ] **If we use a database, is the data structure verified and vetted by the database team?**
- [ ] **Do we have an approximate growth rate of the stored data (for capacity planning)?**
- [ ] **Can we age data and delete data of a certain age?**

## Security and Compliance

Data for this feature consists of DNS names and internal IPs. Both will be world readable, as they will be published on public Google Cloud DNS servers. The DNS names are form from the name of GKE nodes and services, none of which are sensitive data. The internal IPs are only accessible inside the GCP private network.

## Monitoring and Alerts

- [ ] **Is the service logging in JSON format and are logs forwarded to logstash?**

No. We point to the Stackdriver logging in our runbooks https://gitlab.com/gitlab-com/runbooks/-/tree/master/docs/external-dns#logging

- [ ] **Is the service reporting metrics to Prometheus?**

Yes. See https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-helmfiles/-/merge_requests/813 and https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-helmfiles/-/merge_requests/828

- [ ] **How is the end-to-end customer experience measured?**

N/A

- [ ] **Do we have a target SLA in place for this service?**

No. See [this discussion](https://gitlab.com/gitlab-com/runbooks/-/merge_requests/4791#note_1015173006)

- [ ] **Do we know what the indicators (SLI) are that map to the target SLA?**

N/A

- [ ] **Do we have alerts that are triggered when the SLI's (and thus the SLA) are not met?**

N/A, but we did add custom alerts on https://gitlab.com/gitlab-com/runbooks/-/merge_requests/4791

- [ ] **Do we have troubleshooting runbooks linked to these alerts?**

We have the main service runbook https://gitlab.com/gitlab-com/runbooks/-/tree/master/docs/external-dns, but it only lists
the location of the logs. No specific troubleshooting procedure is defined at the moment.

- [ ] **What are the thresholds for tweeting or issuing an official customer notification for an outage related to this feature?**

In case of an outage or malfunction of this feature, the Redis service will not function properly. We can handle communications
as we would for any other Redis-related incidents in that case.

- [ ] **Do the oncall rotations responsible for this service have access to this service?**

Yes

## Responsibility

- [ ] **Which individuals are the subject matter experts and know the most about this feature?**
- [ ] **Which team or set of individuals will take responsibility for the reliability of the feature once it is in production?**

Infrastructure

- [ ] **Is someone from the team who built the feature on call for the launch? If not, why not?**

## Testing

- [ ] **Describe the load test plan used for this feature. What breaking points were validated?**
- [ ] **For the component failures that were theorized for this feature, were they tested? If so include the results of these failure tests.**
- [ ] **Give a brief overview of what tests are run automatically in GitLab's CI/CD pipeline for this feature?**

