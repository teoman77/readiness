The Readiness Review document is designed to help you prepare your features and services for the GitLab Production Platforms.
Please engage with the relevant teams as soon as possible to begin review even if there are incomplete items below.
All sections should be completed up to the current [maturity level](https://docs.gitlab.com/ee/policy/experiment-beta-support.html).
For example, if the target maturity is "Beta", then items under "Experiment" and "Beta" should be completed.

_While it is encouraged for parts of this document to be filled out, not all of the items below will be relevant. Leave all non-applicable items intact and add 'N/A' or reasons for why in place of the response._
_This Guide is just that, a Guide. If something is not asked, but should be, it is strongly encouraged to add it as necessary._

# Internal API service

## Summary

Internal API is used to serve the traffic flowing to the `/api/v4/internal` endpoint by various services, including GitLab Shell and KAS. Adding this service will help us reduce the amount of traffic served by the public load balancer. It should also free up some traffic from the main API, which will help with more granular scaling. Having a separate service with its own SLO will help us reason better about user impact. The current focus of internal-api Epic is limited to Gitlab-shell only. Migrating the traffic to internal-api for KAS is out of scope.

## Metrics

- [Apdex](https://dashboards.gitlab.net/d/internal-api-main/internal-api-overview?orgId=1) for the Internal-API service.
- [Pod](https://dashboards.gitlab.net/goto/OJp8UO14k?orgId=1) resources and limits
- [PodInfo](https://dashboards.gitlab.net/goto/upR2_d1Vk?orgId=1)

## Related Epic

- [Use a separate deployment for the internal API for GitLab SSHd](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/879)

## Architecture

### Current

```mermaid
sequenceDiagram
  participant C as Client
  participant H as HAProxy
  participant gs as GitLab_Shell
  participant w as Workhorse
  participant r as Rails
  participant gi as Gitaly

  C->>H: git-clone
    Note over C,H: git clone git@gitlab.com:project/repo.git
  H->>gs: git-clone
    Note over H,gs: communication through GLB/nginx Ingress
  gs->>w: [authenticate]
    Note over gs,w: communication through Kubernetes Service to webservice endpoint
  w->>r: GET /api/v4/internal/authorized_keys
    Note over w,r: communication via API http://gitlab-webservice:8181
  r->>w: authorized_key
  w->>gs: [authenticate success]
  gs->>gi: command: git-upload-pack
  gi->>gs: SSHUploadPack
  gs->>H: git data
  H->>C: git data
```

### Proposed

```mermaid
sequenceDiagram
  participant C as Client
  participant H as HAProxy
  participant gs as GitLab_Shell
  participant w as Internal-API (Workhorse)
  participant ia as Internal-API (Rails)
  participant gi as Gitaly


  C->>H: git-clone
    Note over C,H: git clone git@gitlab.com:project/repo.git
  H->>gs: git-clone
    Note over H,gs: communication through GLB
  gs->>w: [authenticate]
    Note over gs,w: communication via API http://gitlab-webservice-internal-api:8080
  w->>ia: GET /api/v4/internal/authorized_keys
    Note over w,ia: communication via API http://localhost:8181
  ia->>w: authorized_key
  w->>gs: [authenticate success]
  gs->>gi: command: git-upload-pack
  gi->>gs: SSHUploadPack
  gs->>H: git data
  H->>C: git data
```

---

Internal-API service is similar to Gitlab API deployment and configured as additional deployment within official Gitlab helm chart. Deployment resource contains both `Workhorse` and `Rails` containers in a same pod. Gitlab-shell service talks to Internal-API through internal ClusterIP of Internal-API service. It connects to port 8080 to execute `/api/v4/internal/authorized_keys` request. `Workhorse` communicates with `Rails` container through loopback interface on port 8181 to receive `authorized_key`

## Experiment

Internal API service at ~"Readiness::Beta" maturity level. It's been deployed on the following environments:

- `pre`
- `gstg`
- `gprd`

The traffic to this service enabled on the following environments:

- `pre`
- `gstg-cny`
- `gstg main` regional clusters `us-east-1{b,c,d}`

### Service Catalog

_The items below will be reviewed by the Reliability team._

- [x] Link to the [service catalog entry](https://gitlab.com/gitlab-com/runbooks/-/merge_requests/5338) for the service. Ensure that the following items are present in the service catalog, or listed here:
  - Link to or provide a high-level summary of this new product feature.
    - [Summary](#Summary)
  - Link to the [Architecture Design Workflow](https://about.gitlab.com/handbook/engineering/architecture/workflow/) for this feature, if there wasn't a design completed for this feature please explain why.
  - List the feature group that created this feature/service and who are the current Engineering Managers, Product Managers and their Directors.
    - Group: Deployment Systems
    - Eng Manager: @dawsmith
    - Director: @marin
  - List individuals are the subject matter experts and know the most about this feature.
    - @vglafirov
    - @skarbek
    - @jarv
  - List the team or set of individuals will take responsibility for the reliability of the feature once it is in production.
    - Reliability, Deployment Systems
  - List the member(s) of the team who built the feature will be on-call for the launch.
    - Reliability
  - List the external and internal dependencies to the application (ex: redis, postgres, etc) for this feature and how the service will be impacted by a failure of that dependency.
    - webservice-api
    - gitlab-shell

### Infrastructure

_The items below will be reviewed by the Reliability team._

- [x] Do we use IaC (e.g., Terraform) for all the infrastructure related to this feature? If not, what kind of resources are not covered?
  - Internal API has been deployed using [k8s-workloads configuration change](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/-/merge_requests/2540)
- [x] Is the service covered by any DDoS protection solution (GCP/AWS load-balancers or Cloudflare usually cover this)?
  - This is an internal ClusterIP service and doesn't require any third-party DDoS protection solutions as it's not exposed to Internet. DDoS protection is defined on `gitlab-shell` external endpoint, which prevents flooding Internal API with requests.
- [x] Are all cloud infrastructure resources labeled according to the [Infrastructure Labels and Tags](https://about.gitlab.com/handbook/infrastructure-standards/labels-tags/) guidelines?
  - There are no cloud infrastructure changes involved

### Operational Risk

_The items below will be reviewed by the Reliability team._

- [x] List the top three operational risks when this feature goes live.
  - Additional service that might have an effect on overall reliability metrics in case of failure
  - Wrong resource allocations or autoscaling configuration might lead to `gitlab-shell` services performance degradation
  - If Internal API service is not available any `git` operations are not possible.
- [x] For each component and dependency, what is the blast radius of failures? Is there anything in the feature design that will reduce this risk?
  - In case of failure `gitlab-shell` service might not be able to authenticate git operations

### Monitoring and Alerting

_The items below will be reviewed by the Reliability team._

- [x] Link to the [metrics catalog](https://gitlab.com/gitlab-com/runbooks/-/tree/master/metrics-catalog/services) for the service
  - [Internal API Metrics Catalog service](https://gitlab.com/gitlab-com/runbooks/-/blob/master/metrics-catalog/services/internal-api.jsonnet)
  - [Internal API Grafana dashboards](https://gitlab.com/gitlab-com/runbooks/-/tree/master/dashboards/internal-api)

### Deployment

_The] items below will be reviewed by the Delivery team._

- [x] Will a [change management issue](https://about.gitlab.com/handbook/engineering/infrastructure/change-management/) be used for rollout? If so, link to it here.
  - [Shifting internal traffic to Internal-API on GPRD-CNY and GPRD](https://gitlab.com/gitlab-com/gl-infra/production/-/issues/17191)
- [x] Can the new product feature be safely rolled back once it is live, can it be disabled using a feature flag?
  - It cannot be disabled using feature flag
  - It can be disabled by reconfiguring webservice endpoint configuration from `k8s-workloads` repository. [Link to the configuration parameter](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/-/blob/master/releases/gitlab/values/gstg-cny.yaml.gotmpl?ref_type=heads#L40)
- [x] How are the artifacts being built for this feature (e.g., using the [CNG](https://gitlab.com/gitlab-org/build/CNG/) or another image building pipeline).
  - This a configuration change. Service using pre existing gitlab-webservice artifact.

### Security Considerations

_The items below will be reviewed by the Infrasec team._

- [x] Link or list information for new resources of the following type:
  - AWS Accounts/GCP Projects: There are no new resources added
  - New Subnets: N/A
  - VPC/Network Peering: N/A
  - DNS names: N/A
  - Entry-points exposed to the internet (Public IPs, Load-Balancers, Buckets, etc...): This is internal ClusterIP service and has no endpoints exposed to the Internet
  - Other (anything relevant that might be worth mention):
    - Internal Kubernetes service DNS name: `webservice-internal-api`
- [x] Were the [GitLab security development guidelines](https://docs.gitlab.com/ee/development/secure_coding_guidelines.html) followed for this feature?
  - This service is using the same codebase as the main webservice-api. All security development guidelines applied to this service as well.
- [x] Was an [Application Security Review](https://handbook.gitlab.com/handbook/security/security-engineering/application-security/appsec-reviews/) requested, if appropriate? Link it here.
  - Application for this service passed all security reviews at the same time as webservice-api.
- [x] Do we have an automatic procedure to update the infrastructure (OS, container images, packages, etc...). For example, using unattended upgrade or [renovate bot](https://github.com/renovatebot/renovate) to keep dependencies up-to-date?
  - We use auto_deploy pipeline in order to keep this service updated. Deployment happens at the same time as other services. No updates for the infrastructure required.
- [x] For IaC (e.g., Terraform), is there any secure static code analysis tools like ([kics](https://github.com/Checkmarx/kics) or [checkov](https://github.com/bridgecrewio/checkov))? If not and new IaC is being introduced, please explain why.
  - Terraform wasn't used to deploy this service.
- [x] If we're creating new containers (e.g., a Dockerfile with an image build pipeline), are we using `kics` or `checkov` to scan Dockerfiles or [GitLab's container](https://docs.gitlab.com/ee/user/application_security/container_scanning/#configuration) scanner for vulnerabilities?
  - No new containers created

### Identity and Access Management

_The items below will be reviewed by the Infrasec team._

- [x] Are we adding any new forms of Authentication (New service-accounts, users/password for storage, OIDC, etc...)?
  - We creating a new kubernetes service account specific for this service.
- [x] Was effort put in to ensure that the new service follows the [least privilege principle](https://en.wikipedia.org/wiki/Principle_of_least_privilege), so that permissions are reduced as much as possible?
  - This service uses dedicated service account and NetworkPolicies, which is similar to webservice-api.
- [x] Do firewalls follow the least privilege principle (w/ network policies in Kubernetes or firewalls on cloud provider)?
  - Standard NetworkPolicies for `gitlab-webservice` has been applied
- [x] Is the service covered by a [WAF (Web Application Firewall)](https://cheatsheetseries.owasp.org/cheatsheets/Secure_Cloud_Architecture_Cheat_Sheet.html#web-application-firewall) in [Cloudflare](https://gitlab.com/gitlab-com/runbooks/-/tree/master/docs/cloudflare#how-we-use-page-rules-and-waf-rules-to-counter-abuse-and-attacks)?
  - Service hasn't been exposed to outside of the GKE cluster network

### Logging, Audit and Data Access

_The items below will be reviewed by the Infrasec team._

- [x] Did we make an effort to redact customer data from logs?
  - All customer redaction practices and policies defined for other webservices applied for this service as well by default.
- [x] What kind of data is stored on each system (secrets, customer data, audit, etc...)?
  - This is stateless service and no customer data has been stored. However this service is acting as a proxy for accessing customer data.
- [x] How is data rated according to our [data classification standard](https://about.gitlab.com/handbook/engineering/security/data-classification-standard.html) (customer data is RED)?
  - Since this service process customer data, it should be classified as Red,web-application-firewall
- [x] Do we have audit logs for when data is accessed? If you are unsure or if using Reliability's central logging and a new pubsub topic was created, create an issue in the [Security Logging Project](https://gitlab.com/gitlab-com/gl-security/engineering-and-research/security-logging/security-logging/-/issues/new?issuable_template=add-remove-change-log-source) using the `add-remove-change-log-source` template.
- [x] Ensure appropriate logs are being kept for compliance and requirements for retention are met.
  - There is no change in loging. Same logging principles applied for Internal-API as webservice-api. Logging runbook can be found [here](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/logging/README.md#what-are-we-logging)
- [x] If the data classification = Red for the new environment, please create a [Security Compliance Intake issue](https://gitlab.com/gitlab-com/gl-security/security-assurance/security-compliance-commercial-and-dedicated/security-compliance-intake/-/issues/new?issue[title]=System%20Intake:%20%5BSystem%20Name%20FY2%23%20Q%23%5D&issuable_template=intakeform). Note this is not necessary if the service is deployed in existing Production infrastructure.
  - Service has been deployed on existing Production infrastructure

## Beta

### Monitoring and Alerting

_The items below will be reviewed by the Reliability team._

- [x] Link to examples of logs on https://logs.gitlab.net
  - [Internal-API logs](https://nonprod-log.gitlab.net/app/r/s/ItG9f)
- [x] Link to the [Grafana dashboard](https://dashboards.gitlab.net) for this service.
  - [Overview](https://dashboards.gitlab.net/goto/Q1gVl94IR?orgId=1)
  - [Capacity review](https://dashboards.gitlab.net/goto/6HqxDzNIg?orgId=1)
  - [Kube Containers Detail](https://dashboards.gitlab.net/goto/WI-fvkNIg?orgId=1)
  - [Kube Deployment Detail](https://dashboards.gitlab.net/goto/Gn4PvkNSg?orgId=1)
  - [Pod Info](https://dashboards.gitlab.net/goto/LFJsvzNIR?orgId=1)
  - [Rails Controller](https://dashboards.gitlab.net/goto/r7kgOzHIR?orgId=1)
  - [Regional Detail](https://dashboards.gitlab.net/goto/78NzdzHIg?orgId=1)

### Backup, Restore, DR and Retention

_The items below will be reviewed by the Reliability team._

- [x] Are there custom backup/restore requirements?
  - This is stateless service. No backups required
- [x] Are backups monitored?
  - This is stateless service, no backups required.
- [x] Was a restore from backup tested?
  - This is stateless service, no backups required.
- [x] Link to information about growth rate of stored data.
  - This is stateless service, no backups required.

### Deployment

_The items below will be reviewed by the Delivery team._

- [x] Will a [change management issue](https://about.gitlab.com/handbook/engineering/infrastructure/change-management/) be used for rollout? If so, link to it here.
- [x] Does this feature have any version compatibility requirements with other components (e.g., Gitaly, Sidekiq, Rails) that will require a specific order of deployments?
  - No
- [x] Is this feature validated by our [QA blackbox tests](https://gitlab.com/gitlab-org/gitlab-qa)?
  - Yes
- [x] Will it be possible to roll back this feature? If so explain how it will be possible.
  - It can be rolled back by reverting configuration change commit or changing the [endpoint](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/-/blob/master/releases/gitlab/values/gstg-cny.yaml.gotmpl?ref_type=heads#L40) for `gitlab-shell` service.

### Security

_The items below will be reviewed by the InfraSec team._

- [ ] Put yourself in an attacker's shoes and list some examples of "What could possibly go wrong?". Are you OK going into Beta knowing that?
- [ ] Link to any outstanding security-related epics & issues for this feature. Are you OK going into Beta with those still on the TODO list?

## General Availability

### Monitoring and Alerting

_The items below will be reviewed by the Reliability team._

- [x] Link to the troubleshooting runbooks.
- Similar troubleshooting guidelines and runbooks can be applied to this service as the runbooks to [webservice-api](https://gitlab.com/gitlab-com/runbooks/-/tree/master/docs/api?ref_type=heads#troubleshooting-pointers) services
- [x] Link to an example of an alert and a corresponding runbook.
  - [Internal-API Alerting](https://alerts.gitlab.net/#/alerts?silenced=false&inhibited=false&active=true&filter=%7Btier%3D%22sv%22%2C%20type%3D%22internal-api%22%7D)
- [x] Confirm that on-call Reliability SREs have access to this service and will be on-call. If this is not the case, please add an explanation here.
  - on-call Reliability SREs have access to this service.

### Operational Risk

_The items below will be reviewed by the Reliability team._

- [ ] Link to notes or testing results for assessing the outcome of failures of individual components.
- [ ] What are the potential scalability or performance issues that may result with this change?
- [ ] What are a few operational concerns that will not be present at launch, but may be a concern later?
- [ ] Are there any single points of failure in the design? If so list them here.
- [ ] As a thought experiment, think of worst-case failure scenarios for this product feature, how can the blast-radius of the failure be isolated?

### Backup, Restore, DR and Retention

_The items below will be reviewed by the Reliability team._

- [ ] Are there any special requirements for Disaster Recovery for both Regional and Zone failures beyond our current Disaster Recovery processes that are in place?
- [ ] How does data age? Can data over a certain age be deleted?

### Performance, Scalability and Capacity Planning

_The items below will be reviewed by the Reliability team._

- [ ] Link to any performance validation that was done according to [performance guidelines](https://docs.gitlab.com/ee/development/performance.html).
- [ ] Link to any load testing plans and results.
- [ ] Are there any potential performance impacts on the Postgres database or Redis when this feature is enabled at GitLab.com scale?
- [ ] Explain how this feature uses our [rate limiting](https://gitlab.com/gitlab-com/runbooks/-/tree/master/docs/rate-limiting) features.
- [ ] Are there retry and back-off strategies for external dependencies?
- [ ] Does the feature account for brief spikes in traffic, at least 2x above the expected rate?

### Deployment

_The items below will be reviewed by the Delivery team._

- [ ] Will a [change management issue](https://about.gitlab.com/handbook/engineering/infrastructure/change-management/) be used for rollout? If so, link to it here.
- [ ] Are there healthchecks or SLIs that can be relied on for deployment/rollbacks?
- [ ] Does building artifacts or deployment depend at all on [gitlab.com](https://gitlab.com)?
