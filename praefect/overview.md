## Summary

- [x] Provide a high level summary of this new product feature. Explain how this change will benefit GitLab customers. Enumerate the customer use-cases.

Praefect is a new component to our git storage infrastructure that will enable Gitaly HA in GitLab. It consists of a proxy that will handle RPC calls to Gitaly and manage replication and failover of redundant storage nodes.

For more information about Praefect see the [Gitaly HA design documentation](https://gitlab.com/gitlab-org/gitaly/blob/master/doc/design_ha.md)

- [x] What metrics, including business metrics, should be monitored to ensure will this feature launch will be a success?

The dashboard at https://dashboards.gitlab.net/d/praefect-main/praefect-overview?orgId=1
contains a number of graphs of relevant metrics to the service state of Praefect.
Of high relevance are the Service Apdex and Service Error Ratio.

## Architecture

Praefect will stand in-between rails and other clients and the Gitaly servers in the following way:

```mermaid
sequenceDiagram
	Rails-->>Praefect: RPC call on Praefect's TCP port
	Praefect-->>Gitaly: RPC call on Gitaly's TCP port
	Gitaly-->>Praefect: Request succeeded/failed
        Praefect-->>Rails: Request succeeded/failed
```

For our first deployment to production we'll have redundant praefect nodes
(exact number to be defined, at least 3 –one per zone–) deployed behind a single
internal GCP Load Balancer. All shards will point to that load balancer through
its internal DNS name in the manner illustrated below:

<img src="https://docs.google.com/drawings/d/e/2PACX-1vQoZLL1pmPHN1pD3NDz5DhcqKVOi00sgZ2gDX0zUWp6Jgco2FqYvQryi-B5KoFDW-HPeFZYxihAHRtx/pub?w=480&h=360">

Traffic distribution will be handled by the internal load balancer, relying on
health checks to each Praefect host. Currently the health check is an HTTP
request to the Prometheus server bundled with the Praefect process See
https://gitlab.com/gitlab-org/gitaly/-/issues/2165 for discussion about the
health check. See
https://cloud.google.com/load-balancing/docs/internal#traffic_distribution for
more details on traffic distribution on the load balancer.

All communication channels are unecrypted Gitaly gRPC channels. See
https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/6484 for TLS
enabling efforts.

### Deployment

Praefect is deployed on GCP VMs via omnibus-gitlab packages using our [deployer pipelines](https://ops.gitlab.net/gitlab-com/gl-infra/deployer/-/blob/0e5be9e0eeaaedeba57a4bc8d88a116175c22392/.gitlab-ci.yml#L1201-1220).
We run praefect database migrations in the deploy node, the same way we do it for
rails database migrations.

## Operational Risk Assessment

- [x] What are the potential scalability or performance issues that may result with this change?

There's a slight overhead on each message from a client to a Gitaly node as Praefect will parse the message and route to the primary Gitaly node.
In the initial rollout, there's no replication, which will follow soon after. Once roll out is added, this would increase the load on the primary, as after each write, the secondaries will determine what changed and how to update.

- [x] List the external and internal dependencies to the application (ex: redis, postgres, etc) for this feature and how the it will be impacted by a failure of that dependency.

Praefect will depend on at least one Gitaly node to store the Git data. We may
introduce at least one new Gitaly storage for the initial deployment but the
goal is that all existing storages are changed to go through Praefect.

- [x] Were there any features cut or compromises made to make the feature launch?

Right now the scope is just to introduce a reverse proxy. In the reverse proxy, no.

- [x] List the top three operational risks when this feature goes live.

  - Misconfiguration of secrets or shards: We already ran into one occasion
  where mismatched configuration and secrets caused a brief outage. [We've
  documented how this can be avoided](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/gitaly/praefect-file-storages.md)
  - Saturation: Once we start increasing traffic to Praefect we'll need to be
  observant of CPU, memory, IOPS, and all the usual saturation metrics to check
  that our current provisioning is sufficient or wether it needs upgrades
  - Application bugs: It also already happened once that an uncaught bug in
  Praefect made its way to production. As a corrective action, we've made
  Praefect a default in gstg so our end-to-end testing there is able to catch
  such problems (see https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/9347)

- [x] What are a few operational concerns that will not be present at launch, but may be a concern later?

Failover will probably require its own readiness review when the time comes.
Amongst other things we'll need to establish the expected and acceptable
replication lag, review the coordination and concensus mechanism, etc.

- [x] Can the new product feature be safely rolled back once it is live, can it be disabled using a feature flag?

Not a feature flag per-se but the deploy of Praefect can be rolled back simply
by reverting to pointing directly to each Gitaly node in the `git_data_dirs`
storage list and reconfiguring the hosts, which doesn't require downtime.

- [x] Document every way the customer will interact with this new feature and how customers will be impacted by a failure of each interaction.

Customers will interact with Praefect on every repository interaction: browsing
files in the Web UI, pushing, pulling, cloning, and so on. Failure will result
in outage for all projects being served through Praefect.

Replication failures at this stage are non-critical, since we're not supporting
failover yet.

- [x] As a thought experiment, think of worst-case failure scenarios for this product feature, how can the blast-radius of the failure be isolated?

As mentioned above, a praefect failure will result in outage for all
repositories in shards being served through Praefect. In the event of a
catastrofic failure with no easy recovery, the repository storage configuration
can be updated to point directly to the Gitaly servers running in the file
nodes, bipassing Praefect entirely (see
https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/praefect/praefect-bypass.md)

## Security

- [x] Were the [gitlab security development guidelines](https://about.gitlab.com/security/#gitlab-development-guidelines) followed for this feature?

The applicable part of the security guidelines for Praefect is regarding [OS command
injection](https://docs.gitlab.com/ee/development/secure_coding_guidelines.html#os-command-injection-guidelines).
In the Gitaly project, OS command execution is centralized in the
`git.CommandFactory` and `command.new` interfaces. No OS command is executed by
Praefect, as evidenced by the fact neither of the mentioned interfaces is used
in the praefect code paths (see
[#1](https://gitlab.com/search?group_id=9970&nav_source=navbar&project_id=2009901&repository_ref=master&scope=&search=git.CommandFactory&search_code=true&snippets=false)
and
[#2](https://gitlab.com/search?group_id=9970&nav_source=navbar&project_id=2009901&repository_ref=master&scope=&search=command.New&search_code=true&snippets=false))

- [x] If this feature requires new infrastructure, will it be updated regularly with OS updates?

The infrastructure was implemented using our main terraform repository and
modules, so it will be updated in the same fashion as our other infrastructure
there.

- [x] Has effort been made to obscure or elide sensitive customer data in logging?

There is no sensitive customer data logged.

- [x] Is any potentially sensitive user-provided data persisted? If so is this data encrypted at rest?

There is no sensitive customer data logged.

## Performance

- [ ] Explain what validation was done following GitLab's [performance guidlines](https://docs.gitlab.com/ce/development/performance.html) please explain or link to the results below
    * [Query Performer](https://docs.gitlab.com/ce/development/query_recorder.html)
    * [Sherlock](https://docs.gitlab.com/ce/development/profiling.html#sherlock)
    * [Request Profiling](https://docs.gitlab.com/ce/administration/monitoring/performance/request_profiling.html)
- [x] Are there any potential performance impacts on the database when this feature is enabled at GitLab.com scale?

No. Praefect has its own separate database and doesn't suppose any impact on the
main database. See
https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/9225

- [x] Are there any throttling limits imposed by this feature? If so how are they managed?

Yes. Repository replication is limited to 1 concurrent operation per repo to
ensure consistency
https://ops.gitlab.net/gitlab-cookbooks/chef-repo/blob/2903f90de1225b8d7ea4b0898d0f08ad817fbb21/roles/gprd-base-stor-gitaly.json#L66-67

- [x] If there are throttling limits, what is the customer experience of hitting a limit?

There's no impact on customer experience since replication is a background
operation.

- [ ] For all dependencies external and internal to the application, are there retry and back-off strategies for them?
- [ ] Does the feature account for brief spikes in traffic, at least 2x above the expected TPS?

## Monitoring and Alerts

- [x] Is the service logging in JSON format and are logs forwarded to logstash?

Yes. See https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/8142 for
more details, and https://nonprod-log.gitlab.net/goto/4c75fe5e2cd916edf6a538ac6b77ce5b
for logs.

- [x] Is the service reporting metrics to Prometheus?

  - gprd: https://prometheus.gprd.gitlab.net/targets#job-praefect
  - gstg: https://prometheus.gstg.gitlab.net/targets#job-praefect

- [x] Is the service reporting runtime errors?

  - gprd: https://sentry.gitlab.net/gitlab/praefect-production/
  - gstg: https://sentry.gitlab.net/gitlab/praefect-staging/

- [x] How is the end-to-end customer experience measured?

Via the SLIs mentioned below

- [x] Do we have a target SLA in place for this service?

Yes, from https://gitlab.com/gitlab-com/runbooks/blob/master/metrics-catalog/services/praefect.jsonnet
via the auto-generated rules

- [x] Do we know what the indicators (SLI) are that map to the target SLA?

On https://dashboards.gitlab.net/d/praefect-main/praefect-overview?orgId=1:

  - Error ratios
  - Latency
  - Service Requests per Second
  - Saturation

- [x] Do we have alerts that are triggered when the SLI's (and thus the SLA) are not met?

Yes, from https://gitlab.com/gitlab-com/runbooks/blob/master/metrics-catalog/services/praefect.jsonnet
via the auto-generated rules

- [x] Do we have troubleshooting runbooks linked to these alerts?

Yes. See https://ops.gitlab.net/gitlab-com/runbooks/blob/master/troubleshooting/service-praefect.md

- [x] What are the thresholds for tweeting or issuing an official customer notification for an outage related to this feature?

No special guidelines in this regard. We can follow our existing guidelines for
Web and/or Gitaly outages

## Responsibility

- [ ] Which individuals are the subject matter experts and know the most about this feature?

The Gitaly team (@gl-gitaly)

- [ ] Which team or set of individuals will take responsibility for the reliability of the feature once it is in production?

The SRE Datastores team https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/44008

- [ ] Is someone from the team who built the feature on call for the launch? If not, why not?

## Testing

- [ ] Describe the load test plan used for this feature. What breaking points were validated?
- [ ] For the component failures that were theorized for this feature, were they tested? If so include the results of these failure tests.
- [x] Give a brief overview of what tests are run automatically in GitLab's CI/CD pipeline for this feature?

We run our suite of end-to-end tests after every deployment to Staging.
We also run the full suite of tests on GitLab's master every 2 hours (see
https://gitlab.com/gitlab-org/gitlab/pipeline_schedules), using an
omnibus-gitlab docker image with a simple Praefect setup (GitLab, Praefect,
and Gitaly all in the one container).
