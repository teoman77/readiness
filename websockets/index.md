# Production Readiness Guide

For any new or existing system or a large feature set the questions in this guide help make the existing service more robust, and for new systems it helps prepare them and speed up the process of becoming fully production-ready.

Initially, this guide is likely to be used by Production Engineers who are embedded with other teams working on existing services and features. However, anyone working on a new service or feature set is encouraged to use this guide as well.

The goal of this guide is to help others understand how the new service or feature set may impact the rest of the (production) system; what steps need to be taken (besides deploying this new system) to ensure that it can be properly managed; and to understand what it will take to manage the reliability of the new system / feature / service beyond its' initial deployment.

## Summary

The next service that is part of the effort to [migrate GitLab.com to Kubernetes](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/112) is [websockets](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/355).

Websockets traffic was initially migrated to Kubernetes along with [git https](https://gitlab.com/gitlab-com/gl-infra/readiness/-/blob/master/git-https-websockets/index.md) because we previously were servicing Git traffic by the `git-*` VM fleet.
Now that it is possible to split traffic in the GitLab Chart, we will take websocket service and move it to an isolated deployment. At the same time we will enable the ActionCable feature which will increase the amount of traffic since existing Websocket communication is limited to the interactive web terminal.

## Architecture

This review is for migrating the existing Websockets and Git HTTPs service to Kubernetes.
There are no application configuration changes as part of the migration, as we are currently servicing Websockets traffic on the existing Git fleet which is running in Kubernetes. After this service is built out, we will increase the amount of Websockets traffic by turning on the ActionCable feature. By creating a new service and new dedicated infrastructure we will be able to monitor and isolate this workload.


### Bypassing NGINX with a direct connect to Workhorse

This will be the first service where we bypass NGINX and instead connect directly to Workhorse. If this change is successful we will do the same migration for the Git HTTPs service.
More details about this change in https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/1497, the following diagram highlights the difference in request path once NGINX is removed:

## Current

```mermaid
graph LR
  subgraph GitLab.com
    CloudFlare
  end

  subgraph GCPLoadBalancer
    external-lb
  end


  subgraph HAProxy
    fe
  end

  subgraph GCPInternalLB
    internal-lb
  end

  subgraph NGINXPod
    nginx-controller
  end

  subgraph WebServicePod
    workhorse
    puma
  end

  CloudFlare -->|https| external-lb
  external-lb -->|https| fe
  fe --> |https| internal-lb
  internal-lb --> |https| nginx-controller
  nginx-controller --> |http| workhorse
  workhorse --> |http| puma

```

## Proposed

```mermaid
graph LR
  subgraph GitLab.com
    CloudFlare
  end

  subgraph GCPExternalLB
    external-lb
  end


  subgraph HAProxy
    fe
  end

  subgraph GCPInternalLB
    internal-lb
  end

  subgraph WebServicePod
    workhorse
    puma
  end

  CloudFlare -->|https| external-lb
  external-lb -->|https| fe
  fe --> |http| internal-lb
  internal-lb --> |http| workhorse
  workhorse --> |http| puma

```

### Websockets Interactive Terminal

Currently, the only application for Websockets is the web terminal for in-terminal access to an environment. See more details in https://gitlab.com/gitlab-org/gitlab-workhorse/blob/master/doc/channel.md

 ```mermaid
     sequenceDiagram
        participant Client
        participant HAProxy
        participant Workhorse
        participant Rails
        participant Kubernetes API

        Client->>HAProxy: websocket connection
        HAProxy->>Workhorse: websocket connection
        Workhorse->>Rails: connection details and user permissions
        Rails->>Workhorse: auth ok
        loop ConnectionDetails
            Workhorse->>Rails: connection details and permissions
        end
        Workhorse->>Kubernetes API: websocket frames for terminal access
        Kubernetes API->>Workhorse: websocket frames for terminal access
        Workhorse->>Client: websocket terminal
  ```

Notes:

* The Kubernetes cluster referred to here is the **customer's cluster attached to GitLab**, not the cluster servicing GitLab.com
* Polling for Kubernetes connection details and access is initiated by Rails via a Sidekiq job

### ActionCable

ActionCable is
https://gitlab.com/groups/gitlab-org/-/epics/3056

 ```mermaid
      sequenceDiagram
        participant Client
        participant Workhorse
        participant AC as Rails/ActionCable

        Client->>Workhorse: HTTP GET `Upgrade: websocket`
        Workhorse->>AC: proxy
        AC->>AC: open connection
        AC-->>Workhorse: HTTP 101 Switching Protocols
        Workhorse-->>Client: 200 OK
        AC->>Client: websocket traffic
        Client->>AC: websocket traffic
```

ActionCable will be configured to run in the Rails application by setting `actioncable['in_app'] = true`.
Readiness review for the ActionCable service will be covered by https://gitlab.com/gitlab-com/gl-infra/readiness/-/issues/13

#### Performance and load testing

There will be no dedicated load testing done on staging in advance of rolling this feature out, since it will be controlled by setting a feature flag and isolated to a separate node pool.
The feature flag will be enabled in a Change Issue that will be created when we are ready to turn the feature on.

#### Puma/Workhorse Requests and Limits

Puma and Workhorse will keep the default configuration that we currently use for Git HTTPs workloads.

This allocates a 4 CPUs per Pod and 5GB of memory, with 4 Puma worker processes with each having up to 4 threads.

We are setting memory limits for both Workhorse and Puma but do not set any CPI limits, relying instead on the HPA to scale when needed.

* Puma

[values.yaml](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/-/blob/9677cbbc76f5962a47f23efc572f0662157857cf/releases/gitlab/values/values.yaml.gotmpl#L855-860)

```
  resources:
    limits:
      memory: 6.0G
    requests:
      cpu: 4
      memory: 5G
```

* Workhorse

[values.yaml](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/-/blob/9677cbbc76f5962a47f23efc572f0662157857cf/releases/gitlab/values/values.yaml.gotmpl#L846-851)

```
  resources:
    limits:
      memory: 2G
    requests:
      cpu: 600m
      memory: 200M
```

#### HPA

Websockets will be set to auto-scale from 1 to 150 pods per availability zone, which means there will be a minimum of 3 pods servicing websockets traffic.

Scaling is based on CPU with the following setting:

[values.yaml](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/-/blob/9677cbbc76f5962a47f23efc572f0662157857cf/releases/gitlab/values/values.yaml.gotmpl#L709-710)

```
targetAverageValue: 1600m
```

This instructs the HPA to keep the average CPU utilization around 1600m or 1.6 cores of usage across all the Pods in the deployment.

## Operational Risk Assessment

## What are the internal and external dependencies of this service, are there SPOFs?

Dependencies of this service are the same dependencies of what we currently are running in Kubernetes for Git HTTPs

- Redis (persistent/cache/Sidekiq)
- Patroni / PgBouncer
- HAProxy
- Egress to the public Internet

It was discussed limiting network access in https://gitlab.com/gitlab-com/gl-infra/readiness/-/issues/13#note_479480973 but it was decided that it would be best to leave the network policy that same as what we already have for Git HTTPs for now.

## What are the potential scalability or performance issues that may result with this change?

### Pod Resources

When ActionCable was previously enabled there was memory pressure on Workhorse that caused pod evictions, when we first enable this service we will monitor our memory consumption closely and use generous requests (see above) to profile our usage under production load.

### Logging

- For ActionCable, there will be additional log volume after enabling this feature for `/-/cable` requests, we will need to monitor this usage during the rollout
- For the interactive terminal, there is very little usage and logging so this will have small impact.

### Prometheus metrics

With additional Pods we will generate more metrics which has the potential of causing load on the Prometheus running in cluster. We do not expect a large number of pods and will maintain our current 4 worker configuration to minimize the impact.

### Database

When we enable the new ActionCable feature there will be additional connections to the database, but we do not believe this will be significant since a small number of puma workers will be dedicated to this service.

### Redis

Each Puma worker will have one extra connection to Redis. This connection will be used to run `PUBLISH` and `SUBSCRIBE` commands for all ActionCable connections connected to the Puma worker / process.
This was discussed further in https://gitlab.com/gitlab-com/gl-infra/readiness/-/issues/13#note_484591568 and decided that the additional load on Redis will be minimal.

## List the top three operational risks when this feature goes live.

- Monitoring gaps due to the lack of metrics for Websockets https://gitlab.com/gitlab-org/gitlab/-/issues/296845
- Memory issues that cause pod evictions, this is mitigated by isolating this workload to a dedicated node pool for only websockets traffic
- Increased pressure on the persistent Redis cluster

## Security and Compliance

There are no additional security or compliance concerns as this will be a service that run the same as the existing git https service.

Network limits that prevent access to the internal network using network policy configured in [values.yaml](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/-/blob/8f67ce37b0d1ac71bd54c55ae3246ee555864778/releases/gitlab/values/values.yaml.gotmpl#L760).

Outbound network to the internal network will be restricted, with the following exceptions by port whitelisting:

* Consul and Consul DNS
* Redis
* Postgres
* Gitaly

Outbound network connections to the metadata endpoint `169.254.169.254/32` will not be allowed.

## Monitoring, Alerts and Logging

For the new websockets service there will be a new service defined in the service catalog named "webservice".
The work to setup the new service, dashboards and alerts is covered in https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/1406

For logging we will use the existing index with a new `type=websockets`, the work for this is covered in https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/1502.

We have very little monitoring specific to Websockets, and are currently limited to only seeing metrics for the initial protocol upgrade request.
This feature gap in our metrics is documented in https://gitlab.com/gitlab-org/gitlab/-/issues/296845 and we have decided to move forward with the production rollout despite not having as much visibility as we would like.
Until we have appropriate application metrics, we will instead need to rely on resource metrics provided by the services running in Kubernetes.
These are monitored using our standard service monitoring. For the websockets services, these can be found  in Grafana: https://dashboards.gitlab.net/d/websockets-kube-deployments/websockets-kube-deployment-detail?search=open&folder=current&orgId=1
