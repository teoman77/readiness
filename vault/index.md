# Vault Secrets Management

[[_TOC_]]

Epic: https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/710, https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/739

## Summary

Vault is an identity-based secret and encryption management system. It can be used as a central store to manage access and secrets across applications, systems, and infrastructure. It can be used to audit and version secrets. This would be a replacement to the current system we use to manage secrets. We currently use multiple ways to generate and manage secrets:

  - Project/CI variables
  - GCS objects + KMS
  - GCP Secret Manager
  - Chef vault

Vault will allow us to reduce complexity and standardize our secrets management becoming the single source of truth (SSOT) for all secrets. Other benefits include:

  - RBAC for secrets
  - Auditing and versioning of secrets
  - Integrations with GitLab CI, terraform, kubernetes, ansible, and chef (see [integrations section](#integrations) for more details)
  - Allow us to do an incremental migration
  - Allow us to remove the extra complexity and abstraction that comes from custom plugins/scripts
  - No static tokens
  - No third-party authentication needed

Secret versioning will allow us to easily rotate secrets. Any secret can be retrieved by its version number, even after it has been deleted (as long as the secret key metadata is not deleted which does remove all versions for a secret). So we can specify explicitly the version to fetch for any secret in our deployments, and rotating them is just a matter of bumping the version.

### Self hosting vs Cloud services

We've decided to self host Vault instead of using their cloud provider in order to to keep costs down, drop any external dependencies, and have better control of the data. Additionally, some drawbacks are:

- The hosted model uses Enterprise version which ties pricing to the number of clients ([docs](https://cloud.hashicorp.com/products/vault/pricing))
- Limitations on features ([docs](https://cloud.hashicorp.com/docs/vault#feature-parity))
- Network integration, hosted is AWS only

## Architecture

### High level overview

```mermaid
graph TB

https://vault.env.gitlab.net

subgraph GCP

  IAP-Load-balancer
  Internal-clients
  raft-snapshots-bucket

  subgraph GKE-Vault-Deployment
    Vault
    Raft-PVC
    K8s-CronJob
  end

end

https://vault.env.gitlab.net --> IAP-Load-balancer --> Vault
Internal-clients --> Vault
Vault --> Raft-PVC
K8s-CronJob --> Vault
K8s-CronJob --> raft-snapshots-bucket
```

The application is deployed in pods using the official [Vault Helm chart](https://github.com/hashicorp/vault-helm).

* Helm chart deployment ([example](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-helmfiles/-/blob/3477f7a2e0cce14039f577caac19fef5c522168d/releases/vault))
* Internal and external ingress (see [ingress section below](#ingress))

#### Availability

We will be running vault in [High Availability mode](https://www.vaultproject.io/docs/concepts/ha). This protects against outages by running multiple Vault servers. This consists of one active Vault server and several standby servers. Since we're using the community version, standby instances are not unsealed and are only replicating the data but are not able to read it. The standby instances only unseal when being promoted to leader. We have enabled [automatic unseal with GKMS](https://learn.hashicorp.com/tutorials/vault/autounseal-gcp-kms?in=vault/auto-unseal). The unsealing process is delegated to Google KMS in the event of a failure. The Vault cluster will coordinate leader elections and failovers internally.

We've configured 5 replicas with spread constraints for vault pods to be in [different hosts and be distributed in different zones](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-helmfiles/-/blob/3477f7a2e0cce14039f577caac19fef5c522168d/releases/vault/values.yaml.gotmpl#L46-52). Which will gives us multi-zone failure tolerance across 3 different zones.

Raft storage is configured with regional SSD persistent disks which provide durable storage and replication of data between three zones in the same region.

Additionally, we have multi-region backups we can restore as disaster recovery (in case of full region failure) (see [storage section below](#storage)).

#### Ingress

We will have one internal and one external endpoint, keeping the service not directly exposed to the internet.

- External ingress for web user access is accessible through a GCP HTTPS load balancer that uses Google Identity-Aware Proxy
(IAP). Can be accessed through `https://vault.{env}.gitlab.net`.
  - Vault CLI does not work through this load balancer.
- Internal ingress for CI/Kubernetes/Terraform/Chef/Ansible/etc (API) is exposed through a Kubernetes service with a zonal network endpoint group (NEG) as a backend. The Vault service uses the default port `8200`.
  - Prometheus metrics are gathered from the endpoint `/v1/sys/metrics` on that same port

#### Storage

We will use [Raft](https://www.vaultproject.io/docs/concepts/integrated-storage), Vault's built-in storage engine. This allows all the nodes in a Vault cluster to have a replicated copy of Vault's data.

![storage](img/storage.png)

[Source](https://learn.hashicorp.com/tutorials/vault/raft-storage?in=vault/raft)

We are not using the enterprise version for Vault which come with automatic backups. We are running a [Kubernetes job](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-helmfiles/-/blob/master/releases/vault/charts/vault-extras/templates/job.yaml) to create and save raft snapshots. Manual testing was done in the pre environment for
validation, snapshot backup, and restoration.

Backups are saved [across multiple regions in the United States](https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt/-/blob/b061f37e606f3f81502bae5a8c68571d5bc0f2f0/modules/vault/variables.tf) and can be used as region failure disaster recovery. Testing backups and runbooks on restoring to be added and can be tracked in:

- [Add snapshot restore validation tests](https://gitlab.com/gitlab-com/gl-infra/reliability/-/issues/15855)
- [Add runbook for vault snapshot restore](https://gitlab.com/gitlab-com/gl-infra/reliability/-/issues/15854)

#### Resources and configuration

* Infrastructure resources (IAM, KMS, etc) are managed through Terraform via a [module](https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt/-/blob/master/modules/vault) located in the `config-mgmt` repository.
  * [Pre environment vault configuration](https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt/-/blob/f308d4b6754c2377175867dd836c3bc174ec09c2/environments/pre/vault.tf)
* To maintain consistency across deployments Vault configurations are maintained through the [terraform provider](https://registry.terraform.io/providers/hashicorp/vault/latest/docs) and located in the `config-mgmt` repository. For an example view, the [staging configuration](https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt/-/blob/5c0152cbd4136b198f5b44a61d49fa788e02d441/environments/vault-staging).

### Authentication

#### User authentication
We're using [Google Identity-Aware Proxy](https://cloud.google.com/iap/) for the load balancer and [Google OIDC](https://www.vaultproject.io/docs/auth/jwt/oidc_providers#google) is required to log in to the Vault web interface. In the future, we can also use [Okta](https://www.vaultproject.io/docs/auth/okta) for authentication.

#### API authentication

- CI runners can be authenticated with [JWT/OIDC](https://www.vaultproject.io/docs/auth/jwt) with [JWKS](https://docs.gitlab.com/ee/ci/secrets/#configure-your-vault-server)
- GCP servers (chef client, ansible, etc) can be authenticated with [Google Cloud auth](https://www.vaultproject.io/docs/auth/gcp) (service accounts or instance service accounts)
- Kubernetes can be authenticated with [Kubernetes Service Account Tokens](https://www.vaultproject.io/docs/auth/kubernetes)

[Identities/Roles](https://www.vaultproject.io/docs/concepts/identity) and [Policies](https://www.vaultproject.io/docs/concepts/policies) will be used to enforce RBAC and limit scope of access to necessary secrets. See the [secret access](#secrets-access) section for more information.

## Integrations

GitLab CI variables can be stored in Vault. GitLab authenticates using Vault’s JSON Web Token (JWT) authentication method.

* [GitLab CI example](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-helmfiles/-/merge_requests/729/diffs)

K8s secrets integration via [external-secrets](https://github.com/external-secrets/external-secrets), a Kubernetes operator that reads secrets from Vault and turns them into a Kubernetes Secret.

  * [Tanka example](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/tanka-deployments/-/merge_requests/377/diffs)
  * [Helmfile example](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-helmfiles/-/merge_requests/744/diffs)

Chef integration [options](https://www.hashicorp.com/blog/using-hashicorps-vault-with-chef) is being tracked and discussed in [issue #15798](https://gitlab.com/gitlab-com/gl-infra/reliability/-/issues/15798)

Ansible has a [hashicorp collection that can be used](https://github.com/ansible-collections/community.hashi_vault) (issue TBA)

## Security Considerations

### Data Encryption and Unsealing

Vault data is encrypted at all times. When Vault is started, it is always started in a sealed state and will not be able to decrypt data until it is unsealed.

To [unseal](https://www.vaultproject.io/docs/concepts/seal) vault, a root key is needed to decrypt the Vault data encryption key.

To ensure that the root key is never known or leaked, we have configured auto-unseal using [`GCP KMS`](https://www.vaultproject.io/docs/configuration/seal/gcpckms) which lets us leverage GCP KMS to encrypt and decrypt the root key. Only the KMS key is able to decrypt the root key in our configuration. There is no other method possible to decrypt the root key. You can [view the configuration in terraform](https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt/-/blob/master/modules/vault/kms.tf).

```mermaid
graph TD
    A[Encryption Key] -->|Encrypted by | B(Root Key)
    B --> D[GCP Cloud KMS key]
```

### Recovery keys

Vault uses an algorithm known as [Shamir's Secret Sharing](https://en.wikipedia.org/wiki/Shamir%27s_Secret_Sharing) to split the recovery key into shards. It is important to know that the recovery key can **only** used to generate a root token and not any keys.

Additionally, we're also using end-to-end TLS encryption for vault.

### Secret Access

#### Role-based Access Control (RBAC) Policies

Vault uses policies to govern the behavior of clients and instrument Role-Based Access Control (RBAC). A policy defines a list of paths. Each path declares the capabilities (e.g. "create", "read", "update", "delete", "list", etc) that are allowed. Vault's denies capabilities by default unless explicitly stated other wise.

There are some [built in policies](https://www.vaultproject.io/docs/concepts/policies#built-in-policies) generated by vault and we've currently configured the following policies:

* [Vault policies](https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt/-/blob/master/environments/vault-staging/policies.tf) configured in staging
* [GitLab CI policies](https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt/-/blob/master/environments/vault-staging/policies_gitlab.tf) configured in staging
* [Infra project GitLab CI policies](https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt/-/blob/master/environments/vault-staging/policies_gitlab_ci.tf) configured in staging
* [K8s policies](https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt/-/blob/master/environments/vault-staging/policies_kubernetes.tf) configured in staging

#### JWT Authentication and Bound Claims

The JSON web token (JWT) method can be used to authenticate with Vault by a JWT authentication method or an OIDC. These JWTs can contain claims or a key/value pair. These can be used by Vault to validate that any configured "bound" parameters match which provide more granularity to authentication permissions.

For an example, see the [the bounds claims](https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt/-/blob/f3a02f1df3901361b2031bf04b110b82af1d3eee/environments/vault-staging/roles_oidc.tf#L45-47) configured for OIDC users.

More details and specifications can be found in [the vault documentation](https://www.vaultproject.io/docs/auth/jwt#bound-claims).

## Observability

### Prometheus and Thanos
Vault is monitored via Prometheus in GKE. Configuration is done through a [`PodMonitor`](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-helmfiles/-/blob/master/releases/vault/charts/vault-extras/templates/pod-monitor.yaml) which scrapes the `/v1/sys/metrics` endpoint. Our GKE prometheus metrics are also accessible in our thanos cluster ([example metric](https://thanos.gitlab.net/graph?g0.expr=vault_core_unsealed&g0.tab=1&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D)).

### Logs

By default no auditing is enabled. We've configured an audit device logs on stdout which forwards logs to Kibana. You can [view pre Vault logs here](https://nonprod-log.gitlab.net/goto/0d1be220-e5e7-11ec-b771-57a829f2c394).

### Dashboards, Alerts, etc

Dashboard, alerts, and metrics catalog to be added later and can be tracked in [issue #15853 - Vault metrics, dashboards, and alerts](https://gitlab.com/gitlab-com/gl-infra/reliability/-/issues/15853)


## Responsibility

- Infrastructure team
