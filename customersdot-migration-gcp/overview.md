# CustomersDot migration to GCP

## Summary

This is a migration of the CustomersDot application from Azure to GCP.

- [The main failover issue](https://gitlab.com/gitlab-com/gl-infra/customersdot-ansible-poc/-/issues/82)
- [The production change issue](https://gitlab.com/gitlab-com/gl-infra/production/-/issues/6114)

We currently have no metrics or observability, the Azure box has disk space issues, Chef is on an old version (not upgradable) and stuck with old cookbooks. We are also suffering from micro-outages and large outages regularly due to not being able to scale this box. We've been focusing on workarounds (such as rate-limiting endpoints) to keep it up and running but it is no longer sustainable.

We do have pingdom-like Slack alerts (engineering maintained) and Sentry set up (things we can monitor after the switch). See also the [failover post-checks](https://gitlab.com/gitlab-com/gl-infra/customersdot-ansible-poc/-/issues/82#post-failover).

## Architecture

### Infrastructure architecture

```mermaid
graph TB

cloudflare

subgraph GCP
  subgraph prdsub
    CustomersDot
    gke
    bastion
  end
end

cloudflare --> CustomersDot
```

#### Infrastructure components

- `cloudflare`. This component satisfies [our requirement about minimum recommended version for TLS](https://docs.gitlab.com/ee/development/secure_coding_guidelines.html#tls-minimum-recommended-version).
- `GCP`.  Google Cloud Platform.
- `gke`. The kubernetes cluster for monitoring services.
- `bastion`. SSH proxy for accessing CustomersDot instance.
- `CustomersDot`. A virtual machine with the application.

### Application architecture

```mermaid
graph TB

subgraph CustomersDot
  nginx
  puma-cluster
  rails
  sidekiq
  cron
  database
  redis
end

subgraph external-services
  Zuora
  GitLab-API
  Salesforce
  Platypus
  GitLab-SM
end

nginx -->|socket|puma-cluster

puma-cluster --->|preloads app| rails

rails --->|socket| database
rails --->|session store| redis

sidekiq --->|tcp| database
sidekiq --->|tcp| redis
sidekiq --->cron
```

**Implementation notes:**

- Communication with all `external-services` goes using HTTPS.
- The `CustomersDot` compute engine has opened 80, 443 ports.
- `CustomersDot` is running under the `customersdot` user.
- The `customersdot` user **does not have** permissions to execute commands as `root` via `sudo`.
- `Puma` is configured as a WEB server by default. We have been running it for long time on staging and it showed good results especially in improved performance. We used `unicorn` before.
- PostgreSQL version was bumped from 9.5 to 10. We have been running it for a long time on staging too.
- Ubuntu version upgraded from `16.04` to `20.04`.

#### Application components

- `nginx`. Reverse proxy. This component satisfies [our requirement about minimum recommended version for TLS](https://docs.gitlab.com/ee/development/secure_coding_guidelines.html#tls-minimum-recommended-version).

   ```conf
   # Mozilla Intermediate configuration
   ssl_protocols TLSv1.2 TLSv1.3;
   ssl_ciphers ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384;

   # OCSP Stapling
   ssl_stapling on;
   ssl_stapling_verify on;
   resolver 1.1.1.1 1.0.0.1 8.8.8.8 8.8.4.4 208.67.222.222 208.67.220.220  valid=60s;
   resolver_timeout 2s;
   ```

- `CustomersDot`.
  - `puma-cluster`. Cluster for processing requests.
  - `rails`. WEB application.
  - `sidekiq`. Running background tasks.
  - `database`. The PostgreSQL server instance.
  - `redis`. In-memory key-value database where session data and `Sidekiq` data are stored.
  - `cron`. Running scheduled tasks in background.
- `external-services`. External services that CustomersDot uses in business process.
  - `Zuora`. The payment merchant and gateway.
  - `Platypus`. The proxy for Marketo.
  - `Salesforce`. Leads management.
  - `GitLab-API`. GitLab.com instance.
  - `GitLab-SM`. Self-managed instances of GitLab.

## Provisioning

We have following 3 components for provisioning and running the CustomersDot:

- [Chef](https://gitlab.com/gitlab-com/gl-infra/chef-repo) for bootstrapping/initial provisioning of machines. Count it as deprecated as our goal to move away from it to terraform.
- [Ansible playbooks](https://gitlab.com/gitlab-com/gl-infra/customersdot-ansible-poc) to provision CustomerDot machine and deploy the application.
- [Helm Charts](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-helmfiles) to manage the GKE cluster from Prometheus.

## Migration Plan

**The failover day is Feb 05 2022.**

The migration process and all preparations for it will be started 1 week before the specified data.
Before that we make dry runs and verifications of services and infrastructure, [for instance](https://gitlab.com/gitlab-com/gl-infra/customersdot-ansible-poc/-/issues/86).

1. [Failover pre-flight checks](https://gitlab.com/gitlab-com/gl-infra/customersdot-ansible-poc/-/issues/83)
1. [Failover to GCP](https://gitlab.com/gitlab-com/gl-infra/customersdot-ansible-poc/-/issues/82)

We have [an issue](https://gitlab.com/gitlab-com/gl-infra/customersdot-ansible-poc/-/issues/84) for switching back to old production environment in Azure.

## Operational Risk Assessment

### What are the potential scalability or performance issues that may result with this change

We expect to have zero performance or scalability issues because the new environment has better specs than the current one.
Recently we have been experiencing [performance issues](https://gitlab.com/gitlab-org/customers-gitlab-com/-/issues/3783) with seat links synchronization which come from all self-managed GitLab instances.

The hardware comparison table.

|Name  |Old VM|New VM|
|------|------|------|
|CPU(s)|  2   |  4   |
|Memory|8 Gb  | 16Gb |
|HDD   |30 Gb | 100Gb|

### List the external and internal dependencies to the application (ex: redis, postgres, etc) for this feature and how the it will be impacted by a failure of that dependency

**External dependencies:**

- Zuora. **Impact level: High.** Inability to communicate with Zuora or process its web hooks must be considered as a trigger for a roll back procedure if it takes more than 24 hours to resolve the incident.
- GitLab API. **Impact level: High.** Inability to communicate with GitLab API must be considered as a trigger for a roll back procedure if it takes more than 24 hours to resolve the incident.
- Platypus. **Impact level: Medium** Inability to communicate with Platypus leads to missing leads.
- Visual Compliance API. **Impact level: Medium** This service screens opportunities in Salesforce to ensure that GitLab does not violate any US Regulations by selling to restricted individuals or organizations. Inability to communicate with it means we might miss an user that violates US Regulations.

**Internal dependencies:**

- Redis. **Impact level: High** Any problem with Redis leads to two problems: 1) signing in will not work due out our mechanism for storing session data in Redis. 2) background jobs will not be processed.
- Postgres. **Impact level: High**

### Were there any features cut or compromises made to make the feature launch?

Integration with the ELK stack is postponed due to unclear requirements but still planned. We have two issues per each environment:

- [Staging](https://gitlab.com/gitlab-com/gl-infra/customersdot-ansible-poc/-/issues/19)
- [Production](https://gitlab.com/gitlab-com/gl-infra/customersdot-ansible-poc/-/issues/42)

**Related issues:**

- https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/14941
- https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/14938
- https://gitlab.com/gitlab-com/gl-infra/customersdot-ansible-poc/-/issues/22
- https://gitlab.com/gitlab-com/gl-infra/customersdot-ansible-poc/-/issues/26

### List the top three operational risks when this feature goes live

1. Failure in communication with Zuora.
1. Failure in communication with GitLab API.
1. Potential problems with data integrity in a database.

### What are a few operational concerns that will not be present at launch, but may be a concern later

We expect to have all features to be operational at 100%. HAProxy is not on the list due to CustomersDot being running on a single node as well as horizontal scaling.
We setup basic metrics via `prometheus` and we plan to define more metrics after the migration. This also affects the error budget, SLOs.
Logging is on the post-migration list due to its complexity and limited team resources.

### Can the new product feature be safely rolled back once it is live, can it be disabled using a feature flag

We can switch back to the old production if the migration goes with problems but once we switch to new production environment all new data from the application
and 3rd party services will go into a new database and rolling back to the old production environment will require transferring new data or any other difference back to the old database. Perhaps this can be done by swapping source and destination in our migration scripts for the database. These scripts are located [here](https://gitlab.com/gitlab-com/gl-infra/customersdot-ansible-poc/-/tree/master/scripts).

The main possible issue is our interaction with Zuora. On every update of an entity we usually send or receive update information from / to Zuora and if we decide to switch to old environment we will need to perform synchronization between old database and new data in Zuora. That means we have to transfer all new records to old production database, go over them and pull data from Zuora to update the database.

### Document every way the customer will interact with this new feature and how customers will be impacted by a failure of each interaction

Not covered in this document as all interactions remain the same.

## Database

### If we use a database, is the data structure verified and vetted by the database team?

The data structure was not verified due to its complexity. Our data relies on 3rd party services hence verifying process
must be done with invoking services like Zuora or GitLab API.

### Do we have an approximate growth rate of the stored data (for capacity planning)?

The size of production database is ~7 Gb. We don't expect this size to growth too much.

### Can we age data and delete data of a certain age?

Due to compliance reasons we **must** store everything and prohibit deleting any data except for data covered by GDPR requests or any other legal request.

## Security and Compliance

### If this feature requires new infrastructure, will it be updated regularly with OS updates?

This service will receive security updates in accordance to the [GitLab Release and Maintenance Policy](https://docs.gitlab.com/ee/policy/maintenance.html).

### Has effort been made to obscure or elide sensitive customer data in lvs/customersdot-migration-to-gcpogging?

We filter authentication-related data in logs such as passwords, tokens. Only the database and the redis service hold the customers data.

### Is any potentially sensitive user-provided data persisted? If so is this data encrypted at rest?

For compliance reasons and auditing purposes we store logs for indefinite time. We push all logs to immutable buckets in `Cloud Storage` GCP service. Cloud Storage (and most GCP managed services) [encryption](https://cloud.google.com/storage/docs/encryption) is enabled by default. One of compliance requirements is to have encrypted data on storage devices. GCP provides that.

### Is the service subject to any regulatory/compliance standards? If so, detail which and provide details on applicable controls, management processes, additional monitoring, and mitigating factors

Yes, this service is subject to our standard regulatory and compliance standards and data is classified as [ORANGE](https://about.gitlab.com/handbook/engineering/security/data-classification-standard.html#orange). Access to this data is controlled by the technical owners listed in the `customers.gitlab.com/subscription` [entry in the tech_stack.yml file](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/tech_stack.yml#L913).
## Performance

### Are there any potential performance impacts on the database when this feature is enabled at GitLab.com scale?

Given the fact we haven't experienced any performance drawbacks with the current PostgreSQL implementation, we assume we will not have any of them on the new environment as it has more hardware resources, and we copied configuration settings.

### Are there any throttling limits imposed by this feature? If so how are they managed?

We have 3 defined throttling limits:

- cloudflare
- regular requests. We configured rate limiting to 30 requests per minute except for QA bot.
- [seat links](https://gitlab.com/gitlab-org/customers-gitlab-com/-/blob/staging/doc/license/cloud_license.md#syncing-a-license)

### If there are throttling limits, what is the customer experience of hitting a limit?

When the customer hits the limit we present the special page with the message to try again later.

### For all dependencies external and internal to the application, are there retry and back-off strategies for them?

All background jobs use retry strategy. For external API calls we don't use the retry strategy.

### Does the feature account for brief spikes in traffic, at least 2x above the expected TPS?

Yes, it does. New environment has more resources (CPU, memory) to handle brief spikes in traffic.

## Backup and Restore

### Outside of existing backups, are there any other customer data that needs to be backed up for this product feature?

[We backup](https://ops.gitlab.net/gitlab-com/gl-infra/gitlab-restore/postgres-prdsub) the PostgreSQL database only as it holds customers data. Services like Redis holds temporary data pulled from the database and this data can be re-created by
Sidekiq if required.

### Are backups monitored?

On a daily basis, a [scheduled pipeline](https://ops.gitlab.net/gitlab-com/gl-infra/gitlab-restore/postgres-prdsub/-/pipeline_schedules)
runs, which tests a full database backup and verifies the freshness of the
data. At the end of the verification process, we send a heartbeat to
[deadmanssnitch](https://deadmanssnitch.com/), if no heartbeat is sent for 1
day this will page the EOC.

### Was a restore from backup tested?

Yes, it's validated on a [daily basis](https://ops.gitlab.net/gitlab-com/gl-infra/gitlab-restore/postgres-prdsub/-/pipeline_schedules) both for `prdsub` and `stgsub`.

## Monitoring and Alerts

### Is the service logging in JSON format and are logs forwarded to logstash?

CustomersDot uses the same JSON format as GitLab and `fluentd` daemon forwards
logs to GCP stackdriver, which later on we use a [aggregated
sink](https://cloud.google.com/logging/docs/export/aggregated_sinks)
[`stgsub-logging-sink`](https://console.cloud.google.com/logs/router?project=gitlab-subscriptions-prod)
which sends the logs to cold sotrage for 356 days to
[gitlab-prdsub-logging-archive](https://console.cloud.google.com/storage/browser/gitlab-prdsub-logging-archive).

Below are the logs that we store to stackdriver/GCS
1. [`/var/log/auth.log`](https://cloudlogging.app.goo.gl/DPwz6NyomqbuXi9FA)
1. [`/var/log/sys.log`](https://cloudlogging.app.goo.gl/FC1Rx5TRGJp38DFV9)
1. [`/var/log/kern.log`](https://cloudlogging.app.goo.gl/pC384Pr4WPsUHvyD7)
1. [`/home/customersdot/CustomersDot/shared/log/puma.stderr.log`](https://cloudlogging.app.goo.gl/Nb1GhoePRq232Rof8)
1. [`/home/customersdot/CustomersDot/shared/log/puma.stdout.log`](https://cloudlogging.app.goo.gl/G58L2swud9PRG5Rv8)
1. [`/home/customersdot/CustomersDot/shared/log/production_json.log`](https://cloudlogging.app.goo.gl/PdC5ei4s68mTXsFW7)

If we look at the our production [logging
infrastructure](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/logging/README.md#logging-infrastructure-overview)
we only implement the `GCP hosted services` and don't use any of the
`ElasticSearch cluster` services. Implementing ElasticSearch is not considered
as a blocker for making the migration as it can be completed later. It's
important to note that legacy infrastructure on Azure does not have this
feature too.

**Related issues:**

- [Add logs to Kibana [Staging]](https://gitlab.com/gitlab-com/gl-infra/customersdot-ansible-poc/-/issues/19)
- [Add logs to Kibana [Production]](https://gitlab.com/gitlab-com/gl-infra/customersdot-ansible-poc/-/issues/42)

### Is the service reporting metrics to Prometheus?

CustomersDot reports its metrics to its own Prometheus cluster but we'll have thanos so that we can get long term storage, downsampling and a single interface for SRE/Engineers to query. [The plan](https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/14621) is to have metrics available in https://thanos.gitlab.net/ as well so that everyone can query it and we get long term storage and metric retention (up to 1 year) since with Prometheus we only get around 3 days.

**Issues:**

- [Add Prometheus monitoring [Production]](https://gitlab.com/gitlab-com/gl-infra/customersdot-ansible-poc/-/issues/43)
- [Additional alerting of failed jobs for critical SaaS metrics](https://gitlab.com/gitlab-com/gl-infra/customersdot-ansible-poc/-/issues/33)
- [Add minimum uptime/health checks and alerts](https://gitlab.com/gitlab-com/gl-infra/customersdot-ansible-poc/-/issues/18)
- [Add more Prometheus metrics](https://gitlab.com/gitlab-com/gl-infra/customersdot-ansible-poc/-/issues/26)

### How is the end-to-end customer experience measured?

### Do we have a target SLA in place for this service?

We have [an opened issue](https://gitlab.com/gitlab-com/gl-infra/customersdot-ansible-poc/-/issues/22) about defining SLA for CustomerDot.

### Do we know what the indicators (SLI) are that map to the target SLA?

We don't have any currently.

### Do we have alerts that are triggered when the SLI's (and thus the SLA) are not met?

We use [a self-hosted instance](http://jlo-gitlab.bluegod.net:9092/service/2/) of [the Cabot platform](https://github.com/arachnys/cabot) as a web interface to monitor our services including the health checks above. Changes to health check statuses are reported to the Slack channel `#s_fulfillment_status`. You can find the login credentials in the `Subscription portal` vault in 1Password.

### Do we have troubleshooting runbooks linked to these alerts?

We use [the maintenance mode](https://gitlab.com/gitlab-org/customers-gitlab-com/blob/staging/doc/process/maintenance_mode.md#L20) on CustomersDot for mitigating alerts or ongoing incidents.

### What are the thresholds for tweeting or issuing an official customer notification for an outage related to this feature?

Increased error rate in [Sentry for the last hour](https://sentry.gitlab.net/gitlab/customersgitlabcom/dashboard/?statsPeriod=1h). We don't define any particular threshold except for special type of errors like `Invalid Zuora token` which is defined as a critical metric in this document.

### Do the oncall rotations responsible for this service have access to this service?

No.

## Responsibility

### Which individuals are the subject matter experts and know the most about this feature?

**Engineering:**

- @jameslopez
- @ebaque
- @rcobb
- @vitallium

**Infrastructure:**

- @cmcfarland
- @steveazz

### Which team or set of individuals will take responsibility for the reliability of the feature once it is in production?

For the time being both the fulfillment team and the infrastructure team will be responsible on working together and making sure the infrastructure team enables the fulfillment team to work on reliability issues.

## Readiness Reviewers

### Engineering

- @jameslopez
- @ebaque
- @rcobb
- @vitallium

### Infrastructure

- @cmcfarland
- @steveazz

### Security

- @pmartinsgl
